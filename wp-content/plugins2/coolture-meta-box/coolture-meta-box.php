<?php 
/*
Plugin Name: Festivales en campos personalizados Coolturebox
Plugin URI:
Version: 1.0.1
*/

function custom_festivales_type() {

// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Festivales', 'Post Type General Name', 'festibox' ),
		'singular_name'       => _x( 'Festival', 'Post Type Singular Name', 'festibox' ),
		'menu_name'           => __( 'Festivales', 'festibox' ),
		'parent_item_colon'   => __( 'Festival Padre', 'festibox' ),
		'all_items'           => __( 'Todos los festivales', 'festibox' ),
		'view_item'           => __( 'Ver festival', 'festibox' ),
		'add_new_item'        => __( 'Añadir nuevo festival', 'festibox' ),
		'add_new'             => __( 'Añadir nuevo', 'festibox' ),
		'edit_item'           => __( 'Editar festival', 'festibox' ),
		'update_item'         => __( 'Actualizar festival', 'festibox' ),
		'search_items'        => __( 'Buscar festival', 'festibox' ),
		'not_found'           => __( 'No encontrado', 'festibox' ),
		'not_found_in_trash'  => __( 'No encontrado en la papelera', 'festibox' ),
	);

// Otras opciones para el tipo de entrada personalizada

	$args = array(
		'label'               => $labels,
		'description'         => __( 'Festivales', 'festibox' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'thumbnail', 'comments','page_layout','author' ),
		// Puedes asociar este CPT con una taxonomía por defecto o una taxonomía personalizada
		'taxonomies'          => array( 'caja','cuando','edad','provincia'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);

	// Registro del tipo de entrada personalizada (Customs Post Type)
	register_post_type( 'festival', $args );
}

add_action( 'init', 'custom_festivales_type', 0 );

function crear_taxonomias_fes() {
  register_taxonomy('cuando', array('festival'), array(
  'hierarchical' => false, 'label' => 'Cuándo',
  'query_var' => true, 'rewrite' => true));
}
add_action('init', 'crear_taxonomias_fes', 0);


//hook para crear el meta box principal de los festivales
add_action( 'add_meta_boxes', 'em_mtbx_festival' );

function em_mtbx_festival() {

	// Creamos el meta box personalizado
	add_meta_box( 
                'Datos_festival', // atributo ID
                'Festivales', // Título
                'em_mtbx_festival_function', // Función que muestra el HTML que aparecerá en la pantalla
                'festival', // Tipo de entrada. Puede ser 'post', 'page', 'link', o 'custom_post_type'
                'normal', // Parte de la pantalla donde aparecerá. Puede ser 'normal', 'advanced', o 'side'
                'high' // Prioridad u orden en el que aparecerá. Puede ser 'high', 'core', 'default' o 'low'
                );
	
}

function em_mtbx_festival_function( $post ) {
    
    $f_duracion = get_post_meta( $post->ID, 'f_duración', true );
    $f_recinto = get_post_meta( $post->ID, 'f_recinto', true );
    $f_antecedentes = get_post_meta( $post->ID, 'f_antecedentes', true );
    $f_localidad = get_post_meta( $post->ID, 'f_localidad', true );
    $f_direccion = get_post_meta( $post->ID, 'f_dirección', true );
     ?>

         <h4><?php _e("Duración"); ?></h4>
         <input class="em_mtbx_img" name="f_duración_festival" id="f_duración_festival" type="text" value="<?php if($f_duracion && $f_duracion != '') echo $f_duracion; ?>">
         <h4><?php _e("Ubicación"); ?></h4>
         <input class="em_mtbx_img" name="f_recinto_festival" id="f_recinto_festival" type="text" value="<?php if($f_recinto && $f_recinto != '') echo $f_recinto; ?>">
         <h4><?php _e("Localidad"); ?></h4>
         <input class="em_mtbx_img" name="f_localidad_festival" id="f_localidad_festival" type="text" value="<?php if($f_localidad && $f_localidad != '') echo $f_localidad; ?>">
          <div class="clear"></div>
          
	<?php       
}

//Para grabar los campos. Añadir uno nuevo por cada campo existente, en este caso tres.
add_action( 'save_post', 'em_mtbx_festival_save_meta' );

function em_mtbx_festival_save_meta( $post_id ) {

	echo $_POST['f_duración_festival'];
	if ( isset( $_POST['f_duración_festival'] ) ) {
	
		update_post_meta( $post_id, 'f_duración', $_POST['f_duración_festival']  );

	}
	if ( isset( $_POST['f_recinto_festival'] ) ) {
	
		update_post_meta( $post_id, 'f_recinto', $_POST['f_recinto_festival']  );

	}
	if ( isset( $_POST['f_antecedentes_festival'] ) ) {
	
		update_post_meta( $post_id, 'f_antecedentes', $_POST['f_antecedentes_festival']  );

	}
	if ( isset( $_POST['f_localidad_festival'] ) ) {
	
		update_post_meta( $post_id, 'f_localidad', $_POST['f_localidad_festival']  );

	}
	if ( isset( $_POST['f_direccion_festival'] ) ) {
	
		update_post_meta( $post_id, 'f_dirección', $_POST['f_direccion_festival']  );

	}
}

function add_custom_meta_box() {
	add_meta_box(
				'custom_meta_box', // id
				'Fotos y ediciones del Festival', // título
				'show_custom_meta_box', // función a la que llamamos
				'festival', // sólo para páginas
				'normal', // contexto
				'high'); // prioridad
//TAMBIÉN LO AÑADIMOS A LOS EVENTOS/CONCIERTOS/TEATRO/MUSEOS/MAGIA Y CIRCO/CLASICA Y OPERA/DANZA/MONOLOGOS/MUSICALES
	add_meta_box(
				'custom_meta_box', // id
				'Fotos y ediciones del Festival', // título
				'show_custom_meta_box', // función a la que llamamos
				'espectaculo', // sólo para páginas
				'normal', // contexto
				'high'); // prioridad
	add_meta_box(
				'custom_meta_box', // id
				'Fotos y ediciones del Festival', // título
				'show_custom_meta_box', // función a la que llamamos
				'concierto', // sólo para páginas
				'normal', // contexto
				'high'); // prioridad

	add_meta_box(
				'custom_meta_box', // id
				'Fotos y ediciones del Festival', // título
				'show_custom_meta_box', // función a la que llamamos
				'teatro', // sólo para páginas
				'normal', // contexto
				'default'); // prioridad

	add_meta_box(
				'custom_meta_box', // id
				'Fotos y ediciones del Festival', // título
				'show_custom_meta_box', // función a la que llamamos
				'museos', // sólo para páginas
				'normal', // contexto
				'default'); // prioridad

	add_meta_box(
				'custom_meta_box', // id
				'Fotos y ediciones de Magia/Circo', // título
				'show_custom_meta_box', // función a la que llamamos
				'magiacirco', // sólo para páginas
				'normal', // contexto
				'default'); // prioridad
	add_meta_box(
				'custom_meta_box', // id
				'Fotos y ediciones de Clásica/Ópera', // título
				'show_custom_meta_box', // función a la que llamamos
				'clasicaopera', // sólo para páginas
				'normal', // contexto
				'default'); // prioridad

	add_meta_box(
				'custom_meta_box', // id
				'Fotos y ediciones del Festival', // título
				'show_custom_meta_box', // función a la que llamamos
				'danza', // sólo para páginas
				'normal', // contexto
				'default'); // prioridad

	add_meta_box(
				'custom_meta_box', // id
				'Fotos y ediciones del Monólogo', // título
				'show_custom_meta_box', // función a la que llamamos
				'monologos', // sólo para páginas
				'normal', // contexto
				'default'); // prioridad

	add_meta_box(
				'custom_meta_box', // id
				'Fotos y ediciones del Musical', // título
				'show_custom_meta_box', // función a la que llamamos
				'musicales', // sólo para páginas
				'normal', // contexto
				'default'); // prioridad



}
add_action('add_meta_boxes', 'add_custom_meta_box');

// Para imágenes cargamos el script sólo si estamos en páginas.

function add_admin_scripts( $hook ) {
		wp_enqueue_script('custom-js', plugins_url( '/coolture-meta-box/custom-js.js'));
}
add_action( 'admin_enqueue_scripts', 'add_admin_scripts', 10, 1 );

$prefix = 'custom_';
$custom_meta_fields = array( // Dentro de este array podemos incluir más tipos
     array(
       'label'	=> 'Fechas',
       'desc'	=> 'Fechas de las ediciones',
       'id'		=> $prefix.'fecha',
       'type'	=> 'textorepetible'
     ),
     array(
       'label'	=> 'Logo',
       'desc'	=> 'Logotipo',
       'id'		=> $prefix.'f_logo',
       'type'	=> 'imagenrepetible'
     ),
     array(
       'label'	=> 'Fotos',
       'desc'	=> 'Imagenes de galería inferior',
       'id'		=> $prefix.'imagenrepetible',
       'type'	=> 'imagenrepetible'
     )
);

function show_custom_meta_box() {
	global $custom_meta_fields, $post;
	// Usamos nonce para verificación
	echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';
  
			// Creamos la tabla de campos personalizados y empezamos un loop con todos ellos
			echo '<table class="form-table">';
			foreach ($custom_meta_fields as $field) { // Hacemos un loop con todos los campos personalizados
					// obtenemos el valor del campo personalizado si existe para este $post->ID
					$meta = get_post_meta($post->ID, $field['id'], true);
					// comenzamos una fila de la tabla
                                        //Si la etiqueta es fechas, cambiamos la etiquera para que sea Fechas del Evento
					$etiqueta = $field['label'];
					if ($etiqueta  == 'Fechas')
					{
					  $etiqueta = 'Fechas del Evento';
					}
					echo '<tr>
									<th><label for="'.$field['id'].'">'.$etiqueta.'</label></th>
									<td>';
									switch($field['type']) { // Si tenemos varios tipos de campos aquí se seleccionan

										// En nuestro caso tenemos solo uno: Imagen repetible
										case 'textorepetible': // Lo que pone en "type" más arriba
												echo '<ul id="'.$field['id'].'-repeatable" class="custom_repeatable">';
												$i = 0;
												if ($meta) { // Si get_post_meta nos ha dado valores, hacemos un foreach
													foreach($meta as $row) {
													?>
														<li>
															<!-- El input con el valor del meta. Su attributo "name" tiene un número que se irá incrementando amedida que creamos nuevos campos -->
															<input name="<?php echo $field['id'] . '['.$i.']'; ?>" id="<?php echo $field['id']; ?>" type="text" value="<?php echo $row; ?>" width="100%" class="custom_fecha"/>
															<!-- quitar -->
															<a class="repeatable-remove button" href="#"> Quitar </a>
														</li>
  
														<?php $i++; // Incrementamos el contador para que no se repita el atributo "name"
													} // Fin del foreach
												} else { // Si no hay datos ?>
														<li>
															<input name="<?php echo $field['id'] . '['.$i.']'; ?>" id="<?php echo $field['id']; ?>" type="text" class="custom_fecha" value="<?php echo $row; ?>" />
															<!-- quitar -->
															<a class="repeatable-remove button" href="#"> Quitar </a>
														</li>
												<?php } ?>
												</ul>
												<br />
												<!-- Botón para añadir una nueva fila -->
												<a class="repeatable-add-fecha button-primary" href="#">+ A&ntilde;adir</a>
												<!-- Aquí va la descripción -->
												<br clear="all" /><br /><p class="description"><?php echo $field['desc']; ?></p>
									   <?php break; 
										case 'imagenrepetible': // Lo que pone en "type" más arriba
												$image = plugins_url( '/festival-meta-box/no-hay-imagen.png'); // Ponemos una imagen por defecto
												echo '<span class="custom_default_image" style="display:none">'.$image.'</span>'; // Al principio no la mostramos
												echo '<ul id="'.$field['id'].'-repeatable" class="custom_repeatable">';
												$i = 0;
												if ($meta) { // Si get_post_meta nos ha dado valores, hacemos un foreach
													foreach($meta as $row) {
														// Obtenemos la imagen en su tamaño máximo. Podéis poner en su lugar
														// thumbnail, medium o large	
														$image = wp_get_attachment_image_src($row, 'full'); 
														// la primera parte de wp_get_attachment_image_src nos da su url.
														$image = $image[0]; ?>
														<li>
															<!-- Añadimos la imagen que se arrastra para cambiar posición, dentro de tu tema -->
															<span class="sort hndle" style="float:left;"><img src="<?php echo plugins_url( '/festival-meta-box/move.png');?>" />&nbsp;&nbsp;&nbsp;</span>
															<!-- El input con el valor del meta. Su attributo "name" tiene un número que se irá incrementando a
																 medida que creamos nuevos campos -->
															<input name="<?php echo $field['id'] . '['.$i.']'; ?>" id="<?php echo $field['id']; ?>" type="hidden" class="custom_upload_image" value="<?php echo $row; ?>" />
															<!-- mostramos la imagen con 200px de ancho para ver lo que hemos subido -->
															<img src="<?php echo $image; ?>" class="custom_preview_image" alt="" width="200"/><br />
															<!-- El botón de Seleccionar Imagen -->
															<input class="custom_upload_image_button button" type="button" value="Seleccionar imagen" />
															<!-- Los botones de eliminar imagen y de quitar fila-->
															<small> <a href="#" class="custom_clear_image_button">Eliminar imagen</a></small>                                               
															&nbsp;&nbsp;&nbsp;<a class="repeatable-remove button" href="#">- Quitar fila</a>
														</li>
  
														<?php $i++; // Incrementamos el contador para que no se repita el atributo "name"
													} // Fin del foreach
												} else { // Si no hay datos ?>
														<li><span class="sort hndle" style="float:left;"><img src="<?php echo plugins_url( '/festival-meta-box/move.png');?>" />&nbsp;&nbsp;&nbsp;</span>
															<input name="<?php echo $field['id'] . '['.$i.']'; ?>" id="<?php echo $field['id']; ?>" type="hidden" class="custom_upload_image" value="<?php echo $row; ?>" />
															<img src="<?php echo $image; ?>" class="custom_preview_image" alt="" width="200" /><br />
															<input class="custom_upload_image_button button" type="button" value="Seleccionar imagen" />
															<small> <a href="#" class="custom_clear_image_button">Eliminar imagen</a></small>
															&nbsp;&nbsp;&nbsp;<a class="repeatable-remove button" href="#">- Quitar fila</a>
														</li>
												<?php } ?>
												</ul>
												<br />
												<!-- Botón para añadir una nueva fila -->
												<a class="repeatable-add button-primary" href="#">+ A&ntilde;adir</a>
												<!-- Aquí va la descripción -->
												<br clear="all" /><br /><p class="description"><?php echo $field['desc']; ?></p>
									   <?php break; 
  
									} // fin del switch
					echo '</td></tr>';
			} // fin del foreach
			echo '</table>'; // fin de la tabla
	} // Fin de la función
// Grabar los datos
function save_custom_meta($post_id) {
	global $custom_meta_fields;

		// verificamos usando nonce
		if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))
				return $post_id;
		// comprobamos si se ha realizado una grabación automática, para no tenerla en cuenta
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
				return $post_id;
		// comprobamos que el usuario puede editar
		if ('page' == $_POST['post_type']) {
				if (!current_user_can('edit_page', $post_id))
						return $post_id;
				} elseif (!current_user_can('edit_post', $post_id)) {
						return $post_id;
		}

		// hacemos un loop por todos los campos y guardamos los datos
		foreach ($custom_meta_fields as $field) {
				$old = get_post_meta($post_id, $field['id'], true);
				$new = $_POST[$field['id']];
				if ($new && $new != $old) {
						update_post_meta($post_id, $field['id'], $new);
				} elseif ('' == $new && $old) {
						delete_post_meta($post_id, $field['id'], $old);
				}
		} // final del foreach
}
add_action('save_post', 'save_custom_meta');

function obtener_hoteles_festival($festival) {
//Si se queiren obtener todos los hoteles, utilizar obtener_caja_hoteles()
	$hoteles = array();
    $args = array(
        'posts_per_page' => '-1',
		'order' => 'ASC',
		'orderby' => 'title',
        'post_type' => 'alojamiento',
        'post_status' => 'publish',
		'meta_query' => array(
			array(
				'key' => 'f_festivales',
				'value' => ','.$festival.',',
				'compare' => 'LIKE'
			)
		)
    );

    $wp_query = new WP_Query($args);

    $tests_labs = $wp_query->posts;

    if (!empty($tests_labs)) {
        foreach ($tests_labs as $post) {
			$id_hotel= $post -> ID;
        	$title_hotel= $post -> post_title;
			if (!in_array($id_hotel, $hoteles)){
				$hoteles[] = $id_hotel;
			}
    	}
    }else{
        $hoteles[0] = '';
	}
    return $hoteles;
}
/*
function obtener_caja_festivales($caja = '', $arg_pred = '') 
{
	$festivales = array();
	$festivales2 = array();
	//
	$args = array(
		'order' => 'ASC',
		'orderby' => 'title',
		'post_type' => array('festival','espectaculo','concierto'),
		'post_status' => 'publish',
	);

	//
	if ($arg_pred <> '') 
	{
		$args['tax_query'] = $arg_pred;
	}
	//	
	$wp_query = new WP_Query($args);
	//
	$tests_labs = $wp_query->posts;
	//
	
	
    return $festivales;
}
*/

function obtener_caja_festivales($caja = '', $arg_pred = '') 
{
//para obtener todos los festivales ponemos la $caja = '';
	$festivales = array();
	$festivales2 = array();
	$args = array(
		'posts_per_page' => '-1',
		'order' => 'ASC',
		'orderby' => 'title',
		'post_type' => array('festival','espectaculo','concierto','clasicaopera','magiacirco','danza','monologos','museos','musicales','teatro'),
		'post_status' => 'publish',
	);
	//echo '<h2>EVENTOS DE TEATRO</h2>';
	if ($arg_pred <> '') $args['tax_query'] = $arg_pred;
	$wp_query = new WP_Query($args);

	$tests_labs = $wp_query->posts;

	if (!empty($tests_labs)) 
    {
		foreach ($tests_labs as $post) 
        {
			$id_festival = $post -> ID;
			$title_festival = $post -> post_title;
			if (!in_array($id_festival, $festivales))
            {
				$festivales2[] = $id_festival;
                                //echo '<h2>' .$id_festival. '</h2>';
			}
		}
	}else
        {
		$festivales[0] = '';
	}
	//Si la caja viene informada...
	if ($caja <> '')
	{
	//Obtenemos los servicios de la caja
	//echo '<h2>CAJA...' .$caja. '</h2>';
		$args2 = array(
			'posts_per_page' => '-1',
			'order' => 'ASC',
			'orderby' => 'title',
			'post_type' => 'servicio',
			'post_status' => 'private',
			'meta_query' => array(
				array(
					'key' => 'f_productos',
					'value' => ','.$caja.',',
					'compare' => 'LIKE'
				)
			)
		);
		$wp_query2 = new WP_Query($args2);
	
		$tests_labs2 = $wp_query2->posts;
		if (!empty($tests_labs2)) 
		{
			//Para cada servicio incluido en la caja
			foreach ($tests_labs2 as $post) 
			{
				//echo '<h2>servicio de la caja...' .$post->ID. '</h2>';
				//Recuperamos los festivales del servicio incluido en la caja
				$datas2 = get_post_meta( $post->ID, 'f_festivales', true );
				//Separamos los festivales incluidos en la caja (por comas)
				$datas = explode(',',$datas2);

//echo '<h2>EVENTOS DE FESTIVALES(TEATRO) DE LA CAJA</h2>';
//foreach ($datas as $prueba)
//{
//echo '<h2>' .$prueba. '</h2>';
//}
				//Para cada uno de los festivales recuperados en el bloque anterior (estos son todos los festivales, incluidos o no en la caja)
				foreach ($festivales2 as $data)
				{
					//echo '<h2>El valor de $data...' .$data. ' no es nulo</h2>';
					if (!in_array($data, $datas))
					{						
					//echo '<h2>No existe en $datas</h2>';
					}
					if (!in_array($data, $festivales))
					{						
					//echo '<h2>No existe en $festivales</h2>';
					}

					//Si el festival no es nulo Y el festival está incluido en los festivales de la caja Y el festival no está incluído en los festivales a devolver...
					if ($data <> '' && in_array($data, $datas) && !in_array($data, $festivales) )
					{
						//echo '<h2>El festival' .$data. ' SI se añade</h2>';
						$fechas_evento = obtener_fechas_festival_txt($data);
						//if($fechas_evento['next'] <> 'Aún por confirmar')
						if (($fechas_evento['next'] <> 'Aún por confirmar') || (get_post_type($data) == 'festival'))
						//if(evento_vigente($fechas_evento['next']) == 'S')
						{
							$festivales[] = $data;
						}
					}
				}
			}
		}else{
			$festivales[0] = '';
		}
	}else{
		$festivales = $festivales2;
	}

    return $festivales;
}

function obtener_caja_festivales_ord($caja = '', $arg_pred = '') 
{
//para obtener todos los festivales ponemos la $caja = '';
	$festivales = array();
	$festivales2 = array();
	global $wpdb;
    $tests_labs = $wpdb->get_results("SELECT ID FROM wp_posts WHERE post_type IN ('festival','espectaculo','concierto','clasicaopera','magiacirco','danza','monologos','museos','musicales','teatro') AND post_status = 'publish' ORDER BY post_title ASC");

	if (!empty($tests_labs)) 
    {
		foreach ($tests_labs as $post) 
        {
			$id_festival = $post -> ID;
			$title_festival = $post -> post_title;
			if (!in_array($id_festival, $festivales))
            {
				$festivales2[] = $id_festival;
                                //echo '<h2>' .$id_festival. '</h2>';
			}
		}
	}else
        {
		$festivales[0] = '';
	}
	//Si la caja viene informada...
	if ($caja <> '')
	{
	//Obtenemos los servicios de la caja
	//echo '<h2>CAJA...' .$caja. '</h2>';
		$args2 = array(
			'posts_per_page' => '-1',
			'order' => 'ASC',
			'orderby' => 'title',
			'post_type' => 'servicio',
			'post_status' => 'private',
			'meta_query' => array(
				array(
					'key' => 'f_productos',
					'value' => ','.$caja.',',
					'compare' => 'LIKE'
				)
			)
		);
		$wp_query2 = new WP_Query($args2);
	
		$tests_labs2 = $wp_query2->posts;
		if (!empty($tests_labs2)) 
		{
			//Para cada servicio incluido en la caja
			foreach ($tests_labs2 as $post) 
			{
				//echo '<h2>servicio de la caja...' .$post->ID. '</h2>';
				//Recuperamos los festivales del servicio incluido en la caja
				$datas2 = get_post_meta( $post->ID, 'f_festivales', true );
				//Separamos los festivales incluidos en la caja (por comas)
				$datas = explode(',',$datas2);

				//Para cada uno de los festivales recuperados en el bloque anterior (estos son todos los festivales, incluidos o no en la caja)
				foreach ($festivales2 as $data)
				{
					//echo '<h2>El valor de $data...' .$data. ' no es nulo</h2>';
					if (!in_array($data, $datas))
					{						
					//echo '<h2>No existe en $datas</h2>';
					}
					if (!in_array($data, $festivales))
					{						
					//echo '<h2>No existe en $festivales</h2>';
					}

					//Si el festival no es nulo Y el festival está incluido en los festivales de la caja Y el festival no está incluído en los festivales a devolver...
					if ($data <> '' && in_array($data, $datas) && !in_array($data, $festivales) )
					{
						//echo '<h2>El festival' .$data. ' SI se añade</h2>';
						$fechas_evento = obtener_fechas_festival_txt($data);
						//if($fechas_evento['next'] <> 'Aún por confirmar')
						if(($fechas_evento['next'] <> 'Aún por confirmar')|| (get_post_type($data) == 'festival'))
						//if(evento_vigente($fechas_evento['next']) == 'S')
						{
							$festivales[] = $data;
						}
					}
				}
			}
		}else{
			$festivales[0] = '';
		}
	}else{
		$festivales = $festivales2;
	}

    return $festivales;
}

function obtener_caja_hoteles($caja) {
//para obtener todos los hoteles ponemos la $caja = '';
	$hoteles = array();
	$hoteles2 = array();
	$args = array(
		'posts_per_page' => '-1',
		'order' => 'ASC',
		'orderby' => 'title',
		'post_type' => 'alojamiento',
		'post_status' => 'publish',
	);
	$wp_query = new WP_Query($args);

	$tests_labs = $wp_query->posts;

	if (!empty($tests_labs)) {
		foreach ($tests_labs as $post) {
			$id_hotel = $post -> ID;
			$title_hotel = $post -> post_title;
			if (!in_array($id_hotel, $hoteles)){
				$hoteles2[] = $id_hotel;
			}
		}
	}else{
		$hoteles2[0] = '';
	}
	if ($caja <> ''){
		$args2 = array(
			'posts_per_page' => '-1',
			'order' => 'ASC',
			'orderby' => 'title',
			'post_type' => 'servicio',
			'post_status' => 'private',
			'meta_query' => array(
				array(
					'key' => 'f_productos',
					'value' => ','.$caja.',',
					'compare' => 'LIKE'
				)
			)
		);
		$wp_query2 = new WP_Query($args2);
	
		$tests_labs2 = $wp_query2->posts;
	
		if (!empty($tests_labs2)) {
			foreach ($tests_labs2 as $post) {
				$datas2 = get_post_meta( $post->ID, 'f_alojamientos', true );
				$datas = explode(',',$datas2);
				foreach ($hoteles2 as $data){
					if ($data <> '' && in_array($data, $datas) && !in_array($data, $hoteles) ){
						$hoteles[] = $data;
					}
				}
			}
		}else{
			$hoteles[0] = '';
		}
	}else{
		$hoteles = $hoteles2;
	}

    return $hoteles;
}

function obteneter_caja($caja) {

	$cajas = array();
	$productos = array();

	$args = array(
		'posts_per_page' => '-1',
		'order' => 'ASC',
		'orderby' => 'title',
		'post_type' => 'servicio',
		'post_status' => 'private',
		'meta_query' => array(
			'relation' => 'OR',
			array(
				'key' => 'f_productos',
				'value' => ','.$caja.',',
				'compare' => 'LIKE'
			),
			array(
				'key' => 'f_festivales',
				'value' => ','.$caja.',',
				'compare' => 'LIKE'
			)
		)
	);
	
	$wp_query = new WP_Query($args);

    $tests_labs = $wp_query->posts;

    if (!empty($tests_labs)) {
        foreach ($tests_labs as $post) {
			$id = $post -> ID;
			$productos = explode(',',get_post_meta($id, 'f_productos', true));
			if ($id <> ''){
        		foreach ($productos as $producto) {
					if($producto<>'' && !in_array($producto, $cajas) ){
						$cajas[] = $producto;
					}
				}
			}
    	}
    }else{
        $cajas[0] = '';
	}
    return $cajas;
}

function obtener_hoteles_festival2($festival) {

	$hoteles = array();
    $args = array(
        'posts_per_page' => '-1',
		'order' => 'ASC',
		'orderby' => 'title',
        'post_type' => 'alojamiento',
		'meta_query' => array(
			array(
				'key' => 'f_festivales',
				'value' => $festival,
				'compare' => 'LIKE'
			)
		)
    );

    $wp_query = new WP_Query($args);

    $tests_labs = $wp_query->posts;

    if (!empty($tests_labs)) {
        foreach ($tests_labs as $post) {
			$id_hotel= $post -> ID;
        	$title_hotel= $post -> post_title;
			if (!in_array($id_hotel, $hoteles)){
				$hoteles[] = $id_hotel;
			}
    	}
    }else{
        $hoteles[0] = '';
	}
    return $hoteles;
}

function obtener_caja_festivales2() {

	$festivales = array();
		$args = array(
			'posts_per_page' => '-1',
			'order' => 'ASC',
			'orderby' => 'title',
			'post_type' => 'festival',
			'post_status' => array('draft','publish'),
		);

    $wp_query = new WP_Query($args);

    $tests_labs = $wp_query->posts;

    if (!empty($tests_labs)) {
        foreach ($tests_labs as $post) {
			$id_festival = $post -> ID;
        	$title_festival = $post -> post_title;
			if (!in_array($id_festival, $festivales)){
				$festivales[] = $id_festival;
			}
    	}
    }else{
        $festivales[0] = '';
	}
    return $festivales;
}

function festivales_filtro($page_id){
	$args = array();
	$args['realtion'] = 'AND';
	$cu = '';
	$pr = '';
	$co = '';
	$es = '';
	$ed = '';
	$du = '';
	$bu = '';
	$output = '';
	$taxonomy = array();
	$taxo = array();
	$taxo_term = array();
	$meses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
	$duraciones = array('Un día', 'Dos días', 'Tres días', 'Más de tres días');
	$buscar = '';
	
	if (isset ($_GET['cu']) && $_GET['cu'] <> ''){
		$args[] = array( 'taxonomy' => 'cuando', 'field' => 'slug', 'terms' => array( $_GET['cu'] ), 'include_children' => true,  'operator' => 'IN');
		$cu = $_GET['cu'];
	}
    $taxonomy[] = array('nombre' => 'cuándo', 'listado' => get_terms('cuando'), 'query' => $cu, 'get' => 'cu'  );

	if (isset ($_GET['pr']) && $_GET['pr'] <> ''){
		$args[] = array( 'taxonomy' => 'provincia', 'field' => 'slug', 'terms' => array( $_GET['pr'] ), 'include_children' => true,  'operator' => 'IN');
		$pr = $_GET['pr'];
	}
//    $taxonomy[] = array('nombre' => 'provincia', 'listado' =>  get_terms('provincia'), 'query' => $pr, 'get' => 'pr'  );

	if (isset ($_GET['ca']) && $_GET['ca'] <> ''){
		$args[] = array( 'taxonomy' => 'comunidadautonoma', 'field' => 'slug', 'terms' => array( $_GET['ca'] ), 'include_children' => true,  'operator' => 'IN');
		$co = $_GET['ca'];
	}
    $taxonomy[] = array('nombre' => 'dónde', 'listado' =>  get_terms('comunidadautonoma'), 'query' => $co, 'get' => 'ca' );

	if (isset ($_GET['es']) && $_GET['es'] <> ''){
		$args[] = array( 'taxonomy' => 'estilo', 'field' => 'slug', 'terms' => array( $_GET['es'] ), 'include_children' => true,  'operator' => 'IN');
		$es = $_GET['es'];
	}
    $taxonomy[] = array('nombre' => 'estilo', 'listado' =>  get_terms('estilo'), 'query' => $es, 'get' => 'es'  );

	if (isset ($_GET['ed']) && $_GET['ed'] <> ''){
		$args[] = array( 'taxonomy' => 'edad', 'field' => 'slug', 'terms' => array( $_GET['ed'] ), 'include_children' => true,  'operator' => 'IN');
		$ed = $_GET['ed'];
	}
//    $taxonomy[] = array('nombre' => 'edad', 'listado' =>  get_terms('edad'), 'query' => $ed, 'get' => 'ed'  );

	if (isset ($_GET['du']) && $_GET['du'] <> ''){
		$args[] = array( 'taxonomy' => 'duracion', 'field' => 'slug', 'terms' => array( $_GET['du'] ), 'include_children' => true,  'operator' => 'IN');
		$du = $_GET['du'];
	}
    $taxonomy[] = array('nombre' => 'duración', 'listado' =>  get_terms('duracion'), 'query' => $du, 'get' => 'du'  );

	if (isset ($_GET['ar']) && $_GET['ar'] <> ''){
		$args[] = array( 'taxonomy' => 'artista', 'field' => 'slug', 'terms' => array( $_GET['ar'] ), 'include_children' => true,  'operator' => 'IN');
		$ar = $_GET['ar'];
	}
    $taxonomy[] = array('nombre' => 'artista', 'listado' =>  get_terms('artista'), 'query' => $ar, 'get' => 'ar'  );

	$output .= '<div class="filtro_festivales">';
	if (!is_search() && !is_tax()){
		$output .= '<form method="GET" style="padding-right: 3px;" action="'.get_permalink($page_id).$buscar.'">';	
			foreach ($taxonomy as $taxo) {
				$output .= '<div>';
				$output .= '<label for="'.$taxo['get'].'">'.$taxo['nombre'].'</label>';
				$output .= '<select name="'.$taxo['get'].'">';
				$selected = '';
				if ($taxo['query'] == ''){ $selected = 'selected';}
				$output .= '<option value="" '.$selected.'>Todos</option>';
	//			print_r($taxo['listado']);
				if($taxo['nombre'] == 'cuándo'){
					foreach ($meses as $mes){
						foreach ($taxo['listado'] as $taxo_term) {
							if ( $taxo_term->name == $mes){	
								$selected = '';
								if ($taxo_term->slug == $taxo['query']){ $selected = 'selected';}
								$output .= '<option value="'.$taxo_term->slug.'" '.$selected.'>'.$taxo_term->name.'</option>';
							}
						}
					}
				}elseif($taxo['nombre'] == 'duración'){
					foreach ($duraciones as $duracion){
						foreach ($taxo['listado'] as $taxo_term) {
							if ( $taxo_term->name == $duracion){	
								$selected = '';
								if ($taxo_term->slug == $taxo['query']){ $selected = 'selected';}
								$output .= '<option value="'.$taxo_term->slug.'" '.$selected.'>'.$taxo_term->name.'</option>';
							}
						}
					}
				}else{
					foreach ($taxo['listado'] as $taxo_term) {
						$selected = '';
						if ($taxo_term->slug == $taxo['query']){ $selected = 'selected';}
						$output .= '<option value="'.$taxo_term->slug.'" '.$selected.'>'.$taxo_term->name.'</option>';
					}
				}
				$output .= '</select>';
				$output .= "</div>";
			}
		$output .= '<input type="submit" value="Filtrar">';

		$url = '?codigo='.$codigo;
		if (isset($_GET['codigo']) || $_GET['codigo'] <> '') $output .= '<input name="codigo" type="hidden" value="'.$_GET['codigo'].'">';
		if (isset($_GET['fv']) || $_GET['fv'] <> '') $output .= '<input name="fv" type="hidden" value="'.$_GET['fv'].'">';
		if (isset($_GET['sv']) || $_GET['sv'] <> '') $output .= '<input name="sv" type="hidden" value="'.$_GET['sv'].'">';
		if (isset($_GET['ht']) || $_GET['ht'] <> '') $output .= '<input name="ht" type="hidden" value="'.$_GET['ht'].'">';
		if (isset($_GET['ff']) || $_GET['ff'] <> '') $output .= '<input name="ff" type="hidden" value="'.$_GET['ff'].'">';
		if (isset($_GET['fh']) || $_GET['hf'] <> '') $output .= '<input name="hf" type="hidden" value="'.$_GET['hf'].'">';
		if (isset($_GET['fh']) || $_GET['hf'] <> '') $output .= '<input name="hf" type="hidden" value="'.$_GET['hf'].'">';

		$output .= '</form>';	

	}
	$output .= "</div>\n";
//	print_r($args);
	
	$output = '';
	return array('output' => $output, 'arg' => $args);
}

function festivales_tab_pagination($wp_query, $paged, $pages = '', $range = 2, $codigo = '') {  
	 $output = '';
	 $showitems = ($range * 2)+1;  

	 if(empty($paged)) $paged = 1;

	 $url = str_replace('?','&',$codigo);
	 if (isset($_GET['cu']) || $_GET['cu'] <> '') $url .= '&cu='.$_GET['cu'];
	 if (isset($_GET['ca']) || $_GET['ca'] <> '') $url .= '&ca='.$_GET['ca'];
	 if (isset($_GET['es']) || $_GET['es'] <> '') $url .= '&es='.$_GET['es'];
	 if (isset($_GET['du']) || $_GET['du'] <> '') $url .= '&du='.$_GET['du'];
	 if (isset($_GET['ar']) || $_GET['ar'] <> '') $url .= '&ar='.$_GET['ar'];
//	 if (!empty($codigo)) $url .= '&codigo='.$_GET['codigo'];
//	 if (isset($_GET['fv']) || $_GET['fv'] <> '') $url .= '&fv='.$_GET['fv'];
//	 if (isset($_GET['sv']) || $_GET['sv'] <> '') $url .= '&sv='.$_GET['sv'];
//	 if (isset($_GET['ht']) || $_GET['ht'] <> '') $url .= '&sv='.$_GET['ht'];
//	 if (isset($_GET['ff']) || $_GET['ff'] <> '') $url .= '&ff='.$_GET['ff'];
//	 if (isset($_GET['fh']) || $_GET['hf'] <> '') $url .= '&hf='.$_GET['hf'];

	 if($pages == '')
	 {
		 $pages = $wp_query->max_num_pages;
		 if(!$pages)
		 {
			 $pages = 1;
		 }
	 }   

	 if(1 != $pages)
	 {
		 $output .= "<nav class='portfolio-pagination pagination-cubic'>";
			 $output .= '<ul class="page-numbers">';
				 if($paged > 2 && $paged > $range+1 && $showitems < $pages) $output .= "<li><a href='".get_permalink().'?pg='.(1).$url."' class='prev page-numbers'><<</a></li>";
		
				 for ($i=1; $i <= $pages; $i++)
				 {
					 if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
					 {
						 $output .= ($paged == $i)? "<li><span class='page-numbers current'>".$i."</span></li>":"<li><a href='".get_permalink().'?pg='.($i).$url."' class='inactive' >".$i."</a></li>";
					 }
				 }
		
				 if ($paged < $pages && $showitems < $pages) $output .= "<li><a href='".get_permalink().'?pg='.($paged + 1).$url."' class='next page-numbers'>>></a></li>";
			 $output .= '</ul>';
		 $output .= "</nav>\n";
	 }
	 
	 return $output;
}
/*Código es el original de FESTIBOX
function obtener_fechas_festival_txt($id){
	$fecha_array = (get_post_meta( $id, 'custom_fecha', true ));
	$fecha_string_ant = '';
	$fecha_string_next = 'Aún por confirmar';
	$today = strtotime("now");
	foreach($fecha_array as $fecha){
		$fechas = explode(',',$fecha);
		$tamaño = sizeof($fechas);
		$fecha_string = '';
		$i = 0;
		$fin = 1;
		foreach ($fechas as $fecha2){
			$i++;
			$ii = 0;
			$tamaño2 = sizeof($fechas2);
			$fechas2 = explode('-',$fecha2);
			if ($i <= sizeof($fechas) && $i >1){
				$fecha_string .= ', ';
			}
			foreach ($fechas2 as $fecha3){
				$ii++;
				list($dia, $mes, $ano) = explode('/', $fecha3);
				$fecha4 = strtotime($ano.'-'.intval($mes).'-'.intval($dia));
				if ($ii == 2 && sizeof($fechas2) > 1){
					$fecha_string .= ' al '.$fecha3;
				}elseif ($ii == 1){
					$fecha_string .= $fecha3;
				}
				if ($fecha4 > $today){
					$fin = 0;
				}
			}
		}
		if ($fecha_string <> '' && $fin == 0){
			$fecha_string_next = $fecha_string;
		}elseif ($fecha_string <> ''){
			$fecha_string_ant = $fecha_string;
		}
	}
	return array('ant'=> $fecha_string_ant, 'next' => $fecha_string_next);
}
*/
/*-------------------------------------------------------------------------------------------------------------------
Función: obtener_fechas_festival_txt

Objetivo: Función que retorna el rango de fechas, en formato texto,del festival identificado por el parámetro ID 
en la tabla del posts.
-------------------------------------------------------------------------------------------------------------------*/
function obtener_fechas_festival_txt($id)
{
	//Recuperamos el rango de fechas (ealmacenado en el campo custom_fecha), para el post identificado como $id
	$fecha_array = (get_post_meta( $id, 'custom_fecha', true ));
	$fecha_string_ant = '';
	$fecha_string_next = 'Aún por confirmar';
	//Recuperamos la fecha actual en formato UNIX (segundos pasados desde el 01/01/1970 hasta la fecha actual)	
	$today = strtotime("now");
	//Para cada valor del rango de fechas recuperado
	foreach($fecha_array as $fecha)
	{		
		//Troceamos los valores del rango (que estan separados por comas), y los asignamos al array $fechas
		$fechas = explode(',',$fecha);
		//Inicializamos la variable $tamaño con el nº de elementos del array $fechas (NO ENTIENDO ESTO..., CREO QUE LA VARIABLE LOCAL NO SE UTILIZA)
		$tamaño = sizeof($fechas);
		//Inicializamos variables
		$fecha_string = '';
		$i = 0;
		$fin = 1;
		//Por cada valor del array $fechas
		foreach ($fechas as $fecha2)
		{
			$i++;
			$ii = 0;
			//Inicializamos la variable $tamaño2 con el nº de elementos del array $fechas2 (NO ENTIENDO ESTO..., CREO QUE LA VARIABLE LOCAL NO SE UTILIZA,..., 
			//ADEMÁS ESTA ASIGNADO ANTRES DE ASIGNARLE VALORES AL ARRAY fechas2)			
			$tamaño2 = sizeof($fechas2);
			//Separamos los valores del elemento (separados por guiones), y los asignamos al array fechas2
			$fechas2 = explode('-',$fecha2);
			if ($i <= sizeof($fechas) && $i >1)
			{
				$fecha_string .= ', ';
			}
			//Para cada una de las fechas del array fechas2
			foreach ($fechas2 as $fecha3)
			{
				$ii++;
				//Guarda el dia,mes y año de la fecha en variables independientes ($dia, $mes, $ano)
				list($dia, $mes, $ano) = explode('/', $fecha3);
				//Convierte la fecha en formato UNIX (es decir, segundos que han pasado desde el 01/01/1970 hasta la fecha marcada por $dia-$mes-$ano).
				$fecha4 = strtotime($ano.'-'.intval($mes).'-'.intval($dia));
				if ($ii == 2 && sizeof($fechas2) > 1)
				{
					$fecha_string .= ' al '.$fecha3;
				}
				elseif ($ii == 1)
				{
					$fecha_string .= $fecha3;
				}

				/*Si 
					los segundos pasados desde el 01/01/19770 hasta la fecha marcada por $dia-$mes-$ano 
				                                son mayores a
					los segundos pasados desde el 01/01/1970 hasta la fecha actual
				esto quiere decir que el festival aún no se ha celebrado, por lo que debemos mostrarlo
				*/
				if ($fecha4 > $today)
				{
					$fin = 0;
				}
			}
		}
		if ($fecha_string <> '' && $fin == 0)
		{
			$fecha_string_next = $fecha_string;
		}
		elseif ($fecha_string <> '')
		{
			$fecha_string_ant = $fecha_string;
		}
	}
	return array('ant'=> $fecha_string_ant, 'next' => $fecha_string_next);
}

/*-------------------------------------------------------------------------------------------------------------------
Función: obtener_fechas_festival

Objetivo: Función que retorna un array con las fechas en las que es posible disfrutar del festival, recibido con el 
identifcador $id
-------------------------------------------------------------------------------------------------------------------*/
function obtener_fechas_festival($id)
{
	//Recuperamos el campo CUSTOM_FECHA del post identificado por el parámetro $id
	$fecha_array = (get_post_meta( $id, 'custom_fecha', true ));
	//Recuperamos la fecha de hoy en formato UNIX
	$today = strtotime("now");
	//Inicializamos variables
	$fecha_ant = 0;
	$fecha_string_next = array();
	//Para cada uno de los valores del array $fecha_array
	foreach($fecha_array as $fecha)
	{
		//Separamos los valores del elemento $fecha (vienen separados por comas), y los asignamos al array $fechas
		$fechas = explode(',',$fecha);
		//Calculamos el tamaño del array $fechas (ESTA VARIABLE NO SE UTILIZA!!!)
		$tamaño = sizeof($fechas);
		//Declaramos un nuevo array
		$fecha_string = array();
		//Para cada uno de los valores del array $fechas
		foreach ($fechas as $fecha2)
		{
			$tamaño2 = sizeof($fechas2);
			//Separamos los valores del elemento $fecha2 (vienen separados por guiones), y los asignamos al array $fechas2
			$fechas2 = explode('-',$fecha2);
			//Inicializamos variables
			$fecha_ant = 0;
			$ii=0;
			//Para cada uno de los valores del array $fechas2
			foreach ($fechas2 as $fecha3)
			{
				//Separamos loa valores de dia, mes y año en variables independientes ($dia, $mes, $ano)
				list($dia, $mes, $ano) = explode('/', $fecha3);
				//Convertimos la fecha indicada por ($dia, $mes, $ano) en formato UNIX 
				$fecha4 = strtotime($ano.'-'.intval($mes).'-'.intval($dia));
				//Si la fecha marcada por ($dia, $mes, $ano) es mayor a la fecha actual
				if ($fecha4 > $today)
				{
					//Si la fecha a tratar es la fecha fin del período (hacemos tratamiento de las fechas entre medias de fecha inicial y final del período)
					if 	($ii == 1)
					{
						//Recuperamos el dia mes y año de la fecha inicial del período
						list($dia2, $mes2, $ano2) = explode('/', $fechas2[0]);
						//Guardamos en $fecha_ant la fecha inicial del período, en formato UNIX
						$fecha_ant = strtotime($ano2.'-'.intval($mes2).'-'.intval($dia2));
						//Mientras la fecha anterior más un día (86400 segundos), sea inferior a la fecha final del período
						while($fecha_ant + 86400 < $fecha4)
						{
							//Añadimos fecha a las fechas a pintar
							$fecha_string[] = date('d/m/Y', $fecha_ant + 86400);
							//Incrementamos la fecha_ant en un día
							$fecha_ant = $fecha_ant + 86400;
						}
					}
					//Añadimos fecha3 a las fechas a pintar (con esto conseguimos pintar la primera y la última fecha del período)
					$fecha_string[] = $fecha3;
				}
				//Con esta instrucción indicamos que la primera fecha ha sido tratada
				$ii++;
			}
		}
	}
	//Si, tras el bloque anterior no se ha recuperado ninguna fecha....
	if ($fecha_string[0] == '')
	{
		//El festival no tiene fechas, esta AÚN POR CONFIRMAR
		$fecha_string_next[] = 'Aún por confirmar';
	}
	//,..,si por el contrario se han recuperado fechas...
	else
	{
		//Asignamos las fechas a devolver
		$fecha_string_next = $fecha_string;
	}
	return $fecha_string_next;
}