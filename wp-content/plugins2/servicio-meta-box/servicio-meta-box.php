<?php 
/*
Plugin Name: Servicios en campos personalizados
Plugin URI: 
*/

function custom_servicios_type() {

// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Servicios', 'Post Type General Name', 'festibox' ),
		'singular_name'       => _x( 'Servicio', 'Post Type Singular Name', 'festibox' ),
		'menu_name'           => __( 'Servicios', 'festibox' ),
		'parent_item_colon'   => __( 'Servicio Padre', 'festibox' ),
		'all_items'           => __( 'Todos los servicios', 'festibox' ),
		'view_item'           => __( 'Ver servicio', 'festibox' ),
		'add_new_item'        => __( 'Añadir nuevo servicio', 'festibox' ),
		'add_new'             => __( 'Añadir nuevo', 'festibox' ),
		'edit_item'           => __( 'Editar servicio', 'festibox' ),
		'update_item'         => __( 'Actualizar servicio', 'festibox' ),
		'search_items'        => __( 'Buscar servicio', 'festibox' ),
		'not_found'           => __( 'No encontrado', 'festibox' ),
		'not_found_in_trash'  => __( 'No encontrado en la papelera', 'festibox' ),
	);

// Otras opciones para el tipo de entrada personalizada

	$args = array(
		'label'               => $labels,
		'description'         => __( 'Servicios Festibox', 'festibox' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( ),
		// Puedes asociar este CPT con una taxonomía por defecto o una taxonomía personalizada
		'taxonomies'          => array( ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);

	// Registro del tipo de entrada personalizada (Customs Post Type)
	register_post_type( 'servicio', $args );
}

add_action( 'init', 'custom_servicios_type', 0 );

//hook para crear el meta box
add_action( 'add_meta_boxes', 'em_mtbx_servicio' );

function em_mtbx_servicio() {

	// Creamos el meta box personalizado
	add_meta_box( 
                'em-servicio', // atributo ID
                'servicio', // Título
                'em_mtbx_servicio_function', // Función que muestra el HTML que aparecerá en la pantalla
                'servicio', // Tipo de entrada. Puede ser 'post', 'page', 'link', o 'custom_post_type'
                'normal', // Parte de la pantalla donde aparecerá. Puede ser 'normal', 'advanced', o 'side'
                'default' // Prioridad u orden en el que aparecerá. Puede ser 'high', 'core', 'default' o 'low'
                );
	
}

function listado($tipo, $value){
	  $values = explode(',',$value);
	  global $wpdb;
      $numposts = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = '".$tipo."'");
	  if (0 < $numposts) $numposts = number_format($numposts); 

	  $i= 1;
	  while($i <= $numposts){
		  wp_reset_postdata();
		  $args = array(
			  'numberposts' => -1,
			  'offset' => $i-1,
			  'post_type' => $tipo,
			  'post_status' => 'publish',
			  'order' => 'ASC',
			  'orderby' => 'title'
		  );
		  $wp_query = new WP_Query($args);
		  $tests_labs = $wp_query->posts;
		  if (!empty($tests_labs)) {
			  foreach ($tests_labs as $post) {
				  $id = $post -> ID;
				  $title = $post -> post_title;
				  $selected = "";
				  if (in_array($id, $values)){$selected = "selected";}
if( $tipo == 'product')
{
    $texto_sku = $wpdb->get_var("SELECT meta_value from $wpdb->postmeta WHERE meta_key =  '_sku' AND post_id = '".$id."'");
				  echo '<option value="'.$id.'" '.$selected.'>'.$texto_sku.'</option>';
}
else
{
echo '<option value="'.$id.'" '.$selected.'>'.$title.'</option>';
}
				  $i++;
			  }
		  }
	  }
}

function em_mtbx_servicio_function( $post ) {
		//si ya existe esa imagen obtemos su valor
		$f_productos = get_post_meta( $post->ID, 'f_productos', true );
		$f_festivales = get_post_meta( $post->ID, 'f_festivales', true );
		$f_entrada = get_post_meta( $post->ID, 'f_entrada', true );
		$f_nentrada = get_post_meta( $post->ID, 'f_nentrada', true );
		$f_alojamientos = get_post_meta( $post->ID, 'f_alojamientos', true );
		$f_noches = get_post_meta( $post->ID, 'f_noches', true );
		$f_otro = get_post_meta( $post->ID, 'f_otro', true );
		$f_otro2 = get_post_meta( $post->ID, 'f_otro2', true );
	  	
		$values = explode(',',$f_festivales);
		$f_festival = $f_concierto = $f_cenaespectaculo = $f_clasicaopera= $f_magiacirco = $f_danza = $f_monologos = $f_museos = $f_musicales = $f_teatro = '';
		foreach ($values as $value){
			if (get_post_type($value) == 'festival') {$f_festival .= ",".$value;}
			if (get_post_type($value) == 'concierto') {$f_concierto .= ",".$value;}
			if (get_post_type($value) == 'espectaculo') {$f_evento .= ",".$value;}
			if (get_post_type($value) == 'clasicaopera') {$f_clasicaopera .= ",".$value;}
			if (get_post_type($value) == 'magiacirco') {$f_magiacirco .= ",".$value;}
			if (get_post_type($value) == 'danza') {$f_danza .= ",".$value;}
			if (get_post_type($value) == 'monologos') {$f_monologos .= ",".$value;}
			if (get_post_type($value) == 'museos') {$f_museos .= ",".$value;}
			if (get_post_type($value) == 'musicales') {$f_musicales .= ",".$value;}
			if (get_post_type($value) == 'teatro') {$f_teatro .= ",".$value;}
			}
		
		 echo '<h2>post id... ' .$post->ID. ' </h2>';
		 ?>
        <h4><?php _e("Caja"); ?></h4>
        <select multiple name="f_productos_servicio[]">
        <?php  listado('product', $f_productos);?>
        </select>
        <h4><?php _e("Festival"); ?></h4>
        <select multiple name="f_festivales_servicio[]" size="10px">
        <?php listado('festival', $f_festival);?>
        </select>
        <h4><?php _e("Conciertos"); ?></h4>
        <select multiple name="f_concierto_servicio[]" size="10px">
        <?php listado('concierto', $f_concierto);?>
        </select>
        <h4><?php _e("Cena Espectáculo"); ?></h4>
        <select multiple name="f_evento_servicio[]" size="10px">
        <?php listado(espectaculo, $f_evento);?>
        </select>
        <h4><?php _e("Clásica y Ópera"); ?></h4>
        <select multiple name="f_clasicaopera_servicio[]" size="10px">
        <?php listado(clasicaopera, $f_clasicaopera);?>
        </select>
        <h4><?php _e("Magia y Circo"); ?></h4>
        <select multiple name="f_magiacirco_servicio[]" size="10px">
        <?php listado(magiacirco, $f_magiacirco);?>
        </select>
        <h4><?php _e("Danza"); ?></h4>
        <select multiple name="f_danza_servicio[]" size="10px">
        <?php listado(danza, $f_danza);?>
        </select>
        <h4><?php _e("Monólogos"); ?></h4>
        <select multiple name="f_monologos_servicio[]" size="10px">
        <?php listado(monologos, $f_monologos);?>
        </select>
        <h4><?php _e("Museos"); ?></h4>
        <select multiple name="f_museos_servicio[]" size="10px">
        <?php listado(museos, $f_museos);?>
        </select>
        <h4><?php _e("Musicales"); ?></h4>
        <select multiple name="f_musicales_servicio[]" size="10px">
        <?php listado(musicales, $f_musicales);?>
        </select>
        <h4><?php _e("Teatro"); ?></h4>
        <select multiple name="f_teatro_servicio[]" size="10px">
        <?php listado(teatro, $f_teatro);?>
        </select>
        <h4><?php _e("Tipo de Entradas"); ?></h4>
		<select name="f_entrada_servicio">';
			<option value="0" <?php if ($f_entrada == 0) echo 'selected="selected"';?>>Entrada Normal</option>
			<option value="1" <?php if ($f_entrada == 1) echo 'selected="selected"';?>>Entrada VIP</option>
			<option value="2" <?php if ($f_entrada == 2) echo 'selected="selected"';?>>Abono Normal</option>
			<option value="3" <?php if ($f_entrada == 3) echo 'selected="selected"';?>>Abono VIP</option>
		</select>
        <h4><?php _e("Nº Entradas"); ?></h4>
        <input class="em_mtbx_img" name="f_nentrada_servicio" id="f_entrada_servicio" type="text" value="<?php if($f_nentrada && $f_nentrada != '') echo $f_nentrada; ?>">
        <?php if (isset($f_alojamientos) && $f_noches > 0){?>
            <h4><?php _e("Alojamiento"); ?></h4>
            <select multiple name="f_alojamientos_servicio[]">
            <?php
	  		$festivales = explode(',',$f_festivales);
			$hoteles = explode(',',$f_alojamientos);
			foreach($festivales as $festival){
				$hoteles2 = obtener_hoteles_festival($festival);
				if ($festival <> ''){
					foreach($hoteles2 as $hotel){
						$selected = "";
						if (in_array($hotel, $hoteles)){$selected = "selected";}
						echo '<option value="'.$hotel.'" '.$selected.'>'.get_the_title($hotel).'</option>';
					}
				}
			}
			?>
            </select>
        <?php } ?>
        <h4><?php _e("Nº Noches"); ?></h4>
        <input class="em_mtbx_img" name="f_noches_servicio" id="f_noches_servicio" type="text" value="<?php if($f_noches && $f_noches != '') echo $f_noches; ?>">
        <h4><?php _e("OTROS - FESTIVAL"); ?></h4>
        <textarea  class="em_mtbx_img" name="f_otro_servicio" id="f_otro_servicio"><?php if($f_otro && $f_otro != '') echo $f_otro; ?></textarea>
        <h4><?php _e("OTROS - ALOJAMIENTO"); ?></h4>
        <textarea  class="em_mtbx_img" name="f_otro_servicio2" id="f_otro_servicio2"><?php if($f_otro2 && $f_otro2 != '') echo $f_otro2; ?></textarea>
        <div class="clear"></div>
          
	<?php       
}

//Para grabar los campos. Añadir uno nuevo por cada campo existente, en este caso tres.
add_action( 'save_post', 'em_mtbx_servicio_save_meta' );

function em_mtbx_servicio_save_meta( $post_id ) {

	if ( isset( $_POST['f_productos_servicio'] ) ) 
	{
		foreach ($_POST['f_productos_servicio'] as $f_producto){$f_productos .= ",".$f_producto;}
		update_post_meta( $post_id, 'f_productos', $f_productos.","  );

	}
	if  ( isset( $_POST['f_festivales_servicio'] )   || isset( $_POST['f_concierto_servicio'] ) || isset( $_POST['f_evento_servicio'] )    || 
	      isset( $_POST['f_clasicaopera_servicio'] ) || isset( $_POST['f_magiacirco_servicio'] )    || isset( $_POST['f_danza_servicio'] ) || 
	      isset( $_POST['f_monologos_servicio'] ) || isset( $_POST['f_museos_servicio'] )    || isset( $_POST['f_musicales_servicio'] )    ||
	      isset( $_POST['f_teatro_servicio'] ) 
		) 
	{
		foreach ($_POST['f_festivales_servicio'] as $f_festival){$f_festivales .= ",".$f_festival;}
		foreach ($_POST['f_concierto_servicio'] as $f_concierto){$f_festivales .= ",".$f_concierto;}
		foreach ($_POST['f_evento_servicio'] as $f_evento){$f_festivales .= ",".$f_evento;}
		foreach ($_POST['f_clasicaopera_servicio'] as $f_clasicaopera){$f_festivales .= ",".$f_clasicaopera;}
		foreach ($_POST['f_magiacirco_servicio'] as $f_magiacirco){$f_festivales .= ",".$f_magiacirco;}
		foreach ($_POST['f_danza_servicio'] as $f_danza){$f_festivales .= ",".$f_danza;}
		foreach ($_POST['f_monologos_servicio'] as $f_monologos){$f_festivales .= ",".$f_monologos;}
		foreach ($_POST['f_museos_servicio'] as $f_museos){$f_festivales .= ",".$f_museos;}
		foreach ($_POST['f_musicales_servicio'] as $f_musicales){$f_festivales .= ",".$f_musicales;}
		foreach ($_POST['f_teatro_servicio'] as $f_teatro){$f_festivales .= ",".$f_teatro;}

		update_post_meta( $post_id, 'f_festivales', $f_festivales.","  );

	}
	if ( isset( $_POST['f_entrada_servicio'] ) ) {
	
		update_post_meta( $post_id, 'f_entrada', $_POST['f_entrada_servicio']  );

	}
	if ( isset( $_POST['f_nentrada_servicio'] ) ) {
	
		update_post_meta( $post_id, 'f_nentrada', $_POST['f_nentrada_servicio']  );

	}
	if ( isset( $_POST['f_alojamientos_servicio'] ) ) {
		foreach ($_POST['f_alojamientos_servicio'] as $f_alojamiento){$f_alojamientos .= ",".$f_alojamiento;}
		update_post_meta( $post_id, 'f_alojamientos', $f_alojamientos.","  );

	}
	if ( isset( $_POST['f_noches_servicio'] ) ) {
	
		update_post_meta( $post_id, 'f_noches', $_POST['f_noches_servicio']  );

	}
	if ( isset( $_POST['f_otro_servicio'] ) ) {
	
		update_post_meta( $post_id, 'f_otro', $_POST['f_otro_servicio']  );

	}
	if ( isset( $_POST['f_otro_servicio2'] ) ) {
	
		update_post_meta( $post_id, 'f_otro2', $_POST['f_otro_servicio2']  );

	}
	
}