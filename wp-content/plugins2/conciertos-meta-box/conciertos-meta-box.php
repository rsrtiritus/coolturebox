<?php 
/*
Plugin Name: Conciertos en campos personalizados
Plugin URI:
*/

//hook para crear el meta box principal de los Conciertos
function custom_conciertos_type() {

// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Conciertos', 'Post Type General Name', 'festibox' ),
		'singular_name'       => _x( 'Concierto', 'Post Type Singular Name', 'festibox' ),
		'menu_name'           => __( 'Conciertos', 'festibox' ),
		'parent_item_colon'   => __( 'Concierto Padre', 'festibox' ),
		'all_items'           => __( 'Todos los conciertos', 'festibox' ),
		'view_item'           => __( 'Ver concierto', 'festibox' ),
		'add_new_item'        => __( 'Añadir nuevo concierto', 'festibox' ),
		'add_new'             => __( 'Añadir nuevo', 'festibox' ),
		'edit_item'           => __( 'Editar concierto', 'festibox' ),
		'update_item'         => __( 'Actualizar concierto', 'festibox' ),
		'search_items'        => __( 'Buscar concierto', 'festibox' ),
		'not_found'           => __( 'No encontrado', 'festibox' ),
		'not_found_in_trash'  => __( 'No encontrado en la papelera', 'festibox' ),
	);

// Otras opciones para el tipo de entrada personalizada

	$args = array(
		'label'               => $labels,
		'description'         => __( 'Conciertos', 'festibox' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'thumbnail', 'comments','page_layout','author' ),
		// Puedes asociar este CPT con una taxonomía por defecto o una taxonomía personalizada
		'taxonomies'          => array( 'caja','cuando','edad','provincia'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);

	// Registro del tipo de entrada personalizada (Customs Post Type)
	register_post_type( 'concierto', $args );
}

add_action( 'init', 'custom_conciertos_type', 0 );

add_action( 'add_meta_boxes', 'em_mtbx_concierto' );

function em_mtbx_concierto() {

	// Creamos el meta box personalizado
	add_meta_box( 
                'Datos_Concierto', // atributo ID
                'Conciertos', // Título
                'em_mtbx_concierto_function', // Función que muestra el HTML que aparecerá en la pantalla
                'concierto', // Tipo de entrada. Puede ser 'post', 'page', 'link', o 'custom_post_type'
                'normal', // Parte de la pantalla donde aparecerá. Puede ser 'normal', 'advanced', o 'side'
                'high' // Prioridad u orden en el que aparecerá. Puede ser 'high', 'core', 'default' o 'low'
                );
	
	// Creamos el meta box personalizado
	add_meta_box( 
                'Usuarios_concierto', // atributo ID
                'Usuarios Conciertos', // Título
                'em_mtbx_evento_usuario', // Función que muestra el HTML que aparecerá en la pantalla
                'concierto', // Tipo de entrada. Puede ser 'post', 'page', 'link', o 'custom_post_type'
                'normal', // Parte de la pantalla donde aparecerá. Puede ser 'normal', 'advanced', o 'side'
                'high' // Prioridad u orden en el que aparecerá. Puede ser 'high', 'core', 'default' o 'low'
                );
}

function em_mtbx_concierto_function( $post ) {
    
    $f_hora = get_post_meta( $post->ID, 'f_hora', true );
    $f_recinto = get_post_meta( $post->ID, 'f_recinto', true );
    $f_localidad = get_post_meta( $post->ID, 'f_localidad', true );
    $f_direccion = get_post_meta( $post->ID, 'f_dirección', true );
    $f_nentrada = get_post_meta( $post->ID, 'f_nentrada', true );
     ?>

         <h4><?php _e("Hora"); ?></h4>
         <input class="em_mtbx_img" name="f_hora" id="f_hora" type="text" value="<?php if($f_hora && $f_hora != '') echo $f_hora; ?>">
         <h4><?php _e("Recinto"); ?></h4>
         <input class="em_mtbx_img" name="f_recinto_festival" id="f_recinto_festival" type="text" value="<?php if($f_recinto && $f_recinto != '') echo $f_recinto; ?>">
         <h4><?php _e("Localidad"); ?></h4>
         <input class="em_mtbx_img" name="f_localidad_festival" id="f_localidad_festival" type="text" value="<?php if($f_localidad && $f_localidad != '') echo $f_localidad; ?>">
         <h4><?php _e("Dirección"); ?></h4>
         <input class="em_mtbx_img" name="f_direccion_festival" id="f_direccion_festival" type="text" value="<?php if($f_direccion && $f_direccion != '') echo $f_direccion; ?>">
         <h4><?php _e("Número de entradas disponibles"); ?></h4>
         <input class="em_mtbx_img" name="f_nentrada" id="f_nentrada" type="text" value="<?php if($f_nentrada && $f_nentrada != '') echo $f_nentrada; ?>">
          <div class="clear"></div>
          
	<?php       
}

//Para grabar los campos. Añadir uno nuevo por cada campo existente, en este caso tres.
add_action( 'save_post', 'em_mtbx_concierto_save_meta' );

function em_mtbx_concierto_save_meta( $post_id ) {

	if ( isset( $_POST['f_hora'] ) ) {
	
		update_post_meta( $post_id, 'f_hora', $_POST['f_hora']  );

	}
	if ( isset( $_POST['f_recinto_festival'] ) ) {
	
		update_post_meta( $post_id, 'f_recinto', $_POST['f_recinto_festival']  );

	}
	if ( isset( $_POST['f_localidad_festival'] ) ) {
	
		update_post_meta( $post_id, 'f_localidad', $_POST['f_localidad_festival']  );

	}
	if ( isset( $_POST['f_direccion_festival'] ) ) {
	
		update_post_meta( $post_id, 'f_dirección', $_POST['f_direccion_festival']  );

	}
	if ( isset( $_POST['f_nentrada'] ) ) {
	
		update_post_meta( $post_id, 'f_nentrada', $_POST['f_nentrada']  );

	}
}

function comprobar_nentradas($data){
	$resul = 'NOK';
	update_nreservas($data);
	if( (get_post_meta($data, 'f_nreservas', true) < get_post_meta($data, 'f_netradas', true)) || get_post_meta($data, 'f_netradas', true) == '' || get_post_meta($data, 'f_netradas', true) == 0 ){
		$resul = 'OK';
	}
	return $resul;
}

function update_nreservas($data){
	global $wpdb;
	$consulta = "SELECT * FROM wp_reservas WHERE festival = ".$data." ORDER BY fecha_in ASC";
	$signs = $wpdb->get_results($consulta);
	$i=0;
	foreach ($signs as $sign) {
		$i++;
	}
	if( get_post_meta($data, 'f_nreservas', true) <> $i ){
		update_post_meta( $data, 'f_nreservas', $i  );
	}
}