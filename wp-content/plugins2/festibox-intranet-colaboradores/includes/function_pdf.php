<?php

function eventos_pdf($festival){

	require('fpdf/fpdf.php');

	class PDF extends FPDF
	{
	  //Cabecera de página
	  function Header()
	  {
	    
		$this->Image('logo.png', 10, 8, 33, 33, 'PNG', 'http://www.festibox.com');
	  
		$this->SetFont('Arial','B',12);
	  
//		$this->Cell(30,10,'Lista de asistentes',1,0,'C');
	  
	  }
	  //Pie de página
	  function Footer()
	  {
	  
		  $this->SetY(-10);
		  
		  $this->SetFont('Arial','I',8);
		  
		  $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	  }
	  //Tabla coloreada
	  function tabla($reservas,$titulo){
  
		  $this->Cell(190,6,utf8_decode('Lista de asistentes para '.get_the_title($festival)),'',0,'C',0);
		  $this->Ln();
		  $this->Cell(190,0,'','');
		  $this->Ln();
		  $this->Cell(190,0,'','');
		  $this->Ln();
	  //Colores, ancho de línea y fuente en negrita
		  $this->SetFillColor(255,0,0);
		  $this->SetTextColor(255);
		  $this->SetDrawColor(128,0,0);
		  $this->SetLineWidth(.3);
//		  $this->SetFont('','B');
		  //Cabecera		  
		  $this->Cell(10,6,utf8_decode('Nº'),1,0,'C',1);
		  $this->Cell(55,6,'Nombre',1,0,'C',1);
		  $this->Cell(75,6,'Apellidos',1,0,'C',1);
		  $this->Cell(30,6,utf8_decode('Código'),1,0,'C',1);
		  $this->Cell(20,6,'Fecha Entrada',1,0,'C',1);
		  $this->Cell(20,6,'Fecha Fin',1,0,'C',1);
		  $this->Ln();
		  //Restauración de colores y fuentes
		  $this->SetFillColor(224,235,255);
		  $this->SetTextColor(0);
		  $this->SetFont('');
		  //Datos
		  $fill=false;
		  
		  $ii=1;
		  
		  $reservas = obtener_reserva( array( 'festival' => $festival ) );
		  foreach ($reservas as $reserva){
			  $usuario = 
			  $this->Cell(10,6,$ii,'LR',0,'C',$fill);
			  $this->Cell(55,6,utf8_decode($current_user->user_firstname),'LR',0,'L',$fill);
			  $this->Cell(75,6,utf8_decode($current_user->user_lastname),'LR',0,'L',$fill);
			  $this->Cell(30,6,utf8_decode($reserva['codigo']),'LR',0,'C',$fill);
			  $this->Cell(20,6,utf8_decode($reserva['fecha_in']),'LR',0,'C',$fill);
			  $this->Cell(20,6,utf8_decode($reserva['fecha_end']),'LR',0,'C',$fill);
			  $this->Ln();
			  $fill=!$fill;
			  $ii++;
		  }
		  $this->Cell(190,0,'','T');
	  }
	}	

	$pdf=new PDF();
	$pdf->AliasNbPages();
	//Segunda página
	$pdf->AddPage();
	$pdf->SetY(65);
	$pdf->tabla($reservas,$titulo);
	$pdf->Output($titulo.'.pdf','I');
}