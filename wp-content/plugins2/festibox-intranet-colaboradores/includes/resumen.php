<?php
//Mosramos el resumen de los colaboradores.
function reserva_colaboradores_resumen_short(){
	global $user_ID;
	$current_user = wp_get_current_user();
	
	if ( is_user_logged_in() && (  current_user_can( 'festibox' ) || current_user_can( 'colaborador' ) || current_user_can( 'colaborador' ) ) ) {
	    $festivales = get_user_meta( $user_ID, '_referencias', true );
		
		if ($festivales != ''){
		?>
			<div class="woocommerce reservas_historico_colaborador">
            	<h2>Tus referencias</h2>
            	<table class="shop_table shop_table_responsive referencias_colaboradores">
                	<thead><tr>
                    	<th class="referencia_imagen"></th>
                    	<th class="referencia_festival">Festival</th>
                    	<th class="referencia_reservas">Reservas</th>
                    	<th class="referencia_accion"></th>
                  	</tr></thead>
                    <tbody>
					<?php
					foreach($festivales as $festival){
						?>
						<tr style="text-align:left">
				  			<td class="referencia_imagen" style="text-align:left">
								<?php echo get_the_post_thumbnail( $festival, 'thumbnail' );?>
				  			</td>
				  			<td class="referencia_titulo" style="text-align:left">
						  		<?php echo get_the_title($festival);?>
				  			</td>
				  			<td class="referencia_contador" style="text-align:left">
							<?php
                            $reservas = obtener_reserva(array('festival' => $festival, 'select' => 'COUNT(ID) AS contador' ) );
							echo $reservas[0] -> contador;
							?>
				  			</td>
				  			<td class="referencia_accion">
								<?php if ($reservas[0] -> contador > 0){?>
                                    <a href="/wp-content/plugins/festibox-intranet-colaboradores/pdf.php?fv=<?php echo $festival;?>" class="button descargar">Descargar</a>
								<?php }?>
				  			</td>
				  		</tr>
					<?php }?>
					</tbody>
                </table>
           	</div>
		<?php
		}
	}
}