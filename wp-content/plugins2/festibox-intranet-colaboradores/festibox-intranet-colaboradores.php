<?php
/* Plugin Name: Intranet Colaboradores Festibox
 * Description: Este plugin proporciona una intranet para los colaboradores de Festibox.
 * Author: Cuatro Notas
 * Author URI: http://cuatronotas.com
 * Version: 1.0
 */

/*
Metemos las Inludes.
*/
include('includes/roles.php');
include('includes/resumen.php');

/*
Asignamos los roles.
*/
add_role( 'colaborador', __('Colaborador de Festibox', 'your-plugin-textdomain' ), $capabilities  );

/*
Decidimos como metemos el resumne. .
*/
add_shortcode('reserva_colaboradores_resumen','reserva_colaboradores_resumen_short');
add_action( 'woocommerce_before_my_account','reserva_colaboradores_resumen_short');