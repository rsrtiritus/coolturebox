<?php
/* Plugin Name: Intranet Festibox Clientes
 * Description: Este plugin proporciona una Intrante para los clientes de Festibox
 * Author: Cuatro Notas
 * Author URI: http://cuatronotas.com
 * Version: 1.0
 */

/*
Decidimos como metemos el resumne. .
*/
add_action( 'init', 'reservas_my_account');
function reservas_my_account(){
	if ( is_user_logged_in() && current_user_can( 'customer' ) )
		add_action( 'woocommerce_after_my_account','gestion_hacer_reservas_short');
}

/*
Redireccionamos una vez que se ha salido de la página.
*/
add_action('wp_logout','festibox_redirect_logout');
function festibox_redirect_logout(){
	wp_redirect( home_url() );
	exit();
}


