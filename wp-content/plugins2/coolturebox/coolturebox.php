<?php
/*
Plugin Name: Plugin de Coolturebox
Plugin URI: 
*/

//* Cambia el texto del botón Añadir al carrito en WooCommerce en un producto individual
add_filter( 'add_to_cart_text', 'texto_personalizado_boton_anadir_carrito' );                                // versiones menores a 2.1
add_filter( 'woocommerce_product_single_add_to_cart_text', 'texto_personalizado_boton_anadir_carrito' );    // 2.1 o superior
 
function texto_personalizado_boton_anadir_carrito() {
 
        return __( 'Comprar', 'woocommerce' );
 
}

//* Cambia el texto del botón Añadir al carrito en WooCommerce en la categorías de productos
add_filter( 'add_to_cart_text', 'texto_personalizado_boton_anadir_carrito_archivo' );                        // versiones menores a 2.1
add_filter( 'woocommerce_product_add_to_cart_text', 'texto_personalizado_boton_anadir_carrito_archivo' );    // 2.1 o superior
 
function texto_personalizado_boton_anadir_carrito_archivo() {
 
        return __( 'Comprar', 'woocommerce' );
}

remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);

function add_wpcf7_scripts() {
    if ( is_page('21720') || is_page('22136'))
        wpcf7_enqueue_scripts();
}
if ( ! is_admin() && WPCF7_LOAD_JS )
    remove_action( 'init', 'wpcf7_enqueue_scripts' );
add_action( 'wp', 'add_wpcf7_scripts' );

function limpiar($String){
    $String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
    $String = str_replace(array('í','ì','î','ï'),"i",$String);
    $String = str_replace(array('é','è','ê','ë'),"e",$String);
    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
    $String = str_replace("ç","c",$String);
    $String = str_replace("Ç","C",$String);
    $String = str_replace("ñ","n",$String);
    $String = str_replace("Ñ","N",$String);
    $String = str_replace("Ý","Y",$String);
    $String = str_replace("ý","y",$String);
     
    $String = str_replace("&aacute;","a",$String);
    $String = str_replace("&Aacute;","A",$String);
    $String = str_replace("&eacute;","e",$String);
    $String = str_replace("&Eacute;","E",$String);
    $String = str_replace("&iacute;","i",$String);
    $String = str_replace("&Iacute;","I",$String);
    $String = str_replace("&oacute;","o",$String);
    $String = str_replace("&Oacute;","O",$String);
    $String = str_replace("&uacute;","u",$String);
    $String = str_replace("&Uacute;","U",$String);
    return $String;
}

function pdflink($attr, $content) {
    return '<a class="pdf" href="http://docs.google.com/viewer?url='.$attr['href'].'" target="new">'.$content.'</a>';
}
add_shortcode('pdf','pdflink');

function crear_mis_taxonomias() {
  register_taxonomy('estilo', array('espectaculo','museos'), array(
  'hierarchical' => false, 'label' => 'Categoría',
  'query_var' => true, 'rewrite' => true));

  register_taxonomy('subcategoria', array('festival','concierto','danza'),array(
  'hierarchical' => false, 'label' => 'Estilo',
  'query_var' => true, 'rewrite' => true));

  register_taxonomy('categoria', array('festival','museos','monologos','concierto','danza','musicales','clasicaopera','magiacirco','teatro'),array(
  'hierarchical' => false, 'label' => 'Categoría',
  'query_var' => true, 'rewrite' => true));


//Modificamos la taxonomía idioma para que se tenga en cuenta en las entradas de tipo clasicaopera
  register_taxonomy('idioma', array('teatro','magiacirco','monologos','musicales'), array(
  'hierarchical' => false, 'label' => 'Idioma',
  'query_var' => true, 'rewrite' => true));


    register_taxonomy('cuando', array('concierto','clasicaopera','festival'), array(
  'hierarchical' => false, 'label' => 'Cuándo',
  'query_var' => true, 'rewrite' => false,'show_ui'=>true));


/*
(30/11/2016)-->No sé por qué el campo CUANDO sigue apareciendo en las entradas de ESPECTACULO. Como solución temporal
lo ocultamos (sin eliminarlo)
*/
/*
  register_taxonomy('cuando', array('espectaculo'), array(
  'hierarchical' => false, 'label' => 'Cuándo',
  'query_var' => true, 'rewrite' => false,'show_ui'=>false));
*/
  register_taxonomy('audiencia', array('danza'), array(
  'hierarchical' => false, 'label' => 'Tipo de Audiencia',
  'query_var' => true, 'rewrite' => true));

  register_taxonomy('edad', array('festival','espectaculo','concierto','teatro','magiacirco','clasicaopera','monologos','musicales'), array(
  'hierarchical' => false, 'label' => 'Edad',
  'query_var' => true, 'rewrite' => true));

  register_taxonomy('comunidadautonoma', array('festival','espectaculo','concierto','teatro','museos','magiacirco','clasicaopera','danza','monologos','musicales'), array(
  'hierarchical' => false, 'label' => 'Comunidad Autónoma',
  'query_var' => true, 'rewrite' => true));  
  
  register_taxonomy('provincia', array('festival','espectaculo','concierto','teatro','museos','magiacirco','clasicaopera','danza','monologos','musicales'), array(
  'hierarchical' => false, 'label' => 'Provincia',
  'query_var' => true, 'rewrite' => true));  
/*
  register_taxonomy('duracion', array('festival'), array(
  'hierarchical' => false, 'label' => 'Duración (Filtro)',
  'query_var' => true, 'rewrite' => true));
*/
/*
  register_taxonomy('artista', array('festival','espectaculo','concierto','teatro',museos','magiacirco','clasicaopera','danza','monologos','musicales'), array(
  'hierarchical' => false, 'label' => 'Artista',
  'query_var' => true, 'rewrite' => true));
*/
}
add_action('init', 'crear_mis_taxonomias', 0);

function add_google_analytics() {
    echo '<script src="http://www.google-analytics.com/ga.js" type="text/javascript"></script>';
    echo '<script type="text/javascript">';
    echo 'var pageTracker = _gat._getTracker("UA-48405584-6");';
    echo 'pageTracker._trackPageview();';
    echo '</script>';
}
add_action('wp_footer', 'add_google_analytics');
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
/*
Función comprobar_code
Valida la existencia del código de reserva indicado en la web
*/
function comprobar_code($code)
{
	global $wpdb_ws;
	require_once(ABSPATH . 'ws_conect.php');
    $signs = $wpdb_ws->get_results("SELECT codigo, reserv, valid, activ FROM ws WHERE cod_reser = '".$code."' AND 1 = 1");
 
	foreach ($signs as $sign) 
	{
		$codigo = $sign->codigo;
		$reserv = $sign->reserv;
		$valid = $sign->valid;
		$activo = $sign->activ;
	}
	$productos =  obteneter_producto($codigo);
	$result = array('cod_reser' => $code, 'codigo' => $codigo, 'reserv' => $reserv, 'productos' => $productos[0], 'valid' => $valid, 'activ' => $activo);
	return $result;
}
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------

function obteneter_producto($data) {

	$productos = array();
    $args = array(
        'posts_per_page' => '-1',
		'order' => 'ASC',
		'orderby' => 'title',
        'post_type' => 'product',
        'post_status' => 'publish',
		'meta_query' => array(
			array(
				'key' => 'f_producto',
				'value' => substr($data, 2, 2),
				'compare' => 'LIKE'
			)
		)
    );

    $wp_query = new WP_Query($args);

    $tests_labs = $wp_query->posts;

    if (!empty($tests_labs)) {
        foreach ($tests_labs as $post) {
			$id = $post -> ID;
        	$title = $post -> post_title;
			if (!in_array($id, $productos)){
				$productos[] = $id;
			}
    	}
    }else{
        $productos[0] = '';
	}
    return $productos;
}

function obteneter_servicios($producto, $festival, $hotel) {

	$servicios = array();
	$servicios2 = array();
	$productos = array();
	$festivales = array();
	$alojamientos = array();
	$entradas = array();
	$producto_array = '';
	$festival_array = '';
	$alojamiento_array = '';
	$precio = array();

	$args2= array('relation' => 'AND');

	if($producto <> ''){
		$args2[] =  array('key' => 'f_productos','value' => ','.$producto.',','compare' => 'LIKE');
	}
	if($festival <> ''){
		$args2[] =  array('key' => 'f_festivales','value' => ','.$festival.',','compare' => 'LIKE');
	}
	if($hotel <> ''){
		$args2[] =  array('key' => 'f_alojamientos','value' => ','.$hotel.',','compare' => 'LIKE');
	}

	$args = array(
		'posts_per_page' => '-1',
		'order' => 'ASC',
		'orderby' => 'title',
		'post_type' => 'servicio',
		'post_status' => 'private',
		'meta_query' => $args2
	);
	
	$wp_query = new WP_Query($args);

    $tests_labs = $wp_query->posts;

    if (!empty($tests_labs)) {
        foreach ($tests_labs as $post) {
			$id = $post -> ID;
			$productos = explode(',',get_post_meta($id, 'f_productos', true));
			$festivales = explode(',',get_post_meta($post->ID, 'f_festivales', true));
			$entradas = explode(',',get_post_meta($post->ID, 'f_entrada', true));
			$nentrada = get_post_meta($post->ID, 'f_nentrada', true);
			$alojamientos = explode(',',get_post_meta($post->ID, 'f_alojamientos', true));
			$noches = get_post_meta($post->ID, 'f_noches', true);
			$otro = get_post_meta($post->ID, 'f_otro', true);
			$otro2 = get_post_meta($post->ID, 'f_otro2', true);
			if ($id <> ''){
       			foreach ($productos as $producto2) {
					if($producto2<>''){
        				foreach ($festivales as $festival2) {
							if($festival2<>'' && get_post_status($festival2) == 'publish'){
       							foreach ($entradas as $entrada) {
									if($entrada<>''){
										$servicios2['id'] = $id;
										$servicios2['producto'] = $producto2;
										$servicios2['festivales'] = $festival2;
										$servicios2['entrada'] = $entrada;
										$servicios2['nentrada'] = $nentrada;
										$servicios2['alojamientos'] = get_post_meta($post->ID, 'f_alojamientos', true);
										$servicios2['noches'] = $noches;
										$servicios2['otro'] = $otro;
										$servicios2['otro2'] = $otro2;
										$servicios2['precio'] = get_post_meta($producto2, 'f_precio', true);
										$precio[] = get_post_meta($producto2, 'f_precio', true);
										$servicios[] = $servicios2;

//										if ($noches <> '0' && $noches <> ''){
//											foreach ($alojamientos as $alojamiento) {
//												if($alojamiento <> ''  && get_post_status($alojamiento) == 'publish'){
//													$servicios2['id'] = $id;
//													$servicios2['producto'] = $producto2;
//													$servicios2['festivales'] = $festival2;
//													$servicios2['entrada'] = $entrada;
//													$servicios2['nentrada'] = $nentrada;
//													$servicios2['alojamientos'] = $alojamiento;
//													$servicios2['noches'] = $noches;
//													$servicios2['otro'] = $otro;
//													$servicios2['otro2'] = $otro2;
//													$servicios[] = $servicios2;
//												}
//											}
//										}else{
//											$servicios2['id'] = $id;
//											$servicios2['producto'] = $producto2;
//											$servicios2['festivales'] = $festival2;
//											$servicios2['entrada'] = $entrada;
//											$servicios2['nentrada'] = $nentrada;
//											$servicios2['alojamientos'] = '';
//											$servicios2['noches'] = $noches;
//											$servicios2['otro'] = $otro;
//											$servicios2['otro2'] = $otro2;
//											$servicios[] = $servicios2;
//										}
									}
								}
							}
						}
					}
				}
			}
    	}
    }else{
        $servicios[0] = '';
	}
	array_sort($servicios,'precio'); //DESC = !
	return $servicios;
}

/*
Funcion reservashort
Se utiliza en la página www.coolturebox.com/utilizacion con el shortcode [reserva]
*/
function reserva_short($attr, $content)
{
	//Inicializacion/creacion de variables
	$codigo = '';
	$msg = '';
	$confrimar = 0;	
	//
	//Bloque que se ejecuta cuando se ha introducido valor en el cuadro de texto CODIGO
	if (isset($_GET['codigo']) || $_GET['codigo'] <> '')
	{
		$codigo = $_GET['codigo'];
		$codigo2 = comprobar_code($codigo);			
		$url = '?codigo='.$codigo;
		if (isset($_GET['fv']) || $_GET['fv'] <> '') 
		{
			$url .= '&fv='.$_GET['fv'];
		}
		$nnoches = '';
		if (isset($_GET['sv']) || $_GET['sv'] <> '')
		{
			$url .= '&sv='.$_GET['sv'];
			$nnoches = get_post_meta($_GET['sv'], 'f_noches', true);
		}
		if (isset($_GET['ht']) || $_GET['ht'] <> '') $url .= '&ht='.$_GET['ht'];
		if (isset($_GET['ff']) || $_GET['ff'] <> '') $url .= '&ff='.$_GET['ff'];
		if (isset($_GET['hf']) || $_GET['hf'] <> '') $url .= '&hf='.$_GET['hf'];
			
		if ($codigo2['codigo'] == '')
		{	
			$msg = 'El código introducido es erroneo o ha sucedido un error. Vuelva a intentarlo o póngase encontacto con nostros para solucionarlo.';
		}
		else if ($codigo2['activ'] == 0) 
		{
			$msg   = 'El código introducido aún no ha sido activado. Si tiene cualquier duda póngase en contacto con nosotros.';
		}
		else if ($codigo2['activ'] == 98)
		{
			$msg  = 'Lo sentimos, este código de reserva ha caducado. Si tiene cualquier duda póngase en contacto con nosotros.';
		}
		else if (($codigo2['reserv'] == 1) || ($codigo2['activ'] == 99))
		{
			$msg  = 'Lo sentimos, este código de reserva ya ha sido utilizado. Si tiene cualquier duda póngase en contacto con nosotros.';
		}
	}
	//
	//Bloque que se ejecuta la primera vez que se entra en la pantalla, es decir, cuando no hay código, ni confirmación del mismo, ni mensaje de error
	if ((!isset($_GET['codigo']) && !isset($_GET['confirmar']) && !isset($_GET['confirmado'])) || $msg <> '' )
	{
		//Pintamos en la pantalla la parte variable de la web
		echo '<div style="inline-blok">';
		echo '<div style="text-align: center;">';
		echo '<p style="text-align:left">'.$content.'</p>';;
		echo '<h2>INSERTA TU CÓDIGO</h2>';
		echo '<form method="GET" action="'.get_permalink().'">';
		echo '<input type="text" name="codigo" required="" value="'.$codigo.'" style="display: inline-block;margin-right: 20px;" />';
		echo '<input type="submit" value="Continuar"/>';
		echo '<p style="text-align:center">Si no sabes donde encontrar tu código de reserva, pincha <a href="http://www.coolturebox.com/wp-content/uploads/2017/01/CHEQUE_EJ_CODIGO.jpg" title="Código de reserva" rel="lightbox">aquí</a>.</p>';
		echo '<p style="text-align:center;color: #ff0000;">'.$msg.'</p>';
		echo '</form></div>';
		echo '</div>';
	}
	//Bloque se ejecuta cuando se ha introducido el código de reserva y se ha pulsado el botón CONTINUAR
	elseif (isset($_GET['codigo']) && !isset($_GET['confirmar']) && !isset($_GET['confirmado']))
	{
		//Pintamos información relativa al producto		
		echo '<div style="display:inline-block;vertical-align: top;padding-left:3px;padding-right:3px;text-align: center;">';
		echo '<h3>Producto</h3>';
		echo '<h4>'.get_the_title($codigo2['productos']).'</h4>';
		echo get_the_post_thumbnail($codigo2['productos'], 'thumbnail' );
		echo '</div>';
		echo '<div style="display:inline-block;vertical-align: top;padding-left:3px;padding-right:3px;text-align: center;">';
		echo '<h3>Nº Cheque</h3>';
		echo '<h4>'.$codigo.'</h4>';
		echo '<h3>Fecha de caducidad</h3>';
		//echo '<h4>'.$codigo2['valid'].'</h4>';
		echo '<h4>'.substr($codigo2['valid'], 8,2). '-' .substr($codigo2['valid'], 5,2). '-' .substr($codigo2['valid'], 0,4);
		//Bloque que se encarga de repintar la pantalla una vez que el usuario ha pulsado el botón SELECCIONA TU FESTIVAL
		if (isset($_GET['ff']) &&  isset($_GET['ff']) <> '')
		{
			echo '<h3>Evento Fecha</h3>';
			$fechas = obtener_fechas_festival($_GET['fv']);
			if($_GET['ff'] == 'ABONO')
			{
				echo '<h4>ABONO</h4>';
			}else
			{
				echo '<h4>'.$fechas[$_GET['ff']].'</h4>';
			}
		}
		if (isset($_GET['ht']) &&  $_GET['ht'] <> '')
		{
			echo '<h3>Entrada en el Hotel</h3>';
			echo '<h4>Fechas a confirmar según disponibilidad</h4>';
		}
		echo '</div>';
		//Bloque que se pinta una vez que se introduce el código de reserva y se hace clic sobre el botón CONTINUAR
		if ($codigo2['reserv'] == 0 && !isset($_GET['confirmar']))
		{
			//Si aún no se ha seleccionado festival....
			if (!isset($_GET['fv']) || $_GET['fv'] == '')
			{
				//Pintamos los controles necesario para que el usuario SELECCIONE FESTIVAL-->SHORTCODE reserva-festivales Esta codificado en el plugin de festivales
				echo '<div style="display:inline-block;vertical-align: top;padding-left:3px;padding-right:3px;text-align: center;">';
				echo '<h3>Estado</h3>';
				echo '<h4>libre</h4>';
				echo '<a class="btn active big filled" href="'.get_site_url().'/reserva-festivales/'.$url.'"><span>Selecciona tu Evento</span></a>';
				echo '</div>';
			}
			elseif (isset($_GET['fv']))
			{
				$attachID = get_post_meta( $_GET['fv'], 'custom_f_logo', true );
				echo '<a href="'.get_site_url().'/reserva-festivales/'.$url.'" class="festival_logo"  style="display:inline-block;vertical-align: top;padding-left:3px;padding-right:3px;text-align:center;padding-top: 0px;">';
				echo '<h3>Evento</h3>';
				echo '<h4>'.get_the_title($_GET['fv']).'</h4>';
				foreach($attachID as $item)
				{
					$imagen = wp_get_attachment_image_src($item, 'thumbnail');
					echo  '<img src="'.$imagen[0].'" width="'.$imagen[1].'" height="'.$imagen[2].'" class="attachment-post-thumbnail wp-post-image" alt="'.get_post_meta($item, '_wp_attachment_image_alt', true).'"/>';
				}
				echo '</a>';
				if (!isset($_GET['ff']) &&  isset($_GET['ff']) == '')
				{
					$fechas = obtener_fechas_festival( $_GET['fv']);
					echo '<form type="GET" style="display:inline-block;vertical-align: top;padding-left:3px;padding-right:3px;text-align:center;padding-top: 0px;">';
					echo '<h3>Fecha del Evento</h3>';
					echo '<select name="ff" style="margin:6px;display:inline-block;padding-right:30px;">';
					$i=0;
					if (get_post_meta($_GET['sv'], 'f_entrada', true) == 0 || get_post_meta($_GET['sv'], 'f_entrada', true) == 1)
					{
						foreach($fechas as $fecha)
						{
							echo '<option value="'.$i.'">'.$fecha.'</option>';
							$i++;
						}
					}
					else
					{
						echo '<option value="ABONO">ABONO</option>';
					}
					echo '</select>';
					echo '<input type="hidden" name="fv" value="'.$_GET['fv'].'"/>';
					echo '<input type="hidden" name="codigo" value="'.$_GET['codigo'].'"/>';
					echo '<input type="hidden" name="sv" value="'.$_GET['sv'].'"/>';
					echo '<input type="submit" value="seleccionar">';
					echo '</form>';	
				}
				if ((get_post_meta($_GET['sv'], 'f_noches', true) == '' || get_post_meta($_GET['sv'], 'f_noches', true) == 0) && isset($_GET['ff']) &&  isset($_GET['ff']) <> '')
				{
					$confirmar = 1;
				}
				if ((isset($_GET['ff']) || $_GET['ff'] <> '') && (!isset($_GET['ht']) || $_GET['ht'] == '') && (get_post_meta($_GET['sv'], 'f_noches', true) <> '' && get_post_meta($_GET['sv'], 'f_noches', true) > 0) )
				{
					echo '<div style="display:inline-block;vertical-align: top;padding-left:3px;padding-right:3px;text-align: center;padding-top:0px;">';
					echo '<h3>Hotel</h3>';
					echo '<a class="btn active big filled" href="'.get_site_url().'/reserva-hoteles/'.$url.'"><span>Selecciona Hotel</span></a>';
					echo '</div>';
				}elseif (isset($_GET['ht']) && get_post_meta($_GET['sv'], 'f_noches', true) <> '' && get_post_meta($_GET['sv'], 'f_noches', true) > 0 ){
					echo '<a href="'.get_site_url().'/reserva-hoteles/'.$url.'" class="festival_logo"  style="display:inline-block;vertical-align: top;padding-left:3px;padding-right:3px;text-align:center;padding-top: 0px;">';
					echo '<h3>Hotel</h3>';
					echo '<h4>'.get_the_title($_GET['ht']).'</h4>';
					echo get_the_post_thumbnail( $_GET['ht'], 'thumbnail' );
					echo '</a>';
					$confirmar = 1;
				}
			}
		}
		elseif ($codigo2['reserv'] == 1 && !isset($_GET['confirmar']))
		{
			echo '<div style="display:inline-block;vertical-align: top;padding-left:3px;padding-right:3px;text-align: center;">';
			echo '<h3>Estado</h3>';
			echo '<h4>Utilizada</h4>';
			echo '<a class="btn active big filled" href="'.get_site_url().'/modificar-reserva/"><span>Contacta para cambiarla</span></a>';
			echo '</div>';
		}
		echo '</div>';
		if ($confirmar == 1)
		{
			echo '<div style="text-align: center;padding-top: 20px;display:block">';
			echo '<a class="btn active big filled" href="'.get_permalink().$url.'&confirmar=1"><span>confirma tu reserva</span></a>';
			echo '</div>';
		}

	}
	elseif ($_GET['confirmar'] == 1)
	{
		$servicios = obteneter_servicios($codigo2['productos'], $_GET['fv'], '');
		$data = array();
		foreach($servicios as $servicio)
		{
			if (!in_array($servicio['entrada'].'//'.$servicio['nentrada'].'//'.$servicio['noches'].'//'.$servicio['otro'], $data))
			{
				$data[] = $servicio['entrada'].'//'.$servicio['nentrada'].'//'.$servicio['noches'].'//'.$servicio['otro'];
				$servicio_texto = servicio_texto($servicio);

			}
		}
		$fv_tx = '';
		$fv_tx .= '<div style="vertical-align: top;padding-left:3px;padding-right:3px;text-align: center;">';
		$fv_tx .= '<h3>Producto</h3>';
		$fv_tx .= '<h4>'.get_the_title($codigo2['productos']).'</h4>';
		$fv_tx .= get_the_post_thumbnail($codigo2['productos'], 'thumbnail' );
		$fv_tx .= '</div>';
		$fv_tx .= '<div style="vertical-align: top;padding-left:3px;padding-right:3px;text-align: center;">';
		$fv_tx .= '<h3>Nº Cheque</h3>';
		$fv_tx .= '<h4>'.$codigo.'</h4>';
		$fv_tx .= '</div>';
		$fv_tx .= '<div style="vertical-align: top;padding-left:3px;padding-right:3px;text-align: center;">';
		$fv_tx .= '<h3>Fecha de caducidad</h3>';
		$fv_tx .= '<h4>'.$codigo2['valid'].'</h4>';
		$fv_tx .= '</div>';
		$fv_tx .= '<div style="vertical-align: top;padding-left:3px;padding-right:3px;text-align: center;">';
		$fv_tx .= '<h3>Evento Fecha</h3>';
		$fechas = obtener_fechas_festival($_GET['fv']);
		if($_GET['ff'] == 'ABONO')
		{
			$fv_tx .= '<h4>ABONO</h4>';
		}else
		{
			$fv_tx .= '<h4>'.$fechas[$_GET['ff']].'</h4>';
		}
		$fv_tx .= '</div>';
		$fv_tx .= '<div style="vertical-align: top;padding-left:3px;padding-right:3px;text-align: center;">';
		$fv_tx .= '<h3>Servicio</h3>';
		$fv_tx .= '<h4>'.$servicio_texto.'</h4>';
		$fv_tx .= '</div>';
		if (isset($_GET['fv']))
		{
			$attachID = get_post_meta( $_GET['fv'], 'custom_f_logo', true );
			$fv_tx .= '<div  class="festival_logo" style="vertical-align: top;padding-left:3px;padding-right:3px;text-align:center;padding-top: 0px;width:100%;display:block">';
			$fv_tx .= '<h3>Evento</h3>';
			$fv_tx .= '<h4>'.get_the_title($_GET['fv']).'</h4>';
			foreach($attachID as $item)
			{
				$imagen = wp_get_attachment_image_src($item, 'thumbnail');
				$fv_tx .= '<img src="'.$imagen[0].'" width="'.$imagen[1].'" height="'.$imagen[2].'" class="attachment-post-thumbnail wp-post-image" alt="'.get_post_meta($item, '_wp_attachment_image_alt', true).'"/>';
			}
			$fv_tx .= '</div>';
		}
		if (isset($_GET['ht']))
		{
			$fv_tx .= '<div class="festival_logo" style="vertical-align: top;padding-left:3px;padding-right:3px;text-align:center;padding-top: 0px;width:100%;display:block">';
			$fv_tx .= '<h3>Hotel</h3>';
			$fv_tx .= get_the_post_thumbnail( $_GET['ht'], 'thumbnail' );
			$fv_tx .= '<h4>'.get_the_title($_GET['fv']).'</h4>';
			$fv_tx .= '</div>';
		}
		echo do_shortcode('[vc_row][vc_column width="2/3"][vc_column_text][contact-form-7 id="31281" title="reservas_copy"][/vc_column_text][/vc_column][vc_column width="1/3"][vc_column_text]'.$fv_tx.'[/vc_column_text][/vc_column][/vc_row]');
		echo '</div>';
	}
}
add_shortcode('reserva','reserva_short');


function reserva_short_tito($attr, $content)
{
	//Inicializacion/creacion de variables
	$codigo = '';
	$msg = '';
	$confrimar = 0;
	//Bloque que se ejecuta cuando se ha introducido valor en el cuadro de texto CODIGO
	if (isset($_GET['codigo']) || $_GET['codigo'] <> '')
	{
		echo '<h2>SE HA INTRODUCIDO VALOR EN CODIGO</h2>';
		$codigo = $_GET['codigo'];
		$codigo2 = comprobar_code($codigo);			
		$url = '?codigo='.$codigo;

		if (isset($_GET['fv']) || $_GET['fv'] <> '') 
		{
			$url .= '&fv='.$_GET['fv'];
		}
		$nnoches = '';
		if (isset($_GET['sv']) || $_GET['sv'] <> '')
		{
			$url .= '&sv='.$_GET['sv'];
			$nnoches = get_post_meta($_GET['sv'], 'f_noches', true);
		}
		if (isset($_GET['ht']) || $_GET['ht'] <> '') $url .= '&ht='.$_GET['ht'];
		if (isset($_GET['ff']) || $_GET['ff'] <> '') $url .= '&ff='.$_GET['ff'];
		if (isset($_GET['hf']) || $_GET['hf'] <> '') $url .= '&hf='.$_GET['hf'];
			
		if ($codigo2['codigo'] == '') 
		{	
			$msg = 'El código introducido es erroneo o ha sucedido un error. Vuelva a intentarlo o póngase encontacto con nostros para solucionarlo.';
		}
		else if ($codigo2['activ'] == 0) 
		{
			$msg   = 'El código introducido aún no ha sido activado. Si tiene cualquier duda póngase en contacto con nosotros.';
		}
		else if ($codigo2['reserv'] == 1) 
		{
			$msg  = 'Lo sentimos, este código de reserva ya ha sido utilizado. Si tiene cualquier duda póngase en contacto con nosotros.';
		}
		
	}
	//
	//Bloque que se ejecuta la primera vez que se entra en la pantalla, es decir, cuando no hay código, ni confirmación del mismo, ni mensaje de error
	
	if ((!isset($_GET['codigo']) && !isset($_GET['confirmar']) && !isset($_GET['confirmado'])) || $msg <> '' )
	{
		//Pintamos en la pantalla la parte variable de la web
		echo '<div style="inline-blok">';
		echo '<div style="text-align: center;">';
		echo '<p style="text-align:left">'.$content.'</p>';
		echo '<h2>INSERTA TU CÓDIGO</h2>';
		//echo '<form method="GET" action="' .get_permalink(). '/prueba_tito">';
		echo '<form method="GET" action="http://www.google.es">';
		echo '<input type="text" name="codigo" required="" value="'.$codigo.'" style="display: inline-block;margin-right: 20px;" />';
		echo '<input type="submit" value="Continuar"/>';
		echo '<p style="text-align:center">Si no sabes donde encontrar tu código de reserva, pincha <a href="http://www.coolturebox.com/wp-content/uploads/2017/01/CHEQUE_EJ_CODIGO.jpg" title="Código de reserva" rel="lightbox">aquí</a>.</p>';
		echo '<p style="text-align:center;color: #ff0000;">'.$msg.'</p>';
		echo '</form></div>';
		echo '</div>';
	}
	
	//Bloque se ejecuta cuando se ha introducido el código de reserva y se ha pulsado el botón CONTINUAR
	
	elseif (isset($_GET['codigo']) && !isset($_GET['confirmar']) && !isset($_GET['confirmado']))
	{
		//Pintamos información relativa al producto		
		echo '<div style="display:inline-block;vertical-align: top;padding-left:3px;padding-right:3px;text-align: center;">';
		echo '<h3>Producto</h3>';
		echo '<h4>'.get_the_title($codigo2['productos']).'</h4>';
		echo get_the_post_thumbnail($codigo2['productos'], 'thumbnail' );
		echo '</div>';
		echo '<div style="display:inline-block;vertical-align: top;padding-left:3px;padding-right:3px;text-align: center;">';
		echo '<h3>Nº Cheque</h3>';
		echo '<h4>'.$codigo.'</h4>';
		echo '<h3>Fecha de caducidad</h3>';
		//echo '<h4>'.$codigo2['valid'].'</h4>';
		echo '<h4>'.substr($codigo2['valid'], 8,2). '-' .substr($codigo2['valid'], 5,2). '-' .substr($codigo2['valid'], 0,4);
		//Bloque que se encarga de repintar la pantalla una vez que el usuario ha pulsado el botón SELECCIONA TU FESTIVAL
		if (isset($_GET['ff']) &&  isset($_GET['ff']) <> '')
		{
			echo '<h3>Evento Fecha</h3>';
			$fechas = obtener_fechas_festival($_GET['fv']);
			if($_GET['ff'] == 'ABONO')
			{
				echo '<h4>ABONO</h4>';
			}else
			{
				echo '<h4>'.$fechas[$_GET['ff']].'</h4>';
			}
		}
		if (isset($_GET['ht']) &&  $_GET['ht'] <> '')
		{
			echo '<h3>Entrada en el Hotel</h3>';
			echo '<h4>Fechas a confirmar según disponibilidad</h4>';
		}
		echo '</div>';
		//Bloque que se pinta una vez que se introduce el código de reserva y se hace clic sobre el botón CONTINUAR
		if ($codigo2['reserv'] == 0 && !isset($_GET['confirmar']))
		{
			//Si aún no se ha seleccionado festival....
			if (!isset($_GET['fv']) || $_GET['fv'] == '')
			{
				//Pintamos los controles necesario para que el usuario SELECCIONE FESTIVAL-->SHORTCODE reserva-festivales Esta codificado en el plugin de festivales
				echo '<div style="display:inline-block;vertical-align: top;padding-left:3px;padding-right:3px;text-align: center;">';
				echo '<h3>Estado</h3>';
				echo '<h4>libre</h4>';
				echo '<a class="btn active big filled" href="'.get_site_url().'/reserva-festivales/'.$url.'"><span>Selecciona tu Evento</span></a>';
				echo '</div>';
			}
			elseif (isset($_GET['fv']))
			{
				$attachID = get_post_meta( $_GET['fv'], 'custom_f_logo', true );
				echo '<a href="'.get_site_url().'/reserva-festivales/'.$url.'" class="festival_logo"  style="display:inline-block;vertical-align: top;padding-left:3px;padding-right:3px;text-align:center;padding-top: 0px;">';
				echo '<h3>Evento</h3>';
				echo '<h4>'.get_the_title($_GET['fv']).'</h4>';
				foreach($attachID as $item)
				{
					$imagen = wp_get_attachment_image_src($item, 'thumbnail');
					echo  '<img src="'.$imagen[0].'" width="'.$imagen[1].'" height="'.$imagen[2].'" class="attachment-post-thumbnail wp-post-image" alt="'.get_post_meta($item, '_wp_attachment_image_alt', true).'"/>';
				}
				echo '</a>';
				if (!isset($_GET['ff']) &&  isset($_GET['ff']) == '')
				{
					$fechas = obtener_fechas_festival( $_GET['fv']);
					echo '<form type="GET" style="display:inline-block;vertical-align: top;padding-left:3px;padding-right:3px;text-align:center;padding-top: 0px;">';
					echo '<h3>Fecha del Evento</h3>';
					echo '<select name="ff" style="margin:6px;display:inline-block;padding-right:30px;">';
					$i=0;
					if (get_post_meta($_GET['sv'], 'f_entrada', true) == 0 || get_post_meta($_GET['sv'], 'f_entrada', true) == 1)
					{
						foreach($fechas as $fecha)
						{
							echo '<option value="'.$i.'">'.$fecha.'</option>';
							$i++;
						}
					}
					else
					{
						echo '<option value="ABONO">ABONO</option>';
					}
					echo '</select>';
					echo '<input type="hidden" name="fv" value="'.$_GET['fv'].'"/>';
					echo '<input type="hidden" name="codigo" value="'.$_GET['codigo'].'"/>';
					echo '<input type="hidden" name="sv" value="'.$_GET['sv'].'"/>';
					echo '<input type="submit" value="seleccionar">';
					echo '</form>';	
				}
				if ((get_post_meta($_GET['sv'], 'f_noches', true) == '' || get_post_meta($_GET['sv'], 'f_noches', true) == 0) && isset($_GET['ff']) &&  isset($_GET['ff']) <> '')
				{
					$confirmar = 1;
				}
				if ((isset($_GET['ff']) || $_GET['ff'] <> '') && (!isset($_GET['ht']) || $_GET['ht'] == '') && (get_post_meta($_GET['sv'], 'f_noches', true) <> '' && get_post_meta($_GET['sv'], 'f_noches', true) > 0) )
				{
					echo '<div style="display:inline-block;vertical-align: top;padding-left:3px;padding-right:3px;text-align: center;padding-top:0px;">';
					echo '<h3>Hotel</h3>';
					echo '<a class="btn active big filled" href="'.get_site_url().'/reserva-hoteles/'.$url.'"><span>Selecciona Hotel</span></a>';
					echo '</div>';
				}elseif (isset($_GET['ht']) && get_post_meta($_GET['sv'], 'f_noches', true) <> '' && get_post_meta($_GET['sv'], 'f_noches', true) > 0 ){
					echo '<a href="'.get_site_url().'/reserva-hoteles/'.$url.'" class="festival_logo"  style="display:inline-block;vertical-align: top;padding-left:3px;padding-right:3px;text-align:center;padding-top: 0px;">';
					echo '<h3>Hotel</h3>';
					echo '<h4>'.get_the_title($_GET['ht']).'</h4>';
					echo get_the_post_thumbnail( $_GET['ht'], 'thumbnail' );
					echo '</a>';
					$confirmar = 1;
				}
			}
		}
		elseif ($codigo2['reserv'] == 1 && !isset($_GET['confirmar']))
		{
			echo '<div style="display:inline-block;vertical-align: top;padding-left:3px;padding-right:3px;text-align: center;">';
			echo '<h3>Estado</h3>';
			echo '<h4>Utilizada</h4>';
			echo '<a class="btn active big filled" href="'.get_site_url().'/modificar-reserva/"><span>Contacta para cambiarla</span></a>';
			echo '</div>';
		}
		echo '</div>';
		if ($confirmar == 1)
		{
			echo '<div style="text-align: center;padding-top: 20px;display:block">';
			echo '<a class="btn active big filled" href="'.get_permalink().$url.'&confirmar=1"><span>confirma tu reserva</span></a>';
			echo '</div>';
		}

	}
	
	
	elseif ($_GET['confirmar'] == 1)
	{
		$servicios = obteneter_servicios($codigo2['productos'], $_GET['fv'], '');
		$data = array();
		foreach($servicios as $servicio)
		{
			if (!in_array($servicio['entrada'].'//'.$servicio['nentrada'].'//'.$servicio['noches'].'//'.$servicio['otro'], $data))
			{
				$data[] = $servicio['entrada'].'//'.$servicio['nentrada'].'//'.$servicio['noches'].'//'.$servicio['otro'];
				$servicio_texto = servicio_texto($servicio);

			}
		}
		$fv_tx = '';
		$fv_tx .= '<div style="vertical-align: top;padding-left:3px;padding-right:3px;text-align: center;">';
		$fv_tx .= '<h3>Producto</h3>';
		$fv_tx .= '<h4>'.get_the_title($codigo2['productos']).'</h4>';
		$fv_tx .= get_the_post_thumbnail($codigo2['productos'], 'thumbnail' );
		$fv_tx .= '</div>';
		$fv_tx .= '<div style="vertical-align: top;padding-left:3px;padding-right:3px;text-align: center;">';
		$fv_tx .= '<h3>Nº Cheque</h3>';
		$fv_tx .= '<h4>'.$codigo.'</h4>';
		$fv_tx .= '</div>';
		$fv_tx .= '<div style="vertical-align: top;padding-left:3px;padding-right:3px;text-align: center;">';
		$fv_tx .= '<h3>Fecha de caducidad</h3>';
		$fv_tx .= '<h4>'.$codigo2['valid'].'</h4>';
		$fv_tx .= '</div>';
		$fv_tx .= '<div style="vertical-align: top;padding-left:3px;padding-right:3px;text-align: center;">';
		$fv_tx .= '<h3>Evento Fecha</h3>';
		$fechas = obtener_fechas_festival($_GET['fv']);
		if($_GET['ff'] == 'ABONO')
		{
			$fv_tx .= '<h4>ABONO</h4>';
		}else
		{
			$fv_tx .= '<h4>'.$fechas[$_GET['ff']].'</h4>';
		}
		$fv_tx .= '</div>';
		$fv_tx .= '<div style="vertical-align: top;padding-left:3px;padding-right:3px;text-align: center;">';
		$fv_tx .= '<h3>Servicio</h3>';
		$fv_tx .= '<h4>'.$servicio_texto.'</h4>';
		$fv_tx .= '</div>';
		if (isset($_GET['fv']))
		{
			$attachID = get_post_meta( $_GET['fv'], 'custom_f_logo', true );
			$fv_tx .= '<div  class="festival_logo" style="vertical-align: top;padding-left:3px;padding-right:3px;text-align:center;padding-top: 0px;width:100%;display:block">';
			$fv_tx .= '<h3>Evento</h3>';
			$fv_tx .= '<h4>'.get_the_title($_GET['fv']).'</h4>';
			foreach($attachID as $item)
			{
				$imagen = wp_get_attachment_image_src($item, 'thumbnail');
				$fv_tx .= '<img src="'.$imagen[0].'" width="'.$imagen[1].'" height="'.$imagen[2].'" class="attachment-post-thumbnail wp-post-image" alt="'.get_post_meta($item, '_wp_attachment_image_alt', true).'"/>';
			}
			$fv_tx .= '</div>';
		}
		if (isset($_GET['ht']))
		{
			$fv_tx .= '<div class="festival_logo" style="vertical-align: top;padding-left:3px;padding-right:3px;text-align:center;padding-top: 0px;width:100%;display:block">';
			$fv_tx .= '<h3>Hotel</h3>';
			$fv_tx .= get_the_post_thumbnail( $_GET['ht'], 'thumbnail' );
			$fv_tx .= '<h4>'.get_the_title($_GET['fv']).'</h4>';
			$fv_tx .= '</div>';
		}
		echo do_shortcode('[vc_row][vc_column width="2/3"][vc_column_text][contact-form-7 id="31281" title="reservas_copy"][/vc_column_text][/vc_column][vc_column width="1/3"][vc_column_text]'.$fv_tx.'[/vc_column_text][/vc_column][/vc_row]');
		echo '</div>';
	}
	
}
add_shortcode('reserva_tito','reserva_short_tito');

add_action( 'wpcf7_init', 'custom_add_shortcode_caja_form' );
function custom_add_shortcode_caja_form() {
    wpcf7_add_shortcode( 'caja', 'custom_caja_form_shortcode_handler' ); // "clock" is the type of the form-tag
}
function custom_caja_form_shortcode_handler( $tag ) {
	$codigo = $_GET['codigo'];
	$codigo2 = comprobar_code($codigo);	
   	return '<input type="hidden" name="caja" value="'.get_the_title($codigo2['productos']).'">';
}

add_action( 'wpcf7_init', 'custom_add_shortcode_servicio_form' );
function custom_add_shortcode_servicio_form() {
    wpcf7_add_shortcode( 'servicio', 'custom_servicio_form_shortcode_handler' ); // "clock" is the type of the form-tag
}
function custom_servicio_form_shortcode_handler( $tag ) {
	$codigo = $_GET['codigo'];
	$codigo2 = comprobar_code($codigo);	
	$servicios = obteneter_servicios($codigo2['productos'], $_GET['fv'], '');
	$data = array();
	foreach($servicios as $servicio){
		if (!in_array($servicio['entrada'].'//'.$servicio['nentrada'].'//'.$servicio['noches'].'//'.$servicio['otro'], $data)){
			$data[] = $servicio['entrada'].'//'.$servicio['nentrada'].'//'.$servicio['noches'].'//'.$servicio['otro'];
			$servicio_texto = servicio_texto($servicio);
		}
	}
   	return '<input type="hidden" name="servicio" value="'.$servicio_texto.'">';
}

add_action( 'wpcf7_init', 'custom_add_shortcode_fecha_form' );
function custom_add_shortcode_fecha_form() {
    wpcf7_add_shortcode( 'fecha', 'custom_fecha_form_shortcode_handler' ); // "clock" is the type of the form-tag
}
function custom_fecha_form_shortcode_handler( $tag ) {
	$fechas = obtener_fechas_festival($_GET['fv']);
	if($_GET['ff'] == 'ABONO'){
		return '<input type="hidden" name="fecha" value="ABONO">';
	}else{
		return '<input type="hidden" name="fecha" value="'.$fechas[$_GET['ff']].'">';
	}
}

add_action( 'wpcf7_init', 'custom_add_shortcode_festival_form' );
function custom_add_shortcode_festival_form() {
    wpcf7_add_shortcode( 'festival', 'custom_festival_form_shortcode_handler' ); // "clock" is the type of the form-tag
}
function custom_festival_form_shortcode_handler( $tag ) {
	return '<input type="hidden" name="festival" value="'.get_the_title($_GET['fv']).'">';
}

add_action( 'wpcf7_init', 'custom_add_shortcode_hotel_form' );
function custom_add_shortcode_hotel_form() {
    wpcf7_add_shortcode( 'hotel', 'custom_hotel_form_shortcode_handler' ); // "clock" is the type of the form-tag
}
function custom_hotel_form_shortcode_handler( $tag ) {
	$hotel = '';
	if(isset($_GET['ht']) && $_GET['ht'] <> '') $hotel = get_the_title($_GET['ht']);
	return '<input type="hidden" name="hotel" value="'.$hotel.'">';
}

add_action( 'wpcf7_init', 'custom_add_shortcode_codigo_form' );
function custom_add_shortcode_codigo_form() {
    wpcf7_add_shortcode( 'codigo', 'custom_codigo_form_shortcode_handler' ); // "clock" is the type of the form-tag
}
function custom_codigo_form_shortcode_handler( $tag ) {
	$codigo = $_GET['codigo'];
	$codigo2 = comprobar_code($codigo);	
   	return '<input type="hidden" name="codigo" value="'.$codigo.'">';
}

function hacer_reserva( $name, $apellidos, $email, $subject, $festival, $hotel, $code, $servicio, $tlf, $fecha_festival){
	global $wpdb;
	global $wpdb_new;

	$fechas = obtener_fechas_festival($festival);

	$wpdb->insert( 'wp_reservas', array( nombre => $name, apellidos => $apellidos, email => $email, subject => $subject, festival => $festival, hotel => $hotel, code => $code, servicio => $servicio, tlf => $tlf, fecha_fes => $fechas[$fecha_festival]));
		$wpdb ->show_errors();

	if ($code <> '999999999' && $code <> '888888888'){
		$wpdb_new = new wpdb( 'wscoolturebox', 'chile235', 'wscoolturebox', 'localhost' );
		$wpdb_new ->show_errors();
		$data_array = array('reserv' => 1);
		$where = array('cod_reser' => $code, 'reserv' => 0);
		$wpdb_new->update( 'ws', $data_array, $where );
	}
}

add_action('wpcf7_before_send_mail', 'guardar_reserva_v2');
function guardar_reserva_v2($wpcf7) 
{
	global $wpdb_new;
	$submission = WPCF7_Submission::get_instance();
	$formulario = $wpcf7->title();	
	$data = $submission->get_posted_data();
	if($formulario == 'reservas_copy' && $submission)
	{

	$wpdb_new = new wpdb( 'wscoolturebox', 'coolturechile235', 'wscoolturebox', 'localhost' );
	$wpdb_new ->show_errors();
	$codigo_prod = $_GET['codigo'];
	$data_array = array('reserv' => 1);
	$where = array('cod_reser' => $codigo_prod, 'reserv' => 0);
	$wpdb_new->update( 'ws', $data_array, $where );
	//--------------------------------------
	global $wpdb;
	$festival = $_GET['codigo']; //$data['fv'];
	$hotel = $_GET['ht'];
	$servicio = $_GET['sv'];
	$fechas = obtener_fechas_festival($festival);
	$fecha_festival = $data['ff'];
/*
	$wpdb->insert( 'wp_reservas', array( 
											nombre => $data['your-name'], 
											apellidos => $data['your-surname'], 
											email => $data['your-email'], 
											subject => $data['your-message'], 
											festival => $festival, 
											hotel => $data['ht'], 
											code => $codigo_prod, 
											servicio => $data['sv'], 
											tlf => $data['tel-379'], 
											fecha_fes => $fechas[$fecha_festival]
										)
*/

	$wpdb->insert( 'wp_reservas', array( 
											nombre => $data['your-name'], 
											apellidos => $data['your-surname'], 
											email => $data['your-email'], 
											subject => $data['your-message'], 
											festival => $festival, 
											hotel => $hotel,
											code => $codigo_prod, 
											servicio => $servicio,
											tlf => $data['tel-379'], 
											fecha_fes => $fechas[$fecha_festival] //'16/07/2015'
										)
				);
										
	
	$wpdb ->show_errors();
	}
	
}
function guardar_reserva($wpcf7) 
{
	$submission = WPCF7_Submission::get_instance();
	$formulario = $wpcf7->title();	
	$data = $submission->get_posted_data();
	if($formulario == 'reservas_copy' && $submission)
	{
		$data = $submission->get_posted_data();
		hacer_reserva( $data['your-name'], $data['your-surname'], $data['your-email'], $data['your-message'], $data['fv'], $data['ht'], $data['codigo'], $data['sv'], $data['tel-379'], $data['ff'] );
	}
}

function reserva_festivales_short($attr, $content){
	$c = 20;	
	//Si el código esta establecido (la primera vez que entramos SI TENEMOS VALOR EN EL CODIGO)	
	if (isset($_GET['codigo']))
	{
		//Recuperamos el valor del codigo e reserva en la variable $codigo
		$codigo = $_GET['codigo'];
		//Comprobamos la existencia del código de reserva
		$codigo2 = comprobar_code($codigo);	
		$page_id = get_the_ID();
		$festivales_filtro = festivales_filtro($page_id);
		$festivales = obtener_caja_festivales($codigo2['productos'], $festivales_filtro['arg']);
		$pg = 1;

		if (isset($_GET['pg']))	
			$pg = $_GET['pg'];

		$nf = sizeof($festivales);
		$pages = ceil($nf/$c);
		$inicio = $c*($pg-1);
		$fin = $c*$pg+1;
		

		$url = '?codigo='.$codigo;
		if (isset($_GET['fv']) || $_GET['fv'] <> '') $url .= '&fv='.$_GET['fv'];
		if (isset($_GET['sv']) || $_GET['sv'] <> '')$url .= '&sv='.$_GET['sv'];
		if (isset($_GET['ht']) || $_GET['ht'] <> '') $url .= '&ht='.$_GET['ht'];
		if (isset($_GET['ff']) || $_GET['ff'] <> '') $url .= '&ff='.$_GET['ff'];
		if (isset($_GET['fh']) || $_GET['hf'] <> '') $url .= '&hf='.$_GET['hf'];
	}
	//	
	
	echo '<div style="text-align: center;">';
	echo '<h2>SELECCIONA TU EVENTO</h2>';
	echo '<div>';
	$i = 0;
	echo $festivales_filtro['output'];
	echo '<div style="text-align: center;padding:20px">';
	echo '<a class="btn active big filled" href="'.get_site_url().'/reservas/'.$url.'"><span>volver</span></a>';
	echo '</div>';
	foreach($festivales as $festival)
	{
		$fechas = obtener_fechas_festival_txt($festival);
		$f_falta = obtener_fechas_festival($festival);
		$servicios = obteneter_servicios($codigo2['productos'], $festival, '');
		//if ($fechas['next'] <> 'AÚN POR CONFIRMAR' && comprobar_nentradas($festival) == 'OK')
		if ($fechas['next'] <> 'AÚN POR CONFIRMAR')
		{
			list($f_dia,$f_mes,$f_ano) = explode('/',$f_falta['0']);
			$f_falta_s = mktime(0,0,0,$f_mes,$f_dia,$f_ano);
			$segundos_diferencia = $f_falta_s - $today;

			if (($segundos_diferencia > 3*(60 * 60 * 24) && (get_post_meta( $servicios[0], 'f_noches', true ) == 0 || get_post_meta( $servicios[0], 'f_noches', true ) == '' )) || ($segundos_diferencia > 5*(60 * 60 * 24) && (get_post_meta( $servicios[0], 'f_noches', true ) > 0 && get_post_meta( $servicios[0], 'f_noches', true ) <> '' )))
			{
				$i++;
				if ($i>$inicio && $i < $fin)
				{
					echo '<a href="'.get_the_permalink($festival).$url.'" class="festival_logo">';
					$attachID = get_post_meta( $festival, 'custom_f_logo', true );
					foreach($attachID as $item)
					{
						$imagen = wp_get_attachment_image_src($item, 'thumbnail');
						echo  '<img src="'.$imagen[0].'" width="'.$imagen[1].'" height="'.$imagen[2].'" class="attachment-post-thumbnail wp-post-image" alt="'.get_post_meta($item, '_wp_attachment_image_alt', true).'"/>';
					}
					echo '<h3>'.get_the_title($festival).'</h3>';
					echo '</a>';
				}
			}
		}
		
	}
	echo '</div>';
	echo festivales_tab_pagination('', $pg, $pages, 2, $url);
	echo '</div>';	
}
add_shortcode('reserva_festivales','reserva_festivales_short');

function reserva_hoteles_short($attr, $content){
	$c = 20;
	if (isset($_GET['codigo'])){
		$codigo = $_GET['codigo'];
		$codigo2 = comprobar_code($codigo);	
//		$msg = 'El código introducido es erroneo o ha sucedido un error. Vuelva a intentarlo o póngase encontacto con nostros para solucionarlo.';
		$pg = 1;
		if (isset($_GET['pg'])){
			$pg = $_GET['pg'];
		}
		$nf = sizeof($festivales);
		$pages = ceil($nf/$c);
		$inicio = $c*($pg-1);
		$fin = $c*$pg+1;

		$url = '?codigo='.$codigo;
		if (isset($_GET['fv']) || $_GET['fv'] <> '') $url .= '&fv='.$_GET['fv'];
		if (isset($_GET['sv']) || $_GET['sv'] <> '') $url .= '&sv='.$_GET['sv'];
		if (isset($_GET['ht']) || $_GET['ht'] <> '') $url .= '&ht='.$_GET['ht'];
		if (isset($_GET['ff']) || $_GET['ff'] <> '') $url .= '&ff='.$_GET['ff'];
		if (isset($_GET['fh']) || $_GET['hf'] <> '') $url .= '&hf='.$_GET['hf'];

		$servicios = obteneter_servicios($codigo2['productos'], $_GET['fv'], '');
		foreach ($servicios as $servicio){
//			if($servicio['id'] == $festivales[2] ){
				$splits = explode(',',$servicio['alojamientos']);
				foreach($splits as $split){
					if ($split <> ''){
						$hoteles[] = $split;
					}
				}
//			}
		}
	}
//	print_r($servicios);
//	print_r($hoteles);
	echo '<div style="text-align: center;">';
	echo '<h2>SELECCIONA TU HOTEL</h2>';
	echo '<div>';
	echo '<div style="text-align: center;padding:20px">';
	echo '<a class="btn active big filled" href="'.get_site_url().'/reservas/'.$url.'"><span>volver</span></a>';
	echo '</div>';
	$i = 0;
	foreach($hoteles as $hotel):
		$i++;
//		echo 'i:'.$i.'--inicio:'.$inicio.'--fin:'.$fin;
		if ($i>$inicio && $i < $fin){
//			echo $hotel;
			echo '<a class="festival_logo" href="'.get_the_permalink($hotel).$url.'">';
			echo get_the_post_thumbnail( $hotel, 'thumbnail' );
			echo '<h3>'.get_the_title($hotel).'</h3>';
			echo '</a>';
		}
	endforeach;
	echo '</div>';
	echo festivales_tab_pagination('', $pg, $pages, 2, $url);
	echo '</div>';
}
add_shortcode('reserva_hoteles','reserva_hoteles_short');

//  Se ha ocultado porque se ha añadido en otro sitio.
//function obtener_reserva($usu = ''){
//	global $wpdb;
//	if ( $usu == ''){
//	    $consulta = "SELECT * FROM wp_reservas WHERE ORDER BY fecha_in ASC";
//	}else{
//	    $consulta = "SELECT * FROM wp_reservas WHERE festival = ".$usu." ORDER BY fecha_in ASC";
//	}
//	$signs = $wpdb->get_results($consulta);
//	foreach ($signs as $sign) {
//		$nombre = $sign->nombre;
//		$apellidos = $sign->apellidos;
//		$code = $sign->code;
//		$servicio = $sign->servicio;
//		$fecha_fes = $sign->fecha_fes;
//		$result[] = array( 'nombre' => $nombre, 'apellidos' => $apellidos, 'code' => $code, 'servicio' => $servicio, 'fecha_fes' => $fecha_fes);
//	}
// 	return $result;
//}

//add_action( 'show_user_profile', 'add_extra_social_links' );
//add_action( 'edit_user_profile', 'add_extra_social_links' );
//
//function add_extra_social_links( $user )
//{
//        <h3>Festival del Usuario</h3>
//
//        <table class="form-table">
//            <tr>
//                <th><label for="festival_profile">Festival</label></th>
//                <td><input type="text" name="festival_profile" value="<?php echo esc_attr(get_the_author_meta( 'festival_profile', $user->ID )); " class="regular-text" /></td>
//            </tr>
//        </table>
//    <?php
//}
//add_action( 'personal_options_update', 'save_extra_social_links' );
//add_action( 'edit_user_profile_update', 'save_extra_social_links' );
//
//function save_extra_social_links( $user_id )
//{
//    update_user_meta( $user_id,'festival_profile', sanitize_text_field( $_POST['festival_profile'] ) );
//}

add_action( 'add_meta_boxes', 'em_mtbx_productos' );

function em_mtbx_productos() {

	// Creamos el meta box personalizado
	add_meta_box( 
                'Datos_Producto', // atributo ID
                'Productos', // Título
                'em_mtbx_productos_fv_function', // Función que muestra el HTML que aparecerá en la pantalla
                'product', // Tipo de entrada. Puede ser 'post', 'page', 'link', o 'custom_post_type'
                'normal', // Parte de la pantalla donde aparecerá. Puede ser 'normal', 'advanced', o 'side'
                'high' // Prioridad u orden en el que aparecerá. Puede ser 'high', 'core', 'default' o 'low'
                );
	
}

function em_mtbx_productos_fv_function( $post ) {
    
    $f_producto = get_post_meta( $post->ID, 'f_producto', true );
    $f_precio = get_post_meta( $post->ID, 'f_precio', true );
    $desc_corta = get_post_meta( $post->ID, 'desc_corta', true );
    $f_calameo = get_post_meta( $post->ID, 'f_calameo', true );
	if ($f_precio == '') $f_precio = 0;

     ?>

         <h4><?php _e("Codigo interno del producto"); ?></h4>
         <input class="em_mtbx_img" name="f_producto" id="f_producto" type="text" value="<?php if($f_producto && $f_producto != '') echo $f_producto; ?>">
         <h4><?php _e("Precio"); ?></h4>
         <input class="em_mtbx_img" name="f_precio" id="f_precio" type="text" value="<?php if($f_precio && $f_precio != '') echo $f_precio; ?>">
         <h4><?php _e("Desc corta"); ?></h4>
         <input class="em_mtbx_img" name="desc_corta" id="desc_corta" type="text" value="<?php if($desc_corta && $desc_corta != '') echo $desc_corta; ?>">
         <h4><?php _e("CALAMEO"); ?></h4>
         <input class="em_mtbx_img" name="f_calameo" id="f_calameo" type="text" value="<?php if($f_calameo && $f_calameo != '') echo $f_calameo; ?>">
	<?php       
}

//Para grabar los campos. Añadir uno nuevo por cada campo existente, en este caso tres.
add_action( 'save_post', 'em_mtbx_productos_fv_save_meta' );

function em_mtbx_productos_fv_save_meta( $post_id ) {

	if ( isset( $_POST['f_producto'] ) ) {
	
		update_post_meta( $post_id, 'f_producto', $_POST['f_producto']  );

	}
	if ( isset( $_POST['desc_corta'] ) ) {
	
		update_post_meta( $post_id, 'desc_corta', $_POST['desc_corta']  );

	}
	if ( isset( $_POST['f_calameo'] ) ) {
	
		update_post_meta( $post_id, 'f_calameo', $_POST['f_calameo']  );

	}
	if ( isset( $_POST['f_precio'] ) ) {
	
		if ( $_POST['f_precio'] == ''){
			update_post_meta( $post_id, 'f_precio', 0 );
		}else{
			update_post_meta( $post_id, 'f_precio', $_POST['f_precio']  );
		}
	}
}

function servicio_texto($servicio){
	$servicio_texto = "<b>";
	if ($servicio['entrada']=="0" && intval($servicio['nentrada']) <> 1){
		$servicio_texto .= "Entradas";
	}elseif ($servicio['entrada']=="0" && intval($servicio['nentrada']) == 1){
		$servicio_texto .= "Entrada";
	}elseif ($servicio['entrada']=="1" && intval($servicio['nentrada']) <> 1){
		$servicio_texto .= "Entradas VIP";
	}elseif ($servicio['entrada']=="1" && intval($servicio['nentrada']) == 1){
		$servicio_texto .= "Entrada VIP";
	}elseif($servicio['entrada']=="2" && intval($servicio['nentrada']) <> 1){
		$servicio_texto .= "Abonos";
	}elseif($servicio['entrada']=="2" && intval($servicio['nentrada']) == 1){
		$servicio_texto .= "Abono";
	}elseif($servicio['entrada']=="3" && intval($servicio['nentrada']) <> 1){
		$servicio_texto .= "Abonos VIP";
	}elseif($servicio['entrada']=="3" && intval($servicio['nentrada']) == 1){
		$servicio_texto .= "Abono VIP";
	}
	$servicio_texto .= ' para '.$servicio['nentrada'].' persona';
	if (intval($servicio['nentrada']) <> 1){
		$servicio_texto .= 's';
	}
	
	if ($servicio['noches'] <> 0 && $servicio['noches'] <> ''){
		if (intval($servicio['noches']) == 1){
			$servicio_texto .= ' + '.$servicio['noches'].' noche de hotel';
		}else{
			$servicio_texto .= ' + '.$servicio['noches'].' noches de hotel';
		}
	}
	if ($servicio['otro'] <> '' || $servicio['otro2'] <> '' )
	{
		if($servicio['otro'] <> '')
		{
			$titulo_entradas = '<b>Entradas: </b>';
		}
		else
		{
			$titulo_entradas = '';	
		}
		
		if($servicio['otro2'] <> '')
		{
			$titulo_aloj = '<b>Alojamiento: </b>';
		}
		else
		{
			$titulo_aloj = '';	
		}

		$servicio_texto .= '</b><p><span style = "color:#757575;font-size: 14px;">'.$titulo_entradas.$servicio['otro'].' </span></p><p><span style = "color:#757575;font-size: 14px;">'.$titulo_aloj.$servicio['otro2'].'</span></p>';
	}
	return $servicio_texto;
}
function servicio_texto2($servicio){
	$servicio_texto = "";
	if ($servicio['entrada']=="0" && intval($servicio['nentrada']) <> 1){
		$servicio_texto .= "Entradas";
	}elseif ($servicio['entrada']=="0" && intval($servicio['nentrada']) == 1){
		$servicio_texto .= "Entrada";
	}elseif ($servicio['entrada']=="1" && intval($servicio['nentrada']) <> 1){
		$servicio_texto .= "Entradas VIP";
	}elseif ($servicio['entrada']=="1" && intval($servicio['nentrada']) == 1){
		$servicio_texto .= "Entrada VIP";
	}elseif($servicio['entrada']=="2" && intval($servicio['nentrada']) <> 1){
		$servicio_texto .= "Abonos";
	}elseif($servicio['entrada']=="2" && intval($servicio['nentrada']) == 1){
		$servicio_texto .= "Abono";
	}elseif($servicio['entrada']=="3" && intval($servicio['nentrada']) <> 1){
		$servicio_texto .= "Abonos VIP";
	}elseif($servicio['entrada']=="3" && intval($servicio['nentrada']) == 1){
		$servicio_texto .= "Abono VIP";
	}
	$servicio_texto .= ' para '.$servicio['nentrada'].' persona';
	if (intval($servicio['nentrada']) <> 1){
		$servicio_texto .= 's';
	}
	
	if ($servicio['noches'] <> 0 && $servicio['noches'] <> ''){
		if (intval($servicio['noches']) == 1){
			$servicio_texto .= ' + '.$servicio['noches'].' noche de hotel';
		}else{
			$servicio_texto .= ' + '.$servicio['noches'].' noches de hotel';
		}
	}
	return $servicio_texto;
}

function venta_servicios_short($atts, $content = null){
	extract(shortcode_atts(array(
		'fv' => $_GET['fv']
	), $atts));
	$cajas = array();
	if (isset ($_GET['cj'])){
		$cajas [] = $_GET['cj'];
	}else{
		$cajas = obteneter_caja($fv);
	}
	$str = implode (", ", $cajas);
	
	if (isset($_GET['fv'])){
		echo '<h2 style="font-size: -webkit-xxx-large;line-height: initial;">'.get_the_title($_GET['fv']).'</h2>';
	}
	
	echo $content;
	echo do_shortcode('[etheme_products type="list" style="default" ids="'.$str.'"]');
}
add_shortcode('venta_servicios','venta_servicios_short');

function array_sort_func($a,$b=NULL) { 
   static $keys; 
   if($b===NULL) return $keys=$a; 
   foreach($keys as $k) { 
      if(@$k[0]=='!') { 
         $k=substr($k,1); 
         if(@$a[$k]!==@$b[$k]) { 
            return strcmp(@$b[$k],@$a[$k]); 
         } 
      } 
      else if(@$a[$k]!==@$b[$k]) { 
         return strcmp(@$a[$k],@$b[$k]); 
      } 
   } 
   return 0; 
} 

function array_sort(&$array) { 
   if(!$array) return $keys; 
   $keys=func_get_args(); 
   array_shift($keys); 
   array_sort_func($keys); 
   usort($array,"array_sort_func");        
}


function historico_activaciones(){
	global $wpdb_new;
	$wpdb_new = new wpdb( 'festibox', 'festibox311278', 'wsfestibox', 'localhost' );
	$wpdb_new ->show_errors();
    $signs = $wpdb_new->get_results("SELECT * FROM ws_historico ORDER BY fecha DESC");
	foreach ($signs as $sign) {
		$codigo = $sign->codigo;
		$fecha = $sign->fecha;
		$usuario = $sign->usuario;
		$estado = $sign->Estado;
		$result[] = array( 'codigo' => $codigo, 'fecha' => $fecha, 'usuario' => $usuario, 'estado' => $estado);
	}
 	return $result;
}