<?php 
/*
Plugin Name: Hoteles en campos personalizados
Plugin URI: http://www.emenia.es/plugin-subir-hotel-campo-personalizado-wordpress
*/

//hook para crear el meta box
function custom_alojamientos_type() {

// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Alojamientos', 'Post Type General Name', 'festibox' ),
		'singular_name'       => _x( 'Alojamiento', 'Post Type Singular Name', 'festibox' ),
		'menu_name'           => __( 'Alojamientos', 'festibox' ),
		'parent_item_colon'   => __( 'Alojamiento Padre', 'festibox' ),
		'all_items'           => __( 'Todos los alojamientos', 'festibox' ),
		'view_item'           => __( 'Ver alojamiento', 'festibox' ),
		'add_new_item'        => __( 'Añadir nuevo alojamiento', 'festibox' ),
		'add_new'             => __( 'Añadir nuevo', 'festibox' ),
		'edit_item'           => __( 'Editar alojamiento', 'festibox' ),
		'update_item'         => __( 'Actualizar alojamiento', 'festibox' ),
		'search_items'        => __( 'Buscar alojamiento', 'festibox' ),
		'not_found'           => __( 'No encontrado', 'festibox' ),
		'not_found_in_trash'  => __( 'No encontrado en la papelera', 'festibox' ),
	);

// Otras opciones para el tipo de entrada personalizada

	$args = array(
		'label'               => $labels,
		'description'         => __( 'alojamientos Festibox', 'festibox' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'thumbnail', 'comments','page_layout','author' ),
		// Puedes asociar este CPT con una taxonomía por defecto o una taxonomía personalizada
		'taxonomies'          => array( 'categoria','provincia','tipo' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 6,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);

	// Registro del tipo de entrada personalizada (Customs Post Type)
	register_post_type( 'alojamiento', $args );
}

add_action( 'init', 'custom_alojamientos_type', 0 );

add_action( 'add_meta_boxes', 'em_mtbx_hotel' );

function em_mtbx_hotel() 
{

	// Creamos el meta box personalizado
	add_meta_box( 
                'em-hotel', // atributo ID
                'Alojamiento', // Título
                'em_mtbx_hotel_function', // Función que muestra el HTML que aparecerá en la pantalla
                'alojamiento', // Tipo de entrada. Puede ser 'post', 'page', 'link', o 'custom_post_type'
                'normal', // Parte de la pantalla donde aparecerá. Puede ser 'normal', 'advanced', o 'side'
                'high' // Prioridad u orden en el que aparecerá. Puede ser 'high', 'core', 'default' o 'low'
                );
	
}


function listado_hotel($tipo, $value)
{	
	$values = explode(',',$value);
	global $wpdb;
    $numposts = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = '".$tipo."'");
	if (0 < $numposts) $numposts = number_format($numposts); 
	$i= 1;
	while($i <= $numposts)
	{
		wp_reset_postdata();
		$args = array(
						'numberposts' => -1,
						'offset' => $i-1,
						'post_type' => $tipo,
						'post_status' => 'publish',
						'order' => 'ASC',
						'orderby' => 'title'
					 );
		$wp_query = new WP_Query($args);
		$tests_labs = $wp_query->posts;
		if (!empty($tests_labs)) 
		{
			foreach ($tests_labs as $post) 
			{
				$id = $post -> ID;
				$title = $post -> post_title;
				$selected = "";
				if (in_array($id, $values))
				{
					$selected = "selected";
				}
				if( $tipo == 'product')
				{
					$texto_sku = $wpdb->get_var("SELECT meta_value from $wpdb->postmeta WHERE meta_key =  '_sku' AND post_id = '".$id."'");
					echo '<option value="'.$id.'" '.$selected.'>'.$texto_sku.'</option>';
				}
				else
				{
					echo '<option value="'.$id.'" '.$selected.'>'.$title.'</option>';
				}
				$i++;
			}
		}
	}
}


function em_mtbx_hotel_function( $post ) 
{
    $f_localidad = get_post_meta( $post->ID, 'f_localidad', true );
    $f_provincia = get_post_meta( $post->ID, 'f_provincia', true );
	$f_comunidad = get_post_meta( $post->ID, 'f_comunidad', true );
    $f_direccion = get_post_meta( $post->ID, 'f_direccion', true );
    $f_categoria = get_post_meta( $post->ID, 'f_categoria', true );
    $f_festivales = get_post_meta( $post->ID, 'f_festivales', true );
	$values = explode(',',$f_festivales);
	$f_festival = $f_concierto = $f_cenaespectaculo = $f_clasicaopera= $f_magiacirco = $f_danza = $f_monologos = $f_museos = $f_musicales = $f_teatro = '';
	foreach ($values as $value)
	{
		if (get_post_type($value) == 'festival') {$f_festival .= ",".$value;}
		if (get_post_type($value) == 'concierto') {$f_concierto .= ",".$value;}
		if (get_post_type($value) == 'espectaculo') {$f_evento .= ",".$value;}
		if (get_post_type($value) == 'clasicaopera') {$f_clasicaopera .= ",".$value;}
		if (get_post_type($value) == 'magiacirco') {$f_magiacirco .= ",".$value;}
		if (get_post_type($value) == 'danza') {$f_danza .= ",".$value;}
		if (get_post_type($value) == 'monologos') {$f_monologos .= ",".$value;}
		if (get_post_type($value) == 'museos') {$f_museos .= ",".$value;}
		if (get_post_type($value) == 'musicales') {$f_musicales .= ",".$value;}
		if (get_post_type($value) == 'teatro') {$f_teatro .= ",".$value;}
	}
		
    ?>
        <h4><?php _e("Localidad"); ?></h4>
        <input class="em_mtbx_img" name="f_localidad_alojamiento" id="f_localidad_alojamiento" type="text" value="<?php if($f_localidad && $f_localidad != '') echo $f_localidad; ?>">
        <h4><?php _e("Provincia"); ?></h4>
        <input class="em_mtbx_img" name="f_provincia_alojamiento" id="f_provincia_alojamiento" type="text" value="<?php if($f_provincia && $f_provincia != '') echo $f_provincia; ?>">
        <h4><?php _e("Comunidad Autónoma"); ?></h4>
        <input class="em_mtbx_img" name="f_comunidad_alojamiento" id="f_comunidad_alojamiento" type="text" value="<?php if($f_comunidad && $f_comunidad != '') echo $f_comunidad; ?>">
        <h4><?php _e("Dirección"); ?></h4>
        <input class="em_mtbx_img" name="f_direccion_alojamiento" id="f_direccion_alojamiento" type="text" value="<?php if($f_direccion && $f_direccion != '') echo $f_direccion; ?>">
        <h4><?php _e("Categoría"); ?></h4>
        <input class="em_mtbx_img" name="f_categoria_alojamiento" id="f_categoria_alojamiento" type="text" value="<?php if($f_categoria && $f_categoria != '') echo $f_categoria; ?>">
        <h4><?php _e("Festival"); ?></h4>

        <select multiple name="f_festivales_alojamiento[]" size="10px">
        <?php listado_hotel('festival', $f_festival);?>
        </select>

        <h4><?php _e("Conciertos"); ?></h4>
        <select multiple name="f_concierto_alojamiento[]" size="10px">
        <?php listado('concierto', $f_concierto);?>
        </select>
		
        <h4><?php _e("Cena Espectáculo"); ?></h4>
        <select multiple name="f_evento_alojamiento[]" size="10px">
        <?php listado(espectaculo, $f_evento);?>
        </select>
		
        <h4><?php _e("Clásica y Ópera"); ?></h4>
        <select multiple name="f_clasicaopera_alojamiento[]" size="10px">
        <?php listado(clasicaopera, $f_clasicaopera);?>
        </select>
		
        <h4><?php _e("Magia y Circo"); ?></h4>
        <select multiple name="f_magiacirco_alojamiento[]" size="10px">
        <?php listado(magiacirco, $f_magiacirco);?>
        </select>
		
        <h4><?php _e("Danza"); ?></h4>
        <select multiple name="f_danza_alojamiento[]" size="10px">
        <?php listado(danza, $f_danza);?>
        </select>
		
        <h4><?php _e("Monólogos"); ?></h4>
        <select multiple name="f_monologos_alojamiento[]" size="10px">
        <?php listado(monologos, $f_monologos);?>
        </select>
		
        <h4><?php _e("Museos"); ?></h4>
        <select multiple name="f_museos_alojamiento[]" size="10px">
        <?php listado(museos, $f_museos);?>
        </select>
		
        <h4><?php _e("Musicales"); ?></h4>
        <select multiple name="f_musicales_alojamiento[]" size="10px">
        <?php listado(musicales, $f_musicales);?>
        </select>
		
        <h4><?php _e("Teatro"); ?></h4>
        <select multiple name="f_teatro_alojamiento[]" size="10px">
        <?php listado(teatro, $f_teatro);?>
        </select>
        <div class="clear"></div>


  <?php       
}






//Para grabar los campos. Añadir uno nuevo por cada campo existente, en este caso tres.
add_action( 'save_post', 'em_mtbx_hotel_save_meta' );

function em_mtbx_hotel_save_meta( $post_id ) {

	if ( isset( $_POST['f_localidad_alojamiento'] ) ) {
	
		update_post_meta( $post_id, 'f_localidad', $_POST['f_localidad_alojamiento']  );

	}
	if ( isset( $_POST['f_provincia_alojamiento'] ) ) {
	
		update_post_meta( $post_id, 'f_provincia', $_POST['f_provincia_alojamiento']  );

	}
	if ( isset( $_POST['f_comunidad_alojamiento'] ) ) {
	
		update_post_meta( $post_id, 'f_comunidad', $_POST['f_comunidad_alojamiento']  );

	}
	if ( isset( $_POST['f_direccion_alojamiento'] ) ) {
	
		update_post_meta( $post_id, 'f_direccion', $_POST['f_direccion_alojamiento']  );

	}
	if ( isset( $_POST['f_categoria_alojamiento'] ) ) {
	
		update_post_meta( $post_id, 'f_categoria', $_POST['f_categoria_alojamiento']  );

	}

/*
	if ( 	isset( $_POST['f_festivales_alojamiento'] ) ||  isset( $_POST['f_concierto_alojamiento'] )  || 
			isset( $_POST['f_evento_alojamiento'] )     ||  isset( $_POST['f_clasicaopera_alojamiento'] ) || 
            isset( $_POST['f_magiacirco_alojamiento'] ) ||  isset( $_POST['f_danza_alojamiento'] )      || 
			isset( $_POST['f_monologos_alojamiento'] )  ||  isset( $_POST['f_museos_alojamiento'] ) 	|| 
            isset( $_POST['f_musicales_alojamiento'] ) 	||  isset( $_POST['f_teatro_alojamiento'] )
	   ) 
	   {
		foreach ($_POST['f_festivales_alojamiento'] as $f_festival){$f_festivales .= ",".$f_festival;}
		foreach ($_POST['f_concierto_alojamiento'] as $f_concierto){$f_festivales .= ",".$f_concierto;}
		foreach ($_POST['f_evento_alojamiento'] as $f_evento){$f_festivales .= ",".$f_evento;}
		foreach ($_POST['f_clasicaopera_alojamiento'] as $f_clasicaopera){$f_festivales .= ",".$f_clasicaopera;}
		foreach ($_POST['f_magiacirco_alojamiento'] as $f_magiacirco){$f_festivales .= ",".$f_magiacirco;}
		foreach ($_POST['f_danza_alojamiento'] as $f_danza){$f_festivales .= ",".$f_danza;}
		foreach ($_POST['f_monologos_alojamiento'] as $f_monologos){$f_festivales .= ",".$f_monologos;}
		foreach ($_POST['f_museos_alojamiento'] as $f_museos){$f_festivales .= ",".$f_museos;}
		foreach ($_POST['f_musicales_alojamiento'] as $f_musicales){$f_festivales .= ",".$f_musicales;}		
		foreach ($_POST['f_teatro_alojamiento'] as $f_teatro){$f_festivales .= ",".$f_teatro;}

		update_post_meta( $post_id, 'f_festivales', $f_festivales.","  );

	}
*/	
	if 	(isset( $_POST['f_festivales_alojamiento']))
	{
		foreach ($_POST['f_festivales_alojamiento'] as $f_festival){$f_festivales .= ",".$f_festival;}
	}
	if	(isset( $_POST['f_concierto_alojamiento']))
	{
		foreach ($_POST['f_concierto_alojamiento'] as $f_concierto){$f_festivales .= ",".$f_concierto;}	
	}
	if	(isset( $_POST['f_evento_alojamiento'])) 
	{
		foreach ($_POST['f_evento_alojamiento'] as $f_evento){$f_festivales .= ",".$f_evento;}	
	}
	if	(isset( $_POST['f_clasicaopera_alojamiento']))
	{
		foreach ($_POST['f_clasicaopera_alojamiento'] as $f_clasicaopera){$f_festivales .= ",".$f_clasicaopera;}		
	}
	if	(isset( $_POST['f_magiacirco_alojamiento']))
	{
		foreach ($_POST['f_magiacirco_alojamiento'] as $f_magiacirco){$f_festivales .= ",".$f_magiacirco;}
		
	}
	if	(isset( $_POST['f_danza_alojamiento']))  
	{
		foreach ($_POST['f_danza_alojamiento'] as $f_danza){$f_festivales .= ",".$f_danza;}		
	}
	if	(isset( $_POST['f_monologos_alojamiento']))
	{
		foreach ($_POST['f_monologos_alojamiento'] as $f_monologos){$f_festivales .= ",".$f_monologos;}		
	}
	if	(isset( $_POST['f_museos_alojamiento']))
	{
		foreach ($_POST['f_museos_alojamiento'] as $f_museos){$f_festivales .= ",".$f_museos;}		
	}
    if  (isset( $_POST['f_musicales_alojamiento']))
	{
		foreach ($_POST['f_musicales_alojamiento'] as $f_musicales){$f_festivales .= ",".$f_musicales;}				
	}
	if	(isset( $_POST['f_teatro_alojamiento']))
	{
		foreach ($_POST['f_teatro_alojamiento'] as $f_teatro){$f_festivales .= ",".$f_teatro;}
		
	}
		update_post_meta( $post_id, 'f_festivales', $f_festivales.","  );
	
}