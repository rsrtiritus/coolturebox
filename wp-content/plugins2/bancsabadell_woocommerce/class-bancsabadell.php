<?php

/**
 * Plugin Name: TPV Virtual del Banco Sabadell
 * Plugin URI: http://www.bancsabadell.es
 * Description: Pago mediante el TPV Virtual del Banco Sabadell
 * Version: 2.0
 * Author: Banco Sabadell
 *
 *
 */
require_once('php/apiRedsys.php');
require_once('php/SabadellTPV.php');


if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {

    //Agregamos el hook de activacion que se ejecuta cuando activamos el plugin.
    register_activation_hook(__FILE__, 'bancosabadell_register');

    /**
     * Este metodo se ejecutará cuando se active el módulo
     */
    function bancosabadell_register()
    {
        global $wpdb;

        $table_name = $wpdb->prefix . "sabadell_operaciones";
        $sql = "CREATE TABLE IF NOT EXISTS " . $table_name . " (
                    `id_order` int(10) NOT NULL,                    
                    `merchant_order` varchar(12) NOT NULL,
                    `merchant_code` varchar(9) NOT NULL,
                    `importe_cobrado` decimal(17,2) NOT NULL,
                    `importe_devuelto` decimal(17,2) NOT NULL,
                    `id_terminal` int(3) DEFAULT NULL,
                    `moneda` varchar(3) DEFAULT NULL,
                    `clave_encriptacion` text NOT NULL,
                    `tipo_pago` varchar(2) DEFAULT NULL,
                    `id_estado` int(1) NOT NULL,
                    PRIMARY KEY (`id_order`)
		);";


        //Guardaremos los datos del pago por referencia.		 
        $table_name = $wpdb->prefix . "sabadell_pago_referencia";
        $sql .= "CREATE TABLE IF NOT EXISTS " . $table_name . " (
                    id_customer int(10) NOT NULL,                    
                    numTarjeta   varchar(22), 
                    referencia   varchar(40) NOT NULL,
                    caducidad_tarjeta varchar(10) NOT NULL,                                        
                PRIMARY KEY(id_customer,numTarjeta))";


        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }

    /**
     * Inserta una nueva operación en el registro de operaciones del plugin.
     * @global type $wpdb
     * @param type $id_order
     * @param type $merchant_order
     * @param type $merchant_code
     * @param type $importe_cobrado
     * @param type $id_terminal
     * @param type $moneda
     * @param type $clave_encriptacion
     * @param type $tipo_pago
     * @param type $id_estado
     */
    function insertaOperacion($id_order, $merchant_order, $merchant_code, $importe_cobrado, $id_terminal, $moneda, $clave_encriptacion, $tipo_pago, $id_estado, &$pedido, $anotacion)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "sabadell_operaciones";

        $wpdb->insert($table_name, array(
            'id_order' => $id_order,
            'merchant_order' => $merchant_order,
            'merchant_code' => $merchant_code,
            'importe_cobrado' => $importe_cobrado,
            'id_terminal' => $id_terminal,
            'moneda' => $moneda,
            'clave_encriptacion' => $clave_encriptacion,
            'tipo_pago' => $tipo_pago,
            'id_estado' => $id_estado));

        //Actualizamos el estado de la orden
        actualizaEstadoWooCommerce($pedido, $id_estado, $anotacion);
    }

    /**
     * Obtenemos todas las tarjetas del cliente pasado como parámetro
     * @param type $idCustomer
     * @return type
     */
    function obtenerTarjetasCliente($idCustomer)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "sabadell_pago_referencia";
        $tarjetas = null;

        try {
            if ($idCustomer > 0) {
                $sql = "SELECT id_customer, numTarjeta, referencia, caducidad_tarjeta FROM " . $table_name . " WHERE id_customer=" . $idCustomer;
                $tarjetas = $wpdb->get_results($sql, ARRAY_A);
            }
        } catch (Exception $e) {
            return null;
        }

        return $tarjetas;
    }

    /**
     * Obtenemos los datos de una tarjeta en concreto
     * @param type $idCustomer
     * @param type $numTarjeta
     * @return type
     */
    function obtenerTarjetaCliente($idCustomer, $numTarjeta)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "sabadell_pago_referencia";
        $tarjeta = null;

        try {
            if ($idCustomer > 0 && !empty($numTarjeta)) {
                $sql = "SELECT id_customer, numTarjeta, referencia, caducidad_tarjeta FROM " . $table_name . " WHERE id_customer=" . $idCustomer . " AND numTarjeta='" . $numTarjeta . "'";
                $tarjeta = $wpdb->get_row($sql, ARRAY_A);
            }
        } catch (Exception $e) {
            return null;
        }

        return $tarjeta;
    }

    /**
     * * Agrega una tarjeta a un cliente.
     * @param type $idcustomer
     * @param type $numTarjeta
     * @param type $fechaCaducidad
     * @param type $referencia
     * @return type
     */
    function nuevaTarjetaCliente($idcustomer, $numTarjeta, $fechaCaducidad, $referencia)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "sabadell_pago_referencia";
        try {

            $tarjeta = obtenerTarjetaCliente($idCustomer, $numTarjeta);

            //si ya existia la actualizamos.
            if (is_null($tarjeta)) {
                $wpdb->insert($table_name, array(
                    'id_customer' => $idcustomer,
                    'numTarjeta' => $numTarjeta,
                    'caducidad_tarjeta' => $fechaCaducidad,
                    'referencia' => $referencia));
            } else {
                $wpdb->update(
                    $table_name,
                    array('caducidad_tarjeta' => $fechaCaducidad, 'referencia' => $referencia),
                    array('id_customer' => $idcustomer, 'numTarjeta' => $numTarjeta)
                );
            }
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Borra la tarjeta del cliente
     * @param type $idcustomer Identificador del cliente
     * @param type $numTarjeta Identificador de la tarjeta
     * @return type
     */
    function borraTarjetaCliente($idcustomer, $numTarjeta)
    {
        global $wpdb;
        try {
            if (!empty($idcustomer) && !empty($numTarjeta)) {
                $table_name = $wpdb->prefix . "sabadell_pago_referencia";
                return $wpdb->delete($table_name, array('id_customer' => $idcustomer, 'numTarjeta' => $numTarjeta), array('%d', '%s'));
            }
            return null;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Actualiza el estado en la tabla Purchase Logs de WP-E-Commerce
     * @param type $id_order
     * @param type $estadoSabadellTPV
     */
    function actualizaEstadoWooCommerce($id_order, $id_estado, $anotacion)
    {
        global $wpdb;

        $estadoWooCommerce = "";

        switch ($id_estado) {
            case SabadellTPV::PAGO_ACEPTADO:
                $estadoWooCommerce = 'wc-pago_aceptado';
                break;
            case SabadellTPV::DEVOLUCION_COMPLETA:
                $estadoWooCommerce = 'wc-dev_comp';
                break;
            case SabadellTPV::DEVOLUCION_PARCIAL:
                $estadoWooCommerce = 'wc-dev_parc';
                break;
            case SabadellTPV::PAGO_PREAUTORIZADO:
                $estadoWooCommerce = 'wc-pago_preau';
                break;
            case SabadellTPV::PREAUTORIZACION_CONFIRMADA:
                $estadoWooCommerce = 'wc-preau_conf';
                break;
            case SabadellTPV::PREAUTORIZACION_ANULADA:
                $estadoWooCommerce = 'wc-preau_anul';
                break;
            case SabadellTPV::AUTENTICACION_REALIZADA:
                $estadoWooCommerce = 'wc-aut_realizada';
                break;
            case SabadellTPV::AUTENTICACION_CONFIRMADA:
                $estadoWooCommerce = 'aut_conf';
                break;
        }

        $order = new WC_Order($id_order);
        if (isset($order) && isset($estadoWooCommerce)) {
            $order->update_status($estadoWooCommerce, $anotacion);
        }
    }

    /**
     * Actualiza el estado de una operación
     * @param $id_order
     * @param $id_estado
     * @param $anotacion
     */
    function actualizaEstadoOperacion($id_order, $id_estado, $anotacion)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "sabadell_operaciones";
        $wpdb->update($table_name, array('id_estado' => $id_estado), array('id_order' => $id_order));
        actualizaEstadoWooCommerce($id_order, $id_estado, $anotacion);
    }

    /**
     * Actualiza el estado de una devolución actualizando el importe devuelto.
     * @global type $id_order
     * @param type $importeDevuelto
     * @param type $anotacion
     */
    function actualizaEstadoDevolucion($id_order, $importeDevuelto, $anotacion)
    {
        global $wpdb;

        $table_name = $wpdb->prefix . "sabadell_operaciones";

        $importeDevueltoAnterior = 0;
        $importeCobrado = 0;
        $estadoSabadellTPV = SabadellTPV::DEVOLUCION_PARCIAL;

        $sql = 'SELECT importe_cobrado, importe_devuelto FROM  ' . $table_name . ' WHERE id_order = ' . $id_order;
        $fila = $wpdb->get_row($sql);

        if (isset($fila)) {
            $importeCobrado = $fila->importe_cobrado;
            $importeDevueltoAnterior = $fila->importe_devuelto;
        }

        $importeDevuelto = $importeDevuelto + $importeDevueltoAnterior;

        //Precisión de la comparación
        $epsilon = 0.001;

        //Si el importe devuelto es igual al total, seá una devolución completa, en caso contrario será una devolución parcial.
        if (abs(floatval($importeDevuelto) - floatval($importeCobrado)) < $epsilon) {
            $estadoSabadellTPV = SabadellTPV::DEVOLUCION_COMPLETA;
        }

        $wpdb->update($table_name, array('id_estado' => $estadoSabadellTPV, 'importe_devuelto' => $importeDevuelto), array('id_order' => $id_order));
        actualizaEstadoWooCommerce($id_order, $estadoSabadellTPV, $anotacion);
    }

    /**
     * Obtenemos los datos de una operación.
     * @global type $wpdb
     * @param type $id_order
     * @return type
     */
    function obtenerDatosOperacionSabadell($id_order)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "sabadell_operaciones";
        $pedido = null;

        if ($id_order > 0) {
            $sql = "SELECT  so.merchant_order, 
                            so.merchant_code, 
                            so.importe_cobrado, 
                            so.importe_devuelto, 
                            so.id_terminal, 
                            so.moneda, 
                            so.clave_encriptacion, 
                            so.tipo_pago, 
                            so.id_estado,                            
                            so.id_order
            FROM " . $table_name . " so         
            WHERE id_order = " . $id_order;


            $pedido = $wpdb->get_row($sql, ARRAY_A);
        }

        return $pedido;
    }

    //Register New Order Statuses
    function wpex_wc_register_post_statuses()
    {
        register_post_status('wc-pago_aceptado', array(
            'label' => __('PAGO ACEPTADO', 'woocommerce'),
            'public' => true,
            'exclude_from_search' => false,
            'show_in_admin_all_list' => true,
            'show_in_admin_status_list' => true,
            'label_count' => _n_noop('Approved (%s)', 'Approved (%s)', 'woocommerce')
        ));

        register_post_status('wc-dev_comp', array(
            'label' => __('DEVOLUCION COMPLETA', 'woocommerce'),
            'public' => true,
            'exclude_from_search' => false,
            'show_in_admin_all_list' => true,
            'show_in_admin_status_list' => true,
            'label_count' => _n_noop('Approved (%s)', 'Approved (%s)', 'woocommerce')
        ));

        register_post_status('wc-dev_parc', array(
            'label' => __('DEVOLUCION PARCIAL', 'woocommerce'),
            'public' => true,
            'exclude_from_search' => false,
            'show_in_admin_all_list' => true,
            'show_in_admin_status_list' => true,
            'label_count' => _n_noop('Approved (%s)', 'Approved (%s)', 'woocommerce')
        ));

        register_post_status('wc-pago_preau', array(
            'label' => __('PAGO PREAUTORIZADO', 'woocommerce'),
            'public' => true,
            'exclude_from_search' => false,
            'show_in_admin_all_list' => true,
            'show_in_admin_status_list' => true,
            'label_count' => _n_noop('Approved (%s)', 'Approved (%s)', 'woocommerce')
        ));

        register_post_status('wc-preau_conf', array(
            'label' => __('PREAUTORIZACION CONFIRMADA', 'woocommerce'),
            'public' => true,
            'exclude_from_search' => false,
            'show_in_admin_all_list' => true,
            'show_in_admin_status_list' => true,
            'label_count' => _n_noop('Approved (%s)', 'Approved (%s)', 'woocommerce')
        ));


        register_post_status('wc-preau_anul', array(
            'label' => __('PREAUTORIZACION ANULADA', 'woocommerce'),
            'public' => true,
            'exclude_from_search' => false,
            'show_in_admin_all_list' => true,
            'show_in_admin_status_list' => true,
            'label_count' => _n_noop('Approved (%s)', 'Approved (%s)', 'woocommerce')
        ));

        register_post_status('wc-aut_realizada', array(
            'label' => __('AUTENTICACION REALIZADA', 'woocommerce'),
            'public' => true,
            'exclude_from_search' => false,
            'show_in_admin_all_list' => true,
            'show_in_admin_status_list' => true,
            'label_count' => _n_noop('Approved (%s)', 'Approved (%s)', 'woocommerce')
        ));

        register_post_status('wc-aut_conf', array(
            'label' => __('AUTENTICACION CONFIRMADA', 'woocommerce'),
            'public' => true,
            'exclude_from_search' => false,
            'show_in_admin_all_list' => true,
            'show_in_admin_status_list' => true,
            'label_count' => _n_noop('Approved (%s)', 'Approved (%s)', 'woocommerce')
        ));
    }

    add_filter('init', 'wpex_wc_register_post_statuses');

    // Add New Order Statuses to WooCommerce
    function wpex_wc_add_order_statuses($order_statuses)
    {

        $order_statuses['wc-pago_aceptado'] = __('PAGO ACEPTADO', 'woocommerce');
        $order_statuses['wc-dev_comp'] = __('DEVOLUCION COMPLETA', 'woocommerce');
        $order_statuses['wc-dev_parc'] = __('DEVOLUCION PARCIAL', 'woocommerce');
        $order_statuses['wc-pago_preau'] = __('PAGO PREAUTORIZADO', 'woocommerce');
        $order_statuses['wc-preau_conf'] = __('PREAUTORIZACION CONFIRMADA', 'woocommerce');
        $order_statuses['wc-preau_anul'] = __('PREAUTORIZACION ANULADA', 'woocommerce');
        $order_statuses['wc-aut_realizada'] = __('AUTENTICACION REALIZADA', 'woocommerce');
        $order_statuses['wc-aut_conf'] = __('AUTENTICACION CONFIRMADA', 'woocommerce');

        return $order_statuses;
    }

    add_filter('wc_order_statuses', 'wpex_wc_add_order_statuses');
    add_action('plugins_loaded', 'init_bancsabadell_gateway', 0);

    function init_bancsabadell_gateway()
    {

        //Si no Existe la Clase de extension de metodo de pago salimos.
        if (!class_exists('WC_Payment_Gateway')) {
            return;
        }

        class WC_Gateway_BancSabadell extends WC_Payment_Gateway
        {

            private $BaseDirPath = "";

            //Constructor
            public function __construct()
            {


                $BaseDirPath = home_url() . DIRECTORY_SEPARATOR . 'wp-content' . DIRECTORY_SEPARATOR . 'plugins' . DIRECTORY_SEPARATOR . 'bancsabadell_woocommerce';

                //Identificador único de la pasarela de pago
                $this->id = 'bancsabadell_woocommerce';

                //Icono de la pasarela
                $this->icon = $BaseDirPath . DIRECTORY_SEPARATOR . 'logo.png';

                //Titulo del método de pago. Se mostrará en la página de Administración
                $this->method_title = __('TPV Virtual de Banco Sabadell', 'woocommerce');

                //Descripción del método depago. Se mostrará en la página de Administración				
                $this->method_description = __('Panel de configuración del metodo del TPV Virtual del Banco Sabadell', 'woocommerce');

                $this->log = new WC_Logger();
                $this->has_fields = false;

                // Cargamos los datos de configuracion
                $this->init_form_fields();

                //Leemos los datos de configuracion cargados en el método anterior.
                $this->init_settings();

                //URL Callback
                $this->UrlCallBack = add_query_arg('wc-api', 'WC_Gateway_BancSabadell', home_url('/'));

                //Valores de configuración por defecto
                $this->title = $this->get_option('title');
                $this->description = $this->get_option('description');

                //Resto de valores de configuración
                $this->URLReal = $this->get_option('URLReal');
                $this->URLTest = $this->get_option('URLTest');
                $this->EntornoActivo = $this->get_option('EntornoActivo');
                $this->TipoTransaccion = $this->get_option('TipoTransaccion');
                $this->PagoReferencia = $this->get_option('PagoReferencia');
                $this->NombreComercio = $this->get_option('NombreComercio');
                $this->CodigoFUC = $this->get_option('CodigoFUC');
                $this->Idioma = $this->get_option('Idioma');
                $this->Terminal = $this->get_option('Terminal');
                $this->Moneda = $this->get_option('Moneda');
                $this->ClaveEncriptacion = $this->get_option('ClaveEncriptacion');


                // Actions
                add_action('woocommerce_receipt_' . $this->id, array($this, 'receipt_page'));

                //Agregamos un "hook" para los ajustes del TPV Virtual
                add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));

                //Payment listener/API hook			
                add_action('woocommerce_api_wc_gateway_bancsabadell', array($this, 'check_callback_response'));
                add_action('woocommerce_payment_complete', 'check_callback_response');

                //Para agregar la caja no funciona add_meta_boxes y no se por que.
                //add_action('add_meta_boxes', array($this,'bancsabadell_detalle_orden_meta_box' ));	
                if (is_admin()) {
                    $this->bancsabadell_detalle_orden_meta_box();
                }
            }

            /**
             * Agregamos una "caja" al detall de la orden.
             */
            public function bancsabadell_detalle_orden_meta_box()
            {
                //Solo mostramos nuestra caja de opciones en el caso de que sea una operación con algun estado de los nuestros.					
                add_meta_box(
                    'bancsabadell-detalle-orden', //id  del Metabox
                    $this->title, //Titulo
                    array(&$this, 'bancsabadell_order_transaction'), //Función a la que llamamos
                    'shop_order', //Pagina que queremos editar
                    'normal', //Contexto
                    'default' //Prioridad
                );
            }

            /**
             * Método por el que obtenemos el contenido del bloque en el detalle de la operación
             * @param type $post
             */
            public function bancsabadell_order_transaction($post)
            {
                $operacion = new WC_Order($post->ID);
                $display = "";

                $estadosBS = array('pago_aceptado', 'dev_comp', 'dev_parc', 'pago_preau', 'preau_conf', 'preau_anul', 'aut_realizada', 'aut_conf');

                if (!in_array($operacion->status, $estadosBS)) {
                    $display = __('La operación no ha sido realizada mediante el TPV Virtual del Banco Sabadell', 'woocommerce');
                } else {
                    $display = ObtenerFormularioDetalleOrden($operacion, $this);
                }

                echo '<div class="panel-wrap woocommerce">';
                echo $display;
                echo '</div>';
            }

            /**
             * Inicializamos los valores de configuración del módulo
             */
            public function init_form_fields()
            {
                global $woocommerce;

                $idiomaISO = substr(get_locale(), 0, 2);

                //Tipos de transacción disponibles
                $tiposDeTransaccion = SabadellTPV::tiposTransaccion($idiomaISO);
                $TipoTransaccionArray = array();

                foreach ($tiposDeTransaccion as $key => $value) {
                    $TipoTransaccionArray[$value] = __($key, 'woocommerce');
                }


                //Idiomas disponibles en el TPV
                $idiomasDisponibles = SabadellTPV::idiomas($idiomaISO);
                $IdiomasArray = array();
                $IdiomasArray[''] = __('--Auto--', 'woocommerce');

                foreach ($idiomasDisponibles as $key => $value) {
                    $IdiomasArray[$value] = __($key, 'woocommerce');
                }

                //Monedas disponibles
                $monedasDisponibles = SabadellTPV::monedas($idiomaISO);
                $MonedasArray = array();

                foreach ($monedasDisponibles as $key => $value) {
                    $MonedasArray[$value] = __($key, 'woocommerce');
                }

                $this->form_fields = array(
                    'enabled' => array(
                        'title' => __('Activo', 'woocommerce'),
                        'label' => __('Activar/Desactivar TPV Virtual', 'woocommerce'),
                        'type' => 'checkbox',
                        'description' => '',
                        'default' => 'yes'
                    ),
                    'title' => array(
                        'title' => __('Texto', 'woocommerce'),
                        'type' => 'text',
                        'description' => __('Texto que se verá al mostrar el método de pago', 'woocommerce'),
                        'default' => __('TPV Virtual', 'woocommerce'),
                        'desc_tip' => true,
                    ),
                    'URLReal' => array(
                        'title' => __('URL entorno real', 'woocommerce'),
                        'type' => 'text',
                        'description' => __('URL del entorno de Producción', 'woocommerce'),
                        'default' => __(SabadellTPV::urlReal(), 'woocommerce'),
                        'desc_tip' => true,
                    ),
                    'URLTest' => array(
                        'title' => __('URL entorno de test', 'woocommerce'),
                        'type' => 'text',
                        'description' => __('URL del entorno de Test', 'woocommerce'),
                        'default' => __(SabadellTPV::urlTest(), 'woocommerce'),
                        'desc_tip' => true,
                    ),
                    'EntornoActivo' => array(
                        'title' => __('Entorno activo', 'woocommerce'),
                        'type' => 'select',
                        'description' => __('Entorno del proceso de pago.', 'woocommerce'),
                        'default' => 'TEST',
                        'desc_tip' => true,
                        'options' => array('TEST' => __('TEST', 'woocommerce'), 'REAL' => __('REAL', 'woocommerce'))
                    ),
                    'TipoTransaccion' => array(
                        'title' => __('Tipo de transacción', 'woocommerce'),
                        'type' => 'select',
                        'description' => __('Tipo de transacción que queremos ejecutar', 'woocommerce'),
                        'default' => '0',
                        'desc_tip' => true,
                        'options' => $TipoTransaccionArray
                    ),
                    'PagoReferencia' => array(
                        'title' => __('Pago por referencia', 'woocommerce'),
                        'label' => __('Activar/Desactivar el Pago por referencia', 'woocommerce'),
                        'type' => 'checkbox',
                        'description' => 'Para poder usar esta opción deberá solicitarla a su oficina para que sea soportada por el TPV Virtual. <strong>Para el correcto funcionamiento de esta opción deberá solicitar la devolucion de la tarjeta asteriscada</strong>',
                        'default' => 'no'
                    ),
                    'NombreComercio' => array(
                        'title' => __('Nombre del Comercio', 'woocommerce'),
                        'type' => 'text',
                        'description' => __('Nombre del Comercio', 'woocommerce'),
                        'default' => __('', 'woocommerce'),
                        'desc_tip' => true,
                    ),
                    'CodigoFUC' => array(
                        'title' => __('Código FUC', 'woocommerce'),
                        'type' => 'text',
                        'description' => __('Identificador del comercio', 'woocommerce'),
                        'default' => __('', 'woocommerce'),
                        'desc_tip' => true,
                    ),
                    'Idioma' => array(
                        'title' => __('Idioma', 'woocommerce'),
                        'type' => 'select',
                        'description' => __('Idioma en el que queremos que aparezca el TPV Virtual. Si seleccionamos auto se configurará automaticamente dependiendo del idioma de la tienda.', 'woocommerce'),
                        'default' => '0',
                        'desc_tip' => true,
                        'options' => $IdiomasArray
                    ),
                    'Terminal' => array(
                        'title' => __('Número de terminal', 'woocommerce'),
                        'type' => 'text',
                        'description' => __('Terminal del comercio.', 'woocommerce'),
                        'default' => __('', 'woocommerce'),
                        'desc_tip' => true,
                    ),
                    'Moneda' => array(
                        'title' => __('Moneda', 'woocommerce'),
                        'type' => 'select',
                        'description' => __('Moneda con la que funciona el terminal.', 'woocommerce'),
                        'default' => '978',
                        'desc_tip' => true,
                        'options' => $MonedasArray
                    ),
                    'ClaveEncriptacion' => array(
                        'title' => __('Clave de encriptación', 'woocommerce'),
                        'type' => 'text',
                        'description' => __('Clave de encriptación proporcionada por el Banco Sabadell', 'woocommerce'),
                        'default' => __('', 'woocommerce'),
                        'desc_tip' => true,
                    )
                );
            }

            /**
             * Página de confirmación del pago.
             * @param type $order
             */
            function receipt_page($order)
            {
                $this->log->add('BancSabadell', 'Acceso a la página de confirmación de la opción de pago con tarjeta. ');

                //Comprobamos si viene algo en el post por si tenemos que borrar una tarjeta.
                if (!empty($_POST)) {

                    $idCustomer = null;
                    $numTarjeta = null;
                    $operacion = null;
                    if (isset($_POST['id_customer'])) {
                        $idCustomer = $_POST['id_customer'];
                    }

                    if (isset($_POST['numTarjeta'])) {
                        $numTarjeta = $_POST['numTarjeta'];
                    }

                    if (isset($_POST['operacion'])) {
                        $operacion = $_POST['operacion'];
                    }


                    if ($operacion == "borrado" && !empty($idCustomer) && !empty($numTarjeta)) {
                        borraTarjetaCliente($idCustomer, $numTarjeta);
                    }
                }

                echo $this->ObtenerFormularioBS($order);
            }

            /**
             * Generamos el formulario para el FrontEnd
             * @param type $order_id
             * @return string
             */
            function ObtenerFormularioBS($order_id)
            {

                $idiomaISO = substr(get_locale(), 0, 2);
                $idiomaTPV = null;
                if (isset($this->Idioma) && is_numeric($this->Idioma)) {
                    $idiomaTPV = $this->Idioma;
                } else {
                    $idiomaTPV = SabadellTPV::isoToTPV($idiomaISO);
                }

                //Obtenemos los datos del pedido por su id
                $order = new WC_Order($order_id);

                //Tiular del pedido
                $titular = $order->billing_first_name . ' ' . $order->billing_last_name;

                //Calculo del precio total del pedido
                $importePedido = number_format((float)($order->get_total()), 2, '.', '');
                $importePedido = str_replace('.', '', $importePedido);
                $importePedido = floatval($importePedido);


                // Datos del carrito de la compra
                $productos = WC()->cart->cart_contents;
                $carritoCompra = '';
                foreach ($productos as $producto) {
                    $carritoCompra .= $producto['quantity'] . '-' . $producto['data']->post->post_title . "<br>";
                }

                //Si el carrito de la compra supera los 125 caracteres lo truncamos ya que el TPV virtual no soporta una longitud tan larga.
                if (strlen($carritoCompra) >= 125) {
                    $carritoCompra = substr($carritoCompra, 0, 120) . '...';
                }

                // El número de pedido es  los 8 ultimos digitos del ID del carrito + el tiempo MMSS.
                $codigoPedido = str_pad($order_id, 8, "0", STR_PAD_LEFT) . date("is");

                $urlRespuestaTPV = urlencode($this->UrlCallBack);
                $urlOK = urlencode($this->get_return_url($order));
                $urlKO = urlencode($order->get_cancel_order_url());

                $html = "";

                //Este campo solo lo empleamos si tenemos activo el pago por referencia
                $Ds_Merchant_Identifier = "";
                if ($this->PagoReferencia == "yes") {
                    $html .= $this->ObtenerListadoTarjetasCliente(get_current_user_id(),$codigoPedido);
                    $html .= '<button type="button" onclick="RealizarPago()">' . __('Confirmar pedido', 'woocommerce') . '</button>';
                } else {
                    $html .= '<p>' . __('Gracias por su pedido, por favor pulsa el botón para confirmar el pedido y pagar mediante el TPV Virtual.', 'woocommerce') . '</p>';
                    $html .= '<button type="button" onclick="document.getElementById(\'SabadellTPVForm\').submit();">Confirmar pedido</button>';
                }


                if ($this->EntornoActivo == "TEST") {
                    $urlPost = $this->URLTest;
                } else {
                    $urlPost = $this->URLReal;
                }

                $redsysAPI = new RedsysAPI;
                $redsysAPI->setParameter("DS_MERCHANT_AMOUNT", $importePedido);
                $redsysAPI->setParameter("DS_MERCHANT_CURRENCY", $this->Moneda);
                $redsysAPI->setParameter("DS_MERCHANT_ORDER", $codigoPedido);
                $redsysAPI->setParameter("DS_MERCHANT_MERCHANTCODE", $this->CodigoFUC);
                $redsysAPI->setParameter("DS_MERCHANT_TERMINAL", $this->Terminal);
                $redsysAPI->setParameter("DS_MERCHANT_TRANSACTIONTYPE", $this->TipoTransaccion);
                $redsysAPI->setParameter("DS_MERCHANT_TITULAR", $titular);
                $redsysAPI->setParameter("DS_MERCHANT_MERCHANTNAME", $this->NombreComercio);
                $redsysAPI->setParameter("DS_MERCHANT_MERCHANTURL", $urlRespuestaTPV);
                $redsysAPI->setParameter("DS_MERCHANT_PRODUCTDESCRIPTION", $carritoCompra);
                $redsysAPI->setParameter("DS_MERCHANT_URLOK", $urlOK);
                $redsysAPI->setParameter("DS_MERCHANT_URLKO", $urlKO);
                $redsysAPI->setParameter("DS_MERCHANT_CONSUMERLANGUAGE", $idiomaTPV);
                $redsysAPI->setParameter("DS_MERCHANT_IDENTIFIER", $Ds_Merchant_Identifier);

                $DsMerchantParameters = $redsysAPI->createMerchantParameters();
                $DsSignature = $redsysAPI->createMerchantSignature($this->ClaveEncriptacion);

                $html .= SabadellTPV::obtenerFormularioTPV($urlPost, $importePedido, $this->Moneda, $codigoPedido, $this->CodigoFUC, $this->Terminal, $this->TipoTransaccion, $titular, $this->NombreComercio, $urlRespuestaTPV, $carritoCompra, $urlOK, $urlKO, $idiomaTPV, 'SabadellTPVForm', null, $Ds_Merchant_Identifier, $DsMerchantParameters, $DsSignature);

                return $html;
            }

            /**
             * Obtenemos un
             * @param type $customerId
             */
            function ObtenerListadoTarjetasCliente($idCustomer, $codigoPedido)
            {
                global $wp;

                $urlDeVuelta = add_query_arg($wp->query_string, '', home_url($wp->request));
                $urlBorraTarjeta = 'http://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
                $urlAjax = plugins_url('php/ObtenerFirma.php', __FILE__);
                $tarjetas = obtenerTarjetasCliente($idCustomer);


                $html = '<img src="' . home_url() . '/wp-content/plugins/bancsabadell_woocommerce/img/Tarjetas.png" alt="Banc Sabadell" /><br>';
                //$html .= "[".$idCustomer."]";
                $html .= '<script type="text/javascript">

                            var default_Ds_Merchant_Identifier ;
                            var default_Ds_Signature;
                            var default_Ds_MerchantParameters;

                            jQuery(document ).ready(function() {
                                 default_Ds_Merchant_Identifier = document.getElementById("Ds_Merchant_Identifier").value;
                                 default_Ds_Signature =  document.getElementById("Ds_Signature").value;
                                 default_Ds_MerchantParameters = document.getElementById("Ds_MerchantParameters").value;
                            });



                            function SetDefaultValues(){

                                document.getElementById("Ds_Merchant_Identifier").value = default_Ds_Merchant_Identifier;
                                document.getElementById("Ds_Signature").value = default_Ds_Signature;
                                document.getElementById("Ds_MerchantParameters").value = default_Ds_MerchantParameters;

                            }


                            function BorraTarjeta(numTarjeta){

                                    if(numTarjeta!=null){  
                                            if(confirm("' . __('¿Desea eliminar la  tarjeta ', 'woocommerce') . '" + numTarjeta + "?")){                                                   
                                                     jQuery("#id_customer").val(' . $idCustomer . ');	
                                                     jQuery("#numTarjeta").val(numTarjeta);	
                                                     jQuery("#BorradoTarjetaForm").submit();
                                             }			
                                    } else {                                              
                                            alert("' . __('Debe de seleccionar la tarjeta para confirmar su eliminación.', 'woocommerce') . '");  
                                    } 												 				
                                    return false; 											
                            }
                            
                            function radioSeleccionado(referencia,hashFirma) {
                                if(referencia!=null){  
                                    jQuery("#Ds_Merchant_Identifier").val(referencia);

                                    var xmlhttp;
                                    var ds_merchant_amount 			    = document.getElementById("Ds_Merchant_Amount").value;
                                    var ds_merchant_order 			    = document.getElementById("Ds_Merchant_Order").value;
                                    var ds_merchant_merchantcode 	    = document.getElementById("Ds_Merchant_MerchantCode").value;
                                    var ds_merchant_currency		    = document.getElementById("Ds_Merchant_Currency").value;
                                    var ds_merchant_transactiontype     = document.getElementById("Ds_Merchant_TransactionType").value;
                                    var ds_merchant_terminal 		    = document.getElementById("Ds_Merchant_Terminal").value;
                                    var ds_merchant_merchanturl 	    = document.getElementById("Ds_Merchant_MerchantURL").value;
                                    var ds_merchant_urlok 			    = document.getElementById("Ds_Merchant_UrlOK").value;
                                    var ds_merchant_urlko 			    = document.getElementById("Ds_Merchant_UrlKO").value;
                                    var ds_Merchant_Titular 			= document.getElementById("Ds_Merchant_Titular").value;
                                    var ds_Merchant_MerchantName 		= document.getElementById("Ds_Merchant_MerchantName").value;
                                    var ds_Merchant_ProductDescription 	= document.getElementById("Ds_Merchant_ProductDescription").value;
                                    var ds_Merchant_ConsumerLanguage 	= document.getElementById("Ds_Merchant_ConsumerLanguage").value;
                                    var ds_Merchant_Identifier          = referencia;


                                    var datos = "DS_MERCHANT_AMOUNT="  + ds_merchant_amount;
                                    datos += "&DS_MERCHANT_ORDER=" + ds_merchant_order;
                                    datos += "&DS_MERCHANT_MERCHANTCODE=" +ds_merchant_merchantcode;
                                    datos += "&DS_MERCHANT_CURRENCY=" +ds_merchant_currency;
                                    datos += "&DS_MERCHANT_TRANSACTIONTYPE=" +ds_merchant_transactiontype;
                                    datos += "&DS_MERCHANT_TERMINAL=" + ds_merchant_terminal;
                                    datos += ds_merchant_merchanturl ? "&DS_MERCHANT_MERCHANTURL=" +ds_merchant_merchanturl : "";
                                    datos += ds_merchant_urlok ? "&DS_MERCHANT_URLOK=" + ds_merchant_urlok : "";
                                    datos += ds_merchant_urlko ? "&DS_MERCHANT_URLKO=" + ds_merchant_urlko : "";

                                    datos += ds_Merchant_Titular ? "&DS_MERCHANT_TITULAR=" + ds_Merchant_Titular : "";
                                    datos += ds_Merchant_MerchantName ? "&DS_MERCHANT_MERCHANTNAME=" + ds_Merchant_MerchantName : "";
                                    datos += ds_Merchant_ProductDescription ? "&DS_MERCHANT_PRODUCTDESCRIPTION=" + ds_Merchant_ProductDescription : "";
                                    datos += ds_Merchant_ConsumerLanguage ? "&DS_MERCHANT_CONSUMERLANGUAGE=" + ds_Merchant_ConsumerLanguage : "";
                                    datos += ds_Merchant_Identifier ? "&DS_MERCHANT_IDENTIFIER=" + ds_Merchant_Identifier : "";
                                    datos += "&HASH=" + hashFirma ;

                                    xmlhttp = new XMLHttpRequest();
                                    xmlhttp.open("POST", "' . $urlAjax . '", true);
                                    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                    xmlhttp.send(datos);


                                    xmlhttp.onreadystatechange = function() {
                                        if (xmlhttp.readyState == 4) {
                                            var respuesta = xmlhttp.responseText;
                                            var jsonRespuesta = JSON.parse(respuesta);
                                            document.getElementById("Ds_Signature").value = jsonRespuesta["Ds_Signature"];
                                            document.getElementById("Ds_MerchantParameters").value = jsonRespuesta["Ds_MerchantParameters"] ;

                                        }
                                    }

                                }                                
                            }

                            function RealizarPago(){

                                    var valor = jQuery("input[name=tarjetaSeleccionada]:checked").val();																							
                                    if(valor!=null){  
                                            if(confirm("' . __('¿Confirma el pago con la tarjeta seleccionada?', 'woocommerce') . '")){
                                                    jQuery("#SabadellTPVForm").submit();
                                            }							
                                    }
                                    else{
                                            alert("' . __('Debe de seleccionar al menos una tarjeta para continuar.', 'woocommerce') . '");  
                                    }
                            }	

                            </script>
                        ';

                $html .= '<hr/><p><h3>Pagar con tarjeta seleccionada</h3>' . __('Por favor, seleccione una tarjeta con la que realizar el pago si ha realizado antes compras con nosotros o bien seleccione otra de las opciones disponibles.', 'woocommerce') . '<br><br>';

                $html .= '<form action="' . $urlBorraTarjeta . '" method="post" id="BorradoTarjetaForm">'
                    . '<input type="hidden" name="id_customer" id="id_customer" />'
                    . '<input type="hidden" name="numTarjeta" id="numTarjeta" />'
                    . '<input type="hidden" name="operacion" id="operacion" value="borrado" />'
                    . '</form>';

                $html .= '<table border="0" width="100%" cellpadding="0" cellspacing="10" id="form">';

                if (count($tarjetas) > 0) {
                    foreach ($tarjetas as $tarjeta) {
                        $html .= '<tr>'
                            . '<td><input type="radio"  onClick="radioSeleccionado(\'' . $tarjeta['referencia'] . '\',\''. EncriptarClaveEncriptacion($this->ClaveEncriptacion, $codigoPedido) . '\' )" id="tarjetaSeleccionada" name="tarjetaSeleccionada" value="' . $tarjeta['numTarjeta'] . '"> &nbsp;' . $tarjeta['numTarjeta'] . '</td>'
                            . '<td><a id="borraTarjeta" href="#" onClick="BorraTarjeta(\'' . $tarjeta['numTarjeta'] . '\');"><img border=0 src="' . home_url() . '/wp-content/plugins/bancsabadell_woocommerce/img/borrar.gif" alt="' . __('Borrar tarjeta', 'woocommerce') . '" title="' . __('Borrar tarjeta', 'woocommerce') . '" ></a></td>'
                            . '</tr>';
                    }
                }

                $html .= '</table><br>';


                $html .= '<table border="0" width="100%" cellpadding="0" cellspacing="10" id="form">'
                    . '<tr>'
                    . '<td><input type="radio"  onClick="radioSeleccionado(\'REQUIRED\',\''. EncriptarClaveEncriptacion($this->ClaveEncriptacion, $codigoPedido) . '\')" id="tarjetaSeleccionada" name="tarjetaSeleccionada" value="nueva"> &nbsp;' . __('Pagar con tarjeta y recordar para otras ocasiones.', 'woocommerce') . '</td>'
                    . '<td>&nbsp;</td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td><input type="radio"  onClick="SetDefaultValues()" id="tarjetaSeleccionada" name="tarjetaSeleccionada" value="nuevaNoGuardar" checked="checked"> &nbsp;' . __('Pagar con otra tarjeta y <strong>no</strong> recordar.', 'woocommerce') . '</td>'
                    . '<td>&nbsp;</td>'
                    . '</tr>';


                $html .= '</table>';


                return $html;
            }

            /**
             * Este método indica donde redirigir al usuario
             * @global type $woocommerce
             * @param type $order_id
             * @return type
             */
            function process_payment($order_id)
            {
                global $woocommerce;

                $order = new WC_Order($order_id);
                $this->log->add('BancSabadell', 'Acceso al TPV Virtual del Banco Sabadell ');
                //Redirigimos al usuario
                return array('result' => 'success', 'redirect' => $order->get_checkout_payment_url(true));
            }

            /**
             * Acción que se invoca cuando el TPV Virtual realiza Callback en el pago.
             */
            function check_callback_response()
            {

                $ds_amount = null;
                $ds_currency = null;
                $ds_order = null;
                $ds_merchantcode = null;;
                $ds_terminal = null;
                $ds_response = null;
                $ds_transactiontype = null;
                $ds_date = null;
                $ds_hour = null;
                $ds_AuthorisationCode = null;
                $ds_ExpiryDate = null;
                $ds_CardNumber = null;
                $ds_Merchant_Identifier = null;
                $ds_MerchantParameters = null;
                $ds_SignatureVersion = null;
                $ds_Signature = null;
                $estadoSabadellTPV = null;


                if (!empty($_REQUEST)) {

                    // Recogemos datos de respuesta
                    if (isset($_POST) && count($_POST) > 0) {

                        if (isset($_POST['Ds_SignatureVersion']))
                            $ds_SignatureVersion = $_POST["Ds_SignatureVersion"];

                        if (isset($_POST['Ds_MerchantParameters']))
                            $ds_MerchantParameters = $_POST["Ds_MerchantParameters"];

                        if (isset($_POST['Ds_Signature']))
                            $ds_Signature = $_POST["Ds_Signature"];


                    } else if (isset($_GET) && count($_GET) > 0) {

                        if (isset($_GET['Ds_SignatureVersion']))
                            $ds_SignatureVersion = $_GET["Ds_SignatureVersion"];

                        if (isset($_GET['Ds_MerchantParameters']))
                            $ds_MerchantParameters = $_GET["Ds_MerchantParameters"];

                        if (isset($_GET['Ds_Signature']))
                            $ds_Signature = $_GET["Ds_Signature"];
                    }


                    //Si alguna variable es nula es un error.
                    if (!isset($ds_MerchantParameters) || $ds_MerchantParameters == "" || !isset($ds_SignatureVersion) || $ds_SignatureVersion == "" || !isset($ds_Signature) || $ds_Signature == "") {
                        wp_die('<img src="' . home_url() . '/wp-content/plugins/bancsabadell_woocommerce/img/BancSabadell.png" alt="Banc Sabadell" /><br>	
                                <img src="' . home_url() . '/wp-content/plugins/bancsabadell_woocommerce/img/error.gif" alt="Desactivado" title="Desactivado" />
                                <b>Banco Sabadell - TPV Virtual</b>: Fallo en el proceso de pago.<br>Su pedido ha sido cancelado.');
                    } else {


                        $redsysAPI = new RedsysAPI;
                        $decodec = $redsysAPI->decodeMerchantParameters($ds_MerchantParameters);
                        $redsysAPI->stringToArray($decodec);

                        $ds_amount = $redsysAPI->getParameter("Ds_Amount");
                        $ds_order = $redsysAPI->getParameter("Ds_Order");
                        $ds_merchantcode = $redsysAPI->getParameter("Ds_MerchantCode");
                        $ds_currency = $redsysAPI->getParameter("Ds_Currency");
                        $ds_transactiontype = $redsysAPI->getParameter("Ds_TransactionType");
                        $ds_terminal = $redsysAPI->getParameter("Ds_Terminal");
                        $ds_response = $redsysAPI->getParameter('Ds_Response');
                        $ds_AuthorisationCode = $redsysAPI->getParameter('Ds_AuthorisationCode');
                        $ds_date = $redsysAPI->getParameter('Ds_Date');
                        $ds_hour = $redsysAPI->getParameter('Ds_Hour');
                        $ds_ErrorCode = $redsysAPI->getParameter('Ds_ErrorCode');
                        $ds_ExpiryDate = $redsysAPI->getParameter('Ds_ExpiryDate');
                        $ds_CardNumber = $redsysAPI->getParameter('Ds_Card_Number');
                        $ds_Merchant_Identifier = $redsysAPI->getParameter('Ds_Merchant_Identifier');


                        if ($ds_transactiontype == SabadellTPV::PAGO_ESTANDAR ||
                            $ds_transactiontype == SabadellTPV::PREAUTORIZACION ||
                            $ds_transactiontype == SabadellTPV::PREAUTORIZACION_DIFERIDA ||
                            $ds_transactiontype == SabadellTPV::AUTENTICACION
                        ) {

                            $firma = $redsysAPI->createMerchantSignatureNotif($this->ClaveEncriptacion, $ds_MerchantParameters);

                            $pedido = substr($ds_order, 0, 8);
                            $pedido = intval($pedido);
                            $order = new WC_Order($pedido);

                            if ($firma === $ds_Signature) {

                                $ds_amount = number_format($ds_amount / 100, 4, '.', '');
                                $ds_response = intval($ds_response);

                                if ($ds_response < 101 || $ds_response == 900 ||
                                    $ds_response == 400 || $ds_response == 481 ||
                                    $ds_response == 500 || $ds_response == 9929
                                ) {

                                    //Ahora dependiendo del tipo de transaccion pondremos un estado u otro.	                   
                                    switch ($ds_transactiontype) {
                                        case SabadellTPV::PAGO_ESTANDAR:
                                            $estadoSabadellTPV = SabadellTPV::PAGO_ACEPTADO;
                                            $anotacion = __('Pago aceptado correctamente por la pasarela de pago', 'woocommerce');
                                            break;
                                        case SabadellTPV::PREAUTORIZACION:
                                        case SabadellTPV::PREAUTORIZACION_DIFERIDA:
                                            $estadoSabadellTPV = SabadellTPV::PAGO_PREAUTORIZADO;
                                            $anotacion = __('Pago preautorizado, pendiente de confirmación', 'woocommerce');
                                            break;
                                        case SabadellTPV::AUTENTICACION:
                                            $estadoSabadellTPV = SabadellTPV::AUTENTICACION_REALIZADA;
                                            $anotacion = __('Autenticación realizada, pendiente de confirmación', 'woocommerce');
                                            break;
                                    }


                                    if (isset($estadoSabadellTPV)) {

                                        //Reducimos el Stock								
                                        $order->reduce_order_stock();

                                        // Vaciamos el carrito
                                        WC()->cart->empty_cart();

                                        //Insertamos una operación en nuestra tabla.
                                        insertaOperacion($pedido, $ds_order, $ds_merchantcode, $ds_amount, $ds_terminal, $ds_currency, $this->ClaveEncriptacion, $ds_transactiontype, $estadoSabadellTPV, $pedido, $anotacion);
                                        //wp_die("[" . $pedido . "]-[" . $ds_order . "]-[" . $ds_merchantcode . "]-[" . $ds_amount . "]-[" . $ds_terminal . "]-[" . $ds_currency . "]-[" . $this->ClaveEncriptacion . "]-[" . $ds_transactiontype . "]-[" . $estadoSabadellTPV . "]-[" . $pedido . "]-[" . $anotacion ."]");                                    

                                        if ($ds_ExpiryDate != null && $ds_CardNumber != null && $ds_Merchant_Identifier != null) {
                                            nuevaTarjetaCliente($order->customer_user, $ds_CardNumber, $ds_ExpiryDate, $ds_Merchant_Identifier);
                                        }
                                    }
                                } else {
                                    if (isset($order)) {
                                        $order->update_status('cancelled', __('Pedido no realizado por la pasarela de pago. Respuesta:[' . $ds_response . ']', 'woocommerce'));
                                    }
                                    $this->log->add('BancSabadell', __('Pedido no realizado por la pasarela de pago. Respuesta:[' . $ds_response . ']'));
                                }
                            } else {
                                //Si las firmas no son iguales.
                                if (isset($order)) {
                                    $order->update_status('cancelled', __('Error en la firma', 'woocommerce'));
                                }
                                $this->log->add('BancSabadell', __('Error en la firma', 'woocommerce'));
                            }
                        }// if ($ds_transactiontype == SabadellTPV::PAGO_ESTANDAR...
                        else {

                            $id_order = intval(substr($ds_order, 0, 8));
                            $operacion = obtenerDatosOperacionSabadell($id_order);
                            $claveEncriptacion = $operacion['clave_encriptacion'];
                            $firma = $redsysAPI->createMerchantSignatureNotif($claveEncriptacion, $ds_MerchantParameters);

                            if ($firma === $ds_Signature) {

                                $ds_amount = number_format($ds_amount / 100, 4, '.', '');
                                $anotacion = "";
                                switch (strtoupper($ds_transactiontype)) {
                                    case SabadellTPV::CONFIRMACION_PREAUTORIZACION:
                                        $estadoSabadellTPV = SabadellTPV::PREAUTORIZACION_CONFIRMADA;
                                        $anotacion = __('Preautorización confirmada correctamente', 'woocommerce');
                                        break;
                                    case SabadellTPV::DEVOLUCION_PARCIAL_TOTAL:
                                        break;
                                    case SabadellTPV::ANULACION_PREAUTORIZACION:
                                        $estadoSabadellTPV = SabadellTPV::PREAUTORIZACION_ANULADA;
                                        $anotacion = __('Preautorización anulada', 'woocommerce');
                                        break;
                                    case SabadellTPV::CONFIRMACION_PREAUTORIZACION_DIFERIDA:
                                        $estadoSabadellTPV = SabadellTPV::PREAUTORIZACION_CONFIRMADA;
                                        $anotacion = __('Preautorización confirmada correctamente', 'woocommerce');
                                        break;
                                    case SabadellTPV::ANULACION_PREAUTORIZACION_DIFERIDA:
                                        $estadoSabadellTPV = SabadellTPV::PREAUTORIZACION_ANULADA;
                                        $anotacion = __('Preautorización anulada', 'woocommerce');
                                        break;
                                    case SabadellTPV::CONFIRMACION_AUTENTICACION:
                                        $estadoSabadellTPV = SabadellTPV::AUTENTICACION_CONFIRMADA;
                                        $anotacion = __('Autenticación confirmada', 'woocommerce');
                                        break;
                                }

                                if (strtoupper($ds_transactiontype) == SabadellTPV::DEVOLUCION_PARCIAL_TOTAL) {
                                    actualizaEstadoDevolucion($id_order, $ds_amount, $anotacion);
                                } else {
                                    actualizaEstadoOperacion($id_order, $estadoSabadellTPV, $anotacion);
                                }
                            }
                        }
                    }
                } else {
                    wp_die('<img src="' . home_url() . '/wp-content/plugins/bancsabadell_woocommerce/img/BancSabadell.png" alt="Banc Sabadell" /><br>	
					<img src="' . home_url() . '/wp-content/plugins/bancsabadell_woocommerce/img/error.gif" alt="Desactivado" title="Desactivado" />
					<b>Banco Sabadell - TPV Virtual</b>: Fallo en el proceso de pago.<br>Su pedido ha sido cancelado.');
                }
            }

        }

        /**
         * Add the gateway to WooCommerce
         * @param array $methods
         * @return string
         */
        function woocommerce_add_gateway_BancSabadell($methods)
        {
            $methods[] = 'WC_Gateway_BancSabadell';
            return $methods;
        }

        add_filter('woocommerce_payment_gateways', 'woocommerce_add_gateway_BancSabadell');

        /**
         *  Método que genera la página donde podremos realizar las operaciones adicionales del TPV Virtual.
         * @global type $wpdb
         * @param type $operacion
         * @param type $wooCommerce
         * @return string
         */
        function ObtenerFormularioDetalleOrden(&$operacion, &$wooCommerce)
        {
            global $wpdb;
            $urlAjax = plugins_url('php/ObtenerFirma.php', __FILE__);
            $html = '<script type="text/javascript">

                 function ClearForm() {
                    //eliminamos el resto de nodos y enviamos el formulario
                    document.getElementById("Ds_Merchant_Amount").remove();
                    document.getElementById("Ds_Merchant_Order").remove();
                    document.getElementById("Ds_Merchant_MerchantCode").remove();
                    document.getElementById("Ds_Merchant_Currency").remove();
                    document.getElementById("Ds_Merchant_TransactionType").remove();
                    document.getElementById("Ds_Merchant_Terminal").remove();
                    document.getElementById("Ds_Merchant_MerchantURL").remove();
                    document.getElementById("Ds_Merchant_UrlOK").remove();
                    document.getElementById("Ds_Merchant_UrlKO").remove();
                    document.getElementById("Ds_Merchant_Titular").remove();
                    document.getElementById("Ds_Merchant_MerchantName").remove();
                    document.getElementById("Ds_Merchant_ProductDescription").remove();
                    document.getElementById("Ds_Merchant_ConsumerLanguage").remove();
                    document.getElementById("Ds_Merchant_MerchantData").remove();
                    document.getElementById("Ds_Merchant_Identifier").remove();
                }

                 function ObtenerFirma(hashFirma){
                        var xmlhttp;
                        var ds_merchant_amount 			= document.getElementById("Ds_Merchant_Amount").value;
                        var ds_merchant_order 			= document.getElementById("Ds_Merchant_Order").value;
                        var ds_merchant_merchantcode 	= document.getElementById("Ds_Merchant_MerchantCode").value;
                        var ds_merchant_currency		= document.getElementById("Ds_Merchant_Currency").value;
                        var ds_merchant_transactiontype = document.getElementById("Ds_Merchant_TransactionType").value;
                        var ds_merchant_terminal 		= document.getElementById("Ds_Merchant_Terminal").value;
                        var ds_merchant_merchanturl 	= document.getElementById("Ds_Merchant_MerchantURL").value;
                        var ds_merchant_urlok 			= document.getElementById("Ds_Merchant_UrlOK").value;
                        var ds_merchant_urlko 			= document.getElementById("Ds_Merchant_UrlKO").value;

                        var datos = "DS_MERCHANT_AMOUNT="  + ds_merchant_amount;
                        datos += "&DS_MERCHANT_ORDER=" + ds_merchant_order;
                        datos += "&DS_MERCHANT_MERCHANTCODE=" +ds_merchant_merchantcode;
                        datos += "&DS_MERCHANT_CURRENCY=" +ds_merchant_currency;
                        datos += "&DS_MERCHANT_TRANSACTIONTYPE=" +ds_merchant_transactiontype;
                        datos += "&DS_MERCHANT_TERMINAL=" + ds_merchant_terminal;
                        datos += ds_merchant_merchanturl ? "&DS_MERCHANT_MERCHANTURL=" +ds_merchant_merchanturl : "";
                        datos += ds_merchant_urlok ? "&DS_MERCHANT_URLOK=" + ds_merchant_urlok : "";
                        datos += ds_merchant_urlko ? "&DS_MERCHANT_URLKO=" + ds_merchant_urlko : "";
                        datos += "&HASH=" + hashFirma ;


                        xmlhttp = new XMLHttpRequest();
                        xmlhttp.open("POST", "' . $urlAjax . '", true);
                        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                        xmlhttp.send(datos);


                        xmlhttp.onreadystatechange = function() {
                            if (xmlhttp.readyState == 4) {
                                var respuesta = xmlhttp.responseText;
                                var jsonRespuesta = JSON.parse(respuesta);
                                document.getElementById("Ds_Signature").value = jsonRespuesta["Ds_Signature"];
                                document.getElementById("Ds_MerchantParameters").value = jsonRespuesta["Ds_MerchantParameters"];
                                ClearForm();

                                var divDevolucion = jQuery("div#SabadellDevolucionForm");
                                jQuery("<form>")
                                        .attr({
                                                action: divDevolucion.attr("data-action"),
                                                method: divDevolucion.attr("data-method")
                                        })
                                        .html(divDevolucion.html())
                                        .appendTo(jQuery("body"))
                                        .submit()
                                        .remove();
                            }
                        }

                 }


				function isNumeric(n) {
                    return !isNaN(parseFloat(n)) && isFinite(n);
				}

				function ValidaDevolucion(importe, importeDevuelto, tipoTransaccion, hashFirma) {

                    importeADevolver = document.getElementById("devolverInput").value;

                    if (!isNumeric(importeADevolver)) {
                        alert("' . __('El importe a devolver debe de ser un valor numérico', 'woocommerce') . '");
                        return false;
                    }

                    if (importeADevolver <= 0) {
                        alert("' . __('El importe a devolver debe de ser mayor a 0', 'woocommerce') . '");
                        return false;
                    }

                    if (importe == importeDevuelto) {
                        alert("' . __('No se puede devolver un importe mayor al cobrado', 'woocommerce') . '");
                        return false;
                    }
                    var importeAcomparar = parseFloat(importeDevuelto) + parseFloat(importeADevolver);
                    if (importeAcomparar > importe) {
                        alert("' . __('No se puede devolver un importe mayor al cobrado', 'woocommerce') . '");
                        return false;
                    }


                    //El importe a devolver pasa sin transformar, asi que lo transformamos.

                    var originalFloat = parseFloat(importeADevolver);
                    var importeDevolverRedondeado = Math.round(originalFloat * 100) / 100;
                    importeDevolverRedondeado = importeDevolverRedondeado.toFixed(2);

                    var importeSinPuntos = importeDevolverRedondeado.toString().replace(".", "");
                    importeSinPuntos = parseInt(importeSinPuntos.toString().replace(",", ""));
                    var importeDevueltoFinal = parseInt(importeDevuelto) + parseInt(importeSinPuntos);

                    var importeOriginalSinPuntos = importe.toString().replace(".", "");

                    if (importeOriginalSinPuntos < importeDevueltoFinal) {
                        alert("' . __('No se puede devolver un importe mayor al cobrado', 'woocommerce') . '");
                        return false;
                    }

                    if (confirm("' . __('¿Desea reembolsar el importe indicado?', 'woocommerce') . '")) {
                        document.getElementById("Ds_Merchant_Amount").value = importeSinPuntos;
                        ObtenerFirma(hashFirma);


                    }
				}


				function confirmarPreautorizacion() {
                    if (confirm("' . __('¿Desea confirmar la preautorización?', 'woocommerce') . '")) {
                        ClearForm();
                        var divDevolucion = jQuery("div#SabadellConfirmacionPreautorizacion");
                        jQuery("<form>")
                                .attr({
                                        action: divDevolucion.attr("data-action"),
                                        method: divDevolucion.attr("data-method")
                                })
                                .html(divDevolucion.html())
                                .appendTo(jQuery("body"))
                                .submit()
                                .remove();
                    }
				}

				function anulacionPreautorizacion() {
                    if (confirm("' . __('¿Desea anular la preautorización?', 'woocommerce') . '")) {
                        ClearForm();
                        var divDevolucion = jQuery("div#SabadellAnulacionPreautorizacion");
                        jQuery("<form>")
                                .attr({
                                        action: divDevolucion.attr("data-action"),
                                        method: divDevolucion.attr("data-method")
                                })
                                .html(divDevolucion.html())
                                .appendTo(jQuery("body"))
                                .submit()
                                .remove();
                    }
				}

				function confirmarAutenticacion() {
                    if (confirm("' . __('¿Desea confirmar la preautorización?', 'woocommerce') . '")) {
                        ClearForm();
                        var divDevolucion = jQuery("div#SabadellConfirmacionForm");
                        jQuery("<form>")
                                .attr({
                                        action: divDevolucion.attr("data-action"),
                                        method: divDevolucion.attr("data-method")
                                })
                                .html(divDevolucion.html())
                                .appendTo(jQuery("body"))
                                .submit()
                                .remove();
                    }
				}


			</script>

			<div>
			<h2> <img src="../wp-content/plugins/bancsabadell_woocommerce/img/BancSabadell.png" alt="Banco Sabadell" />&nbsp;' . __('Detalle Orden', 'woocommerce') . '</h2><br>
			<table border="0" width="100%" cellspacing="0" cellpadding="2">	 
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="2">
                                        <tr>
                                            <td colspan="3"><?php echo tep_draw_separator(); ?></td>
                                        </tr>
                                        <tr>
                                            <td valign="top">';


            $datosOperacion = obtenerDatosOperacionSabadell($operacion->id);

            if ($datosOperacion == null || !(count($datosOperacion) > 0)) {
                $html .= '<center><span style = "color:red">' . __('Error: El pedido no existe.', 'woocommerce') . '</span></center>';
            } else {
                $devolucionActiva = false;
                $botonesConfirmacion = null;
                $clave_encriptacion = null;
                $importe_cobrado = null;
                $importe_devuelto = null;
                $tipoTransaccionRealizar = null;
                $id_estado = null;

                $idiomaISO = substr(get_locale(), 0, 2);
                $EstadosTPV = SabadellTPV::obtenerIdiomaEstado($idiomaISO);


                $html .= generarFormularioBackOffice($datosOperacion, $devolucionActiva, $botonesConfirmacion, $clave_encriptacion, $importe_cobrado, $importe_devuelto, $tipoTransaccionRealizar, $id_estado, $wooCommerce);

                $html .= ' <table class="form">
                        <tr>
                            <td><strong>' . __('ID de pedido', 'woocommerce') . ':</strong></td>
                            <td>' . $operacion->id . '</td>
                        </tr>	

                        <tr>
                            <td><strong>' . __('Id Sabadell', 'woocommerce') . ':</strong></td>
                            <td>' . $datosOperacion['merchant_order'] . '</td>
                        </tr>	

                        <tr>
                            <td><strong>' . __('Estado', 'woocommerce') . ':</strong></td>
                            <td>' . $EstadosTPV[$datosOperacion['id_estado']] . '</td>
                        </tr>		  

                        <tr>
                            <td><strong>' . __('Importe total', 'woocommerce') . ':</strong></td>
                            <td>' . $importe_cobrado . '</td>
                        </tr>		  

                        <tr>
                            <td><strong>' . __('Importe devuelto', 'woocommerce') . ':</strong></td>
                            <td>' . $importe_devuelto . '</td>
                        </tr>';


                if ($devolucionActiva) {
                    $html .= '<tr>
                                <td><strong><span>' . __('Importe a devolver', 'woocommerce') . ':</span></strong></td>
                                <td><input name="devolverInput" id="devolverInput" value="0"/></td>					
                            </tr>					
                            <tr>
                                <td></td>
                                <td><button onClick="javascript:ValidaDevolucion(\'' . $importe_cobrado . '\', \'' . $importe_devuelto . '\', \'' . $tipoTransaccionRealizar . '\',\'' . EncriptarClaveEncriptacion($clave_encriptacion, $datosOperacion['merchant_order']) . '\')" class="scalable save" type="button"><span>' . __('Devolver importe', 'woocommerce') . '</span></button></td>
                            </tr>';
                } else {

                    $html .= '<tr><td></td>';

                    if (strlen($botonesConfirmacion) > 0) {
                        $html .= '<td>' . $botonesConfirmacion . '</td>';
                    }

                    $html .= '</tr>';
                }

                $html .= '</table>';
            }

            $html .= '</td></tr><tr><td></td></tr></table></td></tr></table>';
            $html .= "</div>";
            return $html;
        }

        /**
         * Genera el formulario para realizar las devoluciones, confirmaciones etc.
         * @param type $datosOperacion
         * @param type $devolucionActiva
         * @param type $botonesConfirmacion
         * @param type $clave_encriptacion
         * @param type $importe_cobrado
         * @param type $importe_devuelto
         * @param type $tipoTransaccionRealizar
         * @param type $id_estado
         * @return type
         */
        function generarFormularioBackOffice($datosOperacion, &$devolucionActiva, &$botonesConfirmacion, &$clave_encriptacion, &$importe_cobrado, &$importe_devuelto, &$tipoTransaccionRealizar, &$id_estado, &$wooCommerce)
        {

            $formularioTPV = null;

            if ($datosOperacion != null && count($datosOperacion) > 0) {

                $merchant_order = $datosOperacion['merchant_order'];
                $merchant_code = $datosOperacion['merchant_code'];
                $importe_cobrado = $datosOperacion['importe_cobrado'];
                $importe_devuelto = $datosOperacion['importe_devuelto'];
                $terminal = $datosOperacion['id_terminal'];
                $moneda = $datosOperacion['moneda'];
                $tipoPago = $datosOperacion['tipo_pago'];
                $id_estado = $datosOperacion['id_estado'];
                $clave_encriptacion = $datosOperacion['clave_encriptacion'];
                $nomComercio = $wooCommerce->NombreComercio;
                $urlPost = "";

                if ($wooCommerce->EntornoActivo == "TEST")
                    $urlPost = $wooCommerce->URLTest;
                else
                    $urlPost = $wooCommerce->URLReal;

                $urlOk = urlencode(admin_url() . "post.php?post=" . $datosOperacion['id_order'] . "&action=edit&OK=S");
                $urlKO = urlencode(admin_url() . "post.php?post=" . $datosOperacion['id_order'] . "&action=edit&OK=N");
                $urlRespuestaTPV = urlencode($wooCommerce->UrlCallBack);


                $idioma_tpv = $wooCommerce->Idioma;
                $idiomaISO = substr(get_locale(), 0, 2);
                if ($idioma_tpv == "0") {
                    $idioma = SabadellTPV::isoToTPV($idiomaISO);
                } else {
                    $idioma = $idioma_tpv;
                }


                $importeSinDecimales = $importe_cobrado * 100;
                $tipoTransaccionRealizar = -1;


                $redsysAPI = new RedsysAPI;
                $redsysAPI->setParameter("DS_MERCHANT_AMOUNT", $importeSinDecimales);
                $redsysAPI->setParameter("DS_MERCHANT_ORDER", $merchant_order);
                $redsysAPI->setParameter("DS_MERCHANT_MERCHANTCODE", $merchant_code);
                $redsysAPI->setParameter("DS_MERCHANT_CURRENCY", $moneda);
                $redsysAPI->setParameter("DS_MERCHANT_TERMINAL", $terminal);
                $redsysAPI->setParameter("DS_MERCHANT_MERCHANTURL", $urlRespuestaTPV);
                $redsysAPI->setParameter("DS_MERCHANT_URLOK", $urlOk);
                $redsysAPI->setParameter("DS_MERCHANT_URLKO", $urlKO);
                $redsysAPI->setParameter("DS_MERCHANT_MERCHANTNAME", $nomComercio);
                $redsysAPI->setParameter("DS_MERCHANT_CONSUMERLANGUAGE", $idioma);

                //Dependiendo del tipo de pago podemos realizar unas u otras operaciones.
                switch ($tipoPago) {
                    case SabadellTPV::PAGO_ESTANDAR:
                        if (SabadellTPV::PAGO_ACEPTADO == (int)$id_estado || SabadellTPV::DEVOLUCION_PARCIAL == (int)$id_estado) {
                            $tipoTransaccionRealizar = SabadellTPV::DEVOLUCION_PARCIAL_TOTAL;
                        }
                        break;
                    case SabadellTPV::PREAUTORIZACION:
                        if (SabadellTPV::PAGO_PREAUTORIZADO == (int)$id_estado) {
                            $tipoTransaccionRealizar = SabadellTPV::CONFIRMACION_PREAUTORIZACION;
                            $redsysAPI->setParameter("DS_MERCHANT_TRANSACTIONTYPE", $tipoTransaccionRealizar);
                            $DsMerchantParameters = $redsysAPI->createMerchantParameters();
                            $DsSignature = $redsysAPI->createMerchantSignature($clave_encriptacion);
                            $formularioTPV = SabadellTPV::obtenerFormularioTPV_WordPress($urlPost, $importeSinDecimales, $moneda, $merchant_order, $merchant_code, $terminal, $tipoTransaccionRealizar, null, $nomComercio, $urlRespuestaTPV, null, $urlOk, $urlKO, $idioma, 'SabadellConfirmacionPreautorizacion', null,$DsMerchantParameters,$DsSignature);

                            $tipoTransaccionRealizar = SabadellTPV::ANULACION_PREAUTORIZACION;
                            $redsysAPI->setParameter("DS_MERCHANT_TRANSACTIONTYPE", $tipoTransaccionRealizar);
                            $DsMerchantParameters2 = $redsysAPI->createMerchantParameters();
                            $DsSignature2 = $redsysAPI->createMerchantSignature($clave_encriptacion);
                            $formularioTPV .= SabadellTPV::obtenerFormularioTPV_WordPress($urlPost, $importeSinDecimales, $moneda, $merchant_order, $merchant_code, $terminal, $tipoTransaccionRealizar, null, $nomComercio, $urlRespuestaTPV, null, $urlOk, $urlKO, $idioma, 'SabadellAnulacionPreautorizacion', null, $DsMerchantParameters2, $DsSignature2);

                            $botonesConfirmacion = '<input class="button" name="ConfirmarBtn" value="' . __('Confirmar preautorización', 'woocommerce') . '" type="button" onClick="confirmarPreautorizacion()"/> ';
                            $botonesConfirmacion .= '<input class="button" name="CancelarBtn"  value="' . __('Cancelar preautorización', 'woocommerce') . '" type="button" onClick="anulacionPreautorizacion()"/> ';
                        } else if (SabadellTPV::PREAUTORIZACION_CONFIRMADA == (int)$id_estado || SabadellTPV::DEVOLUCION_PARCIAL == (int)$id_estado) {
                            $tipoTransaccionRealizar = SabadellTPV::DEVOLUCION_PARCIAL_TOTAL;
                        }

                        break;
                    case SabadellTPV::PREAUTORIZACION_DIFERIDA:
                        if (SabadellTPV::PAGO_PREAUTORIZADO == (int)$id_estado) {
                            $tipoTransaccionRealizar = SabadellTPV::CONFIRMACION_PREAUTORIZACION_DIFERIDA;
                            $redsysAPI->setParameter("DS_MERCHANT_TRANSACTIONTYPE", $tipoTransaccionRealizar);
                            $DsMerchantParameters = $redsysAPI->createMerchantParameters();
                            $DsSignature = $redsysAPI->createMerchantSignature($clave_encriptacion);
                            $formularioTPV = SabadellTPV::obtenerFormularioTPV_WordPress($urlPost, $importeSinDecimales, $moneda, $merchant_order, $merchant_code, $terminal, $tipoTransaccionRealizar, null, $nomComercio, $urlRespuestaTPV, null, $urlOk, $urlKO,  $idioma, 'SabadellConfirmacionPreautorizacion', null, $DsMerchantParameters, $DsSignature);

                            $tipoTransaccionRealizar = SabadellTPV::ANULACION_PREAUTORIZACION_DIFERIDA;
                            $redsysAPI->setParameter("DS_MERCHANT_TRANSACTIONTYPE", $tipoTransaccionRealizar);
                            $DsMerchantParameters2 = $redsysAPI->createMerchantParameters();
                            $DsSignature2 = $redsysAPI->createMerchantSignature($clave_encriptacion);
                            $formularioTPV .= SabadellTPV::obtenerFormularioTPV_WordPress($urlPost, $importeSinDecimales, $moneda, $merchant_order, $merchant_code, $terminal, $tipoTransaccionRealizar, null, $nomComercio, $urlRespuestaTPV, null, $urlOk, $urlKO,  $idioma, 'SabadellAnulacionPreautorizacion', null, $DsMerchantParameters2, $DsSignature2);

                            $botonesConfirmacion = '<input class="button" name="ConfirmarBtn"  value="' . __('Confirmar preautorización diferida', 'woocommerce') . '" type="button" onClick="confirmarPreautorizacion()"/> ';
                            $botonesConfirmacion .= '<input class="button" name="CancelarBtn"  value="' . __('Cancelar preautorización diferida', 'woocommerce') . '" type="button"  onClick="anulacionPreautorizacion()"/> ';
                        } else if (SabadellTPV::PREAUTORIZACION_CONFIRMADA == (int)$id_estado || SabadellTPV::DEVOLUCION_PARCIAL == (int)$id_estado) {
                            $tipoTransaccionRealizar = SabadellTPV::DEVOLUCION_PARCIAL_TOTAL;
                        }
                        break;
                    case SabadellTPV::AUTENTICACION:
                        if (SabadellTPV::AUTENTICACION_REALIZADA == (int)$id_estado) {
                            $tipoTransaccionRealizar = SabadellTPV::CONFIRMACION_AUTENTICACION;
                            $redsysAPI->setParameter("DS_MERCHANT_TRANSACTIONTYPE", $tipoTransaccionRealizar);
                            $DsMerchantParameters = $redsysAPI->createMerchantParameters();
                            $DsSignature = $redsysAPI->createMerchantSignature($clave_encriptacion);
                            $formularioTPV = SabadellTPV::obtenerFormularioTPV_WordPress($urlPost, $importeSinDecimales, $moneda, $merchant_order, $merchant_code, $terminal, SabadellTPV::CONFIRMACION_AUTENTICACION, null, $nomComercio, $urlRespuestaTPV, null, $urlOk, $urlKO, $idioma, 'SabadellConfirmacionForm', null, $DsMerchantParameters, $DsSignature);
                            $botonesConfirmacion = '<input class="button" name="ConfirmacionAuth"  value="' . __('Confirmar autenticación', 'woocommerce') . '" type="button" onClick="confirmarAutenticacion()"/> ';
                        } else if (SabadellTPV::AUTENTICACION_CONFIRMADA == (int)$id_estado || SabadellTPV::DEVOLUCION_PARCIAL == (int)$id_estado) {
                            $tipoTransaccionRealizar = SabadellTPV::DEVOLUCION_PARCIAL_TOTAL;
                        }
                        break;
                }

                if($tipoTransaccionRealizar === SabadellTPV::DEVOLUCION_PARCIAL_TOTAL) {
                    $formularioTPV = SabadellTPV::obtenerFormularioTPV_WordPress($urlPost, '', $moneda, trim($merchant_order), $merchant_code, $terminal, $tipoTransaccionRealizar, '', $nomComercio, $urlRespuestaTPV, '', $urlOk, $urlKO, $idioma, 'SabadellDevolucionForm', '','','');
                    $devolucionActiva = true;
                }
            }

            return $formularioTPV;
        }

    }


    function EncriptarClaveEncriptacion($kc, $ds_merchant_order)
    {
        $miObj = new RedsysAPI;
        //Encriptamos la clave de encriptacion y la pasamos a Base64;
        $clave = substr($ds_merchant_order, 0, 5);
        $kcEncriptado = $miObj->encrypt_3DES($kc, $clave);
        return $miObj->base64_url_encode($kcEncriptado);

    }

}
