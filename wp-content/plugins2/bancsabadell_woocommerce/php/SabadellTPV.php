<?php

/*
  Banc Sabadell Payment Module
  Copyright (c) 2015 Banco Sabadell
  http://www.bancsabadell.com
 */

// Clase en la que pretendemos sacar toda la funcionalidad para el TPV
// para poder usar lo máximo posible en otros entornos.
class SabadellTPV {

    const PAGO_ESTANDAR = '0';
    const PREAUTORIZACION = '1';
    const CONFIRMACION_PREAUTORIZACION = '2';
    const DEVOLUCION_PARCIAL_TOTAL = '3';
    const AUTENTICACION = '7';
    const CONFIRMACION_AUTENTICACION = '8';
    const ANULACION_PREAUTORIZACION = '9';
    const PREAUTORIZACION_DIFERIDA = 'O';
    const CONFIRMACION_PREAUTORIZACION_DIFERIDA = 'P';
    const ANULACION_PREAUTORIZACION_DIFERIDA = 'Q';
    const VERSION_CIFRADO = "HMAC_SHA256_V1";


    static private $urlReal = "https://sis.redsys.es/sis/realizarPago";
    static private $urlTest = "https://sis-t.redsys.es:25443/sis/realizarPago";
    //Los tipos de pago autorizados.
    static private $tiposPagoES = array("Pago estándar o Autorización" => "0", "Preautorización" => "1", "Preautorización Diferida" => "O", "Autenticación" => "7");
    static private $tiposPagoEN = array("Payments or Authorization" => "0", "Preauthorization" => "1", "Deferred Preautorización" => "O", "Authentication" => "7");
    static private $tiposPagoCAT = array("Pagament estàndard o Autorització" => "0", "Preautorització" => "1", "Preautorització Diferida" => "O", "Autenticació" => "7");
    //Monedas disponibles.
    static private $monedasES = array("Euro" => "978", "Dólar" => "840", "Libra" => "826", "Yen" => "392", "Peso Argentino" => "032", "Dólar Canadiense" => "124", "Peso Chileno" => "152", "Peso Colombiano" => "170", "Rupia India" => "356", "Nuevo Peso Mexicano" => "484", "Nuevos Soles" => "604", "Franco Suizo" => "756", "Real Brasileño" => "986", "Bolívar Venezolano" => "937", "Lira Turca" => "949", "Yuan Chino Extracontinental" => "156", "Corona Noruega" => "578", "Corona Suiza" => "752", "Rand Sudafricano" => "710", "Corona Danesa" => "208", "Dólar Australiano" => "36");
    static private $monedasEN = array("Euro" => "978", "Dollar" => "840", "Pound" => "826", "Yen" => "392", "Peso Argentino" => "032", "Canadian Dollar" => "124", "Chilean Peso" => "152", "Colombian Peso" => "170", "India Rupee" => "356", "New Mexican Peso" => "484", "New Suns" => "604", "Swiss Franc" => "756", "Brazilian Real" => "986", "Venezuelan Bolivar" => "937", "Lira Turca" => "949", "Chinese Yuan Extracontinental" => "156", "Norwegian Krone" => "578", "Swiss Corona" => "752", "South African Rand" => "710", "Danish Krone" => "208", "Australian Dollar" => "36");
    static private $monedasCAT = array("Euro" => "978", "Dòlar" => "840", "Lliura" => "826", "Yen" => "392", "Pes Argentí" => "032", "Dòlar Canadenc" => "124", "Pes Xilè" => "152", "Pes Colombià" => "170", "Rupia Índia" => "356", "Nou Pes Mexicà" => "484", "Nous Sols" => "604", "Franc Suís" => "756", "Reial Brasiler" => "986", "Bolívar veneçolà" => "937", "Lira Turca" => "949", "Yuan Xinès Extracontinental" => "156", "Corona Noruega" => "578", "Corona Suïssa" => "752", "Rand Sud-africà" => "710", "Corona Danesa" => "208", " Dòlar Australià " => "36");
    //Idiomas del TPV
    static private $idiomasES = array("Castellano" => "1", "Inglés" => "2", "Catalán" => "3", "Francés" => "4", "Aleman" => "5", "Holandes" => "6", "Italiano" => "7", "Sueco" => "8", "Portugués" => "9", "Valenciano" => "10", "Polaco" => "11", "Gallego" => "12", "Euskera" => "13");
    static private $idiomasEN = array("Spanish" => "1", "English" => "2", "Catalan" => "3", "French" => "4", "German" => "5", "Netherlander" => "6", "Italian" => "7", "Swedish" => "8", "Portuguese" => "9", "Valencian" => "10", "Polish" => "11", "Galician" => "12", "Euskera" => "13");
    static private $idiomasCAT = array("Castellà" => "1", "Anglès" => "2", "Català" => "3", "Francès" => "4", "Alemà" => "5", "Holandes" => "6", "Italià" => "7", "Suec" => "8", "Portuguès" => "9", "Valencià" => "10", "Polonès" => "11", "Gallego" => "12", "Euskera" => "13");
    static private $idiomaFormTPVES = array("Conectar con la pasarela de pago", "Pago con tarjeta a través del TPV Virtual de Banco Sabadell.", "Este método de pago lleva asociado un recargo de", "No hay un terminal configurado para la moneda en uso.");
    static private $idiomaFormTPVEN = array("Connect with payment gateway", "Credit card payments through Banco Sabadell TPV.", "This payment method is associated with a charge of", "No terminal available for the currency in use.");
    static private $idiomaFormTPVCAT = array("Connectar amb la passarel·la de pagament", "Pagament amb targeta a través del TPV Virtual de Banc Sabadell.", "Aquest mètode de pagament porta associat un recàrrec de", "No hi ha un terminal configurat per a la moneda en ús.");
    static private $idiomaFormPagoOKES = array("Pago OK", "El pago ha sido realizado correctamente.", "Recuerde que puede conctactar con nosotros cuando quiera si tiene alguna duda sobre su pedido.", "Ver detalle de la orden.");
    static private $idiomaFormPagoOKEN = array("Payment OK", "The payment has been successfully accomplished.", "Remember that you can contact us at any time if you have any doubt about your order", "Click here to check your order details");
    static private $idiomaFormPagoOKCAT = array("Pagament OK", "El pagament ha estat realitzat correctament.", "Recordi que vostè pot contactar amb nosaltres en qualsevol moment si vostè té algun dubte sobre la seva comanda", "Fes clic aquí per veure els detalls de l'ordre");
    static private $idiomaFormPagoKOES = array("El pago con su tarjeta de crédito no se ha podido completar.", "Lo sentimos, pero el pago no se ha realizado con éxito. Usted puede intentar otra vez o elegir otro método de pago. Recuerde que sólo puede utilizar las tarjetas de crédito Visa y Mastercard, y tarjetas de débito Maestro y (sólo España)", "Hay varias razones para que esto suceda:", "Se confundió al introducir los numeros de la tarjeta de crédito. Asegúrese de introducirlos correctamente.", "Asegúrese de que su tarjeta de crédito no ha expirado y es válida. Las tarjetas de débito Maestro, por ejemplo, sólo son válidos en España", "Ha habido un problema con nuestro proveedor de la pasarela de pago.", "En cualquier caso, puede ponerse en contacto con nosotros por correo o por teléfono y nos pondremos en intentar arreglar el problema juntos.");
    static private $idiomaFormPagoKOEN = array("Your credit card payment could not be accomplished", "We are sorry, but your payment has not been successfully accomplished. You can try again or choose another payment method. Remember that you can only use Visa and Mastercard credit cards, and Maestro debit cards as well (Spain only)", "There are several reasons for this to happen:", "You mistook any of the digits of your credit card. Make sure you introduce them well.", "Make sure your credit card has not expired and is valid. Maestro debit cards, for example, are only valid in Spain", "There has been a problem with our payment gateway provider.", "In any case, you can contact us by mail or by phone and we will try to fix your problem together.");
    static private $idiomaFormPagoKOCAT = array("El pagament amb la targeta de crèdit no s'ha pogut completar.", "Ho sentim, però el pagament no s'ha realitzat amb èxit. Vostè pot intentar una altra vegada o triar un altre mètode de pagament. Recordeu que només podeu utilitzar les targetes de crèdit Visa i Mastercard, i targetes de dèbit Mestre i (només Espanya)", "Hi ha diverses raons perquè això succeeixi:", "Es va confondre a introduir els números de la targeta de crèdit. Assegureu-vos de introduir correctament.", "Assegureu-vos que la targeta de crèdit no ha expirat i és vàlid. Targetes de dèbit Mestre, per exemple, només són vàlids a Espanya", "Hi ha hagut un problema amb el nostre proveïdor de la passarel-la de pagament.", "En qualsevol cas, es pot posar en contacte amb nosaltres per correu o per telèfon i ens posarem en intentar arreglar el problema junts.");
    static private $idiomaISO = array("es" => "1", "en" => "2", "gb" => "2", "ca" => "3", "fr" => "4", "de" => "5", "nl" => "6", "it" => "7", "sv" => "8", "pt" => "9", "pl" => "11", "gl" => "12", "eu" => "13");
    static private $idiomaEstadoES = array('Pago Aceptado', 'Devolución completa', 'Devolución parcial', 'Pago preautorizado', 'Preautorización confirmada', 'Preautorización anulada', 'Autenticación realizada', 'Autenticación confirmada');
    static private $idiomaEstadoEN = array('Accepted Payment', 'Full Return', 'Partial refund', 'pre-authorized payment', 'Preauthorization confirmed', 'Preauthorization void', 'Authentication performed', 'Authentication confirmed');
    static private $idiomaEstadoCAT = array('Pago Acceptat', 'Devolució completa', 'Devolució parcial', 'Pago preautoritzada', 'Preautorització confirmada', 'Preautorització anul-lada', 'Autenticació realitzada', 'Autenticació confirmada');
    static private $colorEstado = array('#4BC432', '#F72019', '#F77D19', '#F77D19', '#4BC432', '#F72019', '#F77D19', '#4BC432');

    const PAGO_ACEPTADO = 0;
    const DEVOLUCION_COMPLETA = 1;
    const DEVOLUCION_PARCIAL = 2;
    const PAGO_PREAUTORIZADO = 3;
    const PREAUTORIZACION_CONFIRMADA = 4;
    const PREAUTORIZACION_ANULADA = 5;
    const AUTENTICACION_REALIZADA = 6;
    const AUTENTICACION_CONFIRMADA = 7;

    static private $monedasISO = array(
        'ADP' => '020', 'AED' => '784', 'AFA' => '004', 'ALL' => '008',
        'AMD' => '051', 'ANG' => '532', 'AOA' => '973', 'ARS' => '032',
        'AUD' => '036', 'AWG' => '533', 'AZM' => '031', 'BAM' => '977',
        'BBD' => '052', 'BDT' => '050', 'BGL' => '100', 'BGN' => '975',
        'BHD' => '048', 'BIF' => '108', 'BMD' => '060', 'BND' => '096',
        'BOB' => '068', 'BOV' => '984', 'BRL' => '986', 'BSD' => '044',
        'BTN' => '064', 'BWP' => '072', 'BYR' => '974', 'BZD' => '084',
        'CAD' => '124', 'CDF' => '976', 'CHF' => '756', 'CLF' => '990',
        'CLP' => '152', 'CNY' => '156', 'COP' => '170', 'CRC' => '188',
        'CUP' => '192', 'CVE' => '132', 'CYP' => '196', 'CZK' => '203',
        'DJF' => '262', 'DKK' => '208', 'DOP' => '214', 'DZD' => '012',
        'ECS' => '218', 'ECV' => '983', 'EEK' => '233', 'EGP' => '818',
        'ERN' => '232', 'ETB' => '230', 'EUR' => '978', 'FJD' => '242',
        'FKP' => '238', 'GBP' => '826', 'GEL' => '981', 'GHC' => '288',
        'GIP' => '292', 'GMD' => '270', 'GNF' => '324', 'GTQ' => '320',
        'GWP' => '624', 'GYD' => '328', 'HKD' => '344', 'HNL' => '340',
        'HRK' => '191', 'HTG' => '332', 'HUF' => '348', 'IDR' => '360',
        'ILS' => '376', 'INR' => '356', 'IQD' => '368', 'IRR' => '364',
        'ISK' => '352', 'JMD' => '388', 'JOD' => '400', 'JPY' => '392',
        'KES' => '404', 'KGS' => '417', 'KHR' => '116', 'KMF' => '174',
        'KPW' => '408', 'KRW' => '410', 'KWD' => '414', 'KYD' => '136',
        'KZT' => '398', 'LAK' => '418', 'LBP' => '422', 'LKR' => '144',
        'LRD' => '430', 'LSL' => '426', 'LTL' => '440', 'LVL' => '428',
        'LYD' => '434', 'MAD' => '504', 'MDL' => '498', 'MGF' => '450',
        'MKD' => '807', 'MMK' => '104', 'MNT' => '496', 'MOP' => '446',
        'MRO' => '478', 'MTL' => '470', 'MUR' => '480', 'MVR' => '462',
        'MWK' => '454', 'MXN' => '484', 'MXV' => '979', 'MYR' => '458',
        'MZM' => '508', 'NAD' => '516', 'NGN' => '566', 'NIO' => '558',
        'NOK' => '578', 'NPR' => '524', 'NZD' => '554', 'OMR' => '512',
        'PAB' => '590', 'PEN' => '604', 'PGK' => '598', 'PHP' => '608',
        'PKR' => '586', 'PLN' => '985', 'PYG' => '600', 'QAR' => '634',
        'ROL' => '642', 'RUB' => '643', 'RUR' => '810', 'RWF' => '646',
        'SAR' => '682', 'SBD' => '090', 'SCR' => '690', 'SDD' => '736',
        'SEK' => '752', 'SGD' => '702', 'SHP' => '654', 'SIT' => '705',
        'SKK' => '703', 'SLL' => '694', 'SOS' => '706', 'SRG' => '740',
        'STD' => '678', 'SVC' => '222', 'SYP' => '760', 'SZL' => '748',
        'THB' => '764', 'TJS' => '972', 'TMM' => '795', 'TND' => '788',
        'TOP' => '776', 'TPE' => '626', 'TRL' => '792', 'TRY' => '949',
        'TTD' => '780', 'TWD' => '901', 'TZS' => '834', 'UAH' => '980',
        'UGX' => '800', 'USD' => '840', 'UYU' => '858', 'UZS' => '860',
        'VEB' => '862', 'VND' => '704', 'VUV' => '548', 'XAF' => '950',
        'XCD' => '951', 'XOF' => '952', 'XPF' => '953', 'YER' => '886',
        'YUM' => '891', 'ZAR' => '710', 'ZMK' => '894', 'ZWD' => '716',
    );

    static function urlReal() {
        return self::$urlReal;
    }

    static function urlTest() {
        return self::$urlTest;
    }

    static function colorEstado() {
        return self::$colorEstado;
    }

    //En función del código ISO devolvemos los idiomas.
    static function obtenerIdiomaEstado($idiomaWeb) {

        $idiomaEstado = null;

        switch ($idiomaWeb) {
            case 'es':
                $idiomaEstado = self::$idiomaEstadoES;
                break;
            case 'en':
                $idiomaEstado = self::$idiomaEstadoEN;
                break;
            case 'gb':
                $idiomaEstado = self::$idiomaEstadoEN;
                break;
            case 'ca':
                $idiomaEstado = self::$idiomaEstadoCAT;
                break;
            default:
                $idiomaEstado = self::$idiomaEstadoES;
        }
        return $idiomaEstado;
    }

    //En función del código ISO devolvemos los idiomas.
    static function monedas($idiomaWeb) {

        $monedas = null;

        switch ($idiomaWeb) {
            case 'es':
                $monedas = self::$monedasES;
                break;
            case 'en':
                $monedas = self::$monedasEN;
                break;
            case 'gb':
                $monedas = self::$monedasEN;
                break;
            case 'ca':
                $monedas = self::$monedasCAT;
                break;
            default:
                $monedas = self::$monedasES;
        }

        return $monedas;
    }

    //En función del código ISO devolvemos los idiomas.
    static function tiposTransaccion($idiomaWeb) {

        $tiposTransaccion = null;

        switch ($idiomaWeb) {
            case 'es':
                $tiposTransaccion = self::$tiposPagoES;
                break;
            case 'en':
                $tiposTransaccion = self::$tiposPagoEN;
                break;
            case 'gb':
                $tiposTransaccion = self::$tiposPagoEN;
                break;
            case 'ca':
                $tiposTransaccion = self::$tiposPagoCAT;
                break;
            default:
                $tiposTransaccion = self::$tiposPagoES;
        }

        return $tiposTransaccion;
    }

    //En función del código ISO devolvemos los idiomas.
    static function idiomas($idiomaWeb) {

        $idiomas = null;

        switch ($idiomaWeb) {
            case 'es':
                $idiomas = self::$idiomasES;
                break;
            case 'en':
                $idiomas = self::$idiomasEN;
                break;
            case 'gb':
                $idiomas = self::$idiomasEN;
                break;
            case 'ca':
                $idiomas = self::$idiomasCAT;
                break;
            default:
                $idiomas = self::$idiomasES;
        }

        return $idiomas;
    }

    static function idiomaFormTPV($idiomaWeb) {
        $idiomaActual = null;

        switch ($idiomaWeb) {
            case 'es':
                $idiomaActual = self::$idiomaFormTPVES;
                break;
            case 'en':
                $idiomaActual = self::$idiomaFormTPVEN;
                break;
            case 'gb':
                $idiomaActual = self::$idiomaFormTPVEN;
                break;
            case 'ca':
                $idiomaActual = self::$idiomaFormTPVCAT;
                break;
            default:
                $idiomaActual = self::$idiomaFormTPVES;
        }


        return $idiomaActual;
    }

    static function idiomaFormPagoCorrecto($idiomaWeb) {
        $idiomaActual = null;

        switch ($idiomaWeb) {
            case 'es':
                $idiomaActual = self::$idiomaFormPagoOKES;
                break;
            case 'en':
                $idiomaActual = self::$idiomaFormPagoOKEN;
                break;
            case 'gb':
                $idiomaActual = self::$idiomaFormPagoOKEN;
                break;
            case 'ca':
                $idiomaActual = self::$idiomaFormPagoOKCAT;
                break;
            default:
                $idiomaActual = self::$idiomaFormPagoOKES;
        }


        return $idiomaActual;
    }

    static function idiomaFormPagoIncorrecto($idiomaWeb) {
        $idiomaActual = null;

        switch ($idiomaWeb) {
            case 'es':
                $idiomaActual = self::$idiomaFormPagoKOES;
                break;
            case 'en':
                $idiomaActual = self::$idiomaFormPagoKOEN;
                break;
            case 'gb':
                $idiomaActual = self::$idiomaFormPagoKOEN;
                break;
            case 'ca':
                $idiomaActual = self::$idiomaFormPagoKOCAT;
                break;
            default:
                $idiomaActual = self::$idiomaFormPagoKOES;
        }


        return $idiomaActual;
    }

    /* Obtiene a partir del código ISO del idioma el valor que espera el TPV. 
      En caso de que el ISO no esté soportado devolveremos siempre el castellano. */

    static function isoToTPV($idiomaWeb) {

        $codigoIdioma = '1'; // Por defecto devolvemos el español
        if (array_key_exists(strtolower($idiomaWeb), self::$idiomaISO)) {
            $codigoIdioma = self::$idiomaISO[strtolower($idiomaWeb)];
        }

        return $codigoIdioma;
    }

    static function obtenerCodMonedaISO($moneda)
    {
        $codMoneda = '978';

        if (isset(self::$monedasISO[$moneda]))
            $codMoneda = self::$monedasISO[$moneda];

        return $codMoneda;
    }

    static function inversaObtenerCodMonedaISO($monedaISO)
    {
        $codMoneda = 'EUR';
        $inversaMonedas = array_flip(self::$monedasISO);

        if (isset($inversaMonedas[$monedaISO]))
            $codMoneda = $inversaMonedas[$monedaISO];

        return $codMoneda;
    }

    static function obtenerFormularioTPV($urlPost, $importeFinal, $moneda, $codigoPedido, $idComercio, $terminal, $tipoTransaccion, $titular, $nomComercio, $urlRespuestaTPV, $carritoCompra, $urlOk, $urlKO, $idioma_tpv, $nomFormulario, $merchantData, $merchantIdentifier, $DsMerchantParameters, $DsSignature){

        $inputs = SabadellTPV::ObtenerInputs($importeFinal, $moneda, $codigoPedido, $idComercio, $terminal, $tipoTransaccion, $titular, $nomComercio, $urlRespuestaTPV, $carritoCompra, $urlOk, $urlKO, $idioma_tpv, $nomFormulario, $merchantData, $merchantIdentifier, $DsMerchantParameters, $DsSignature);
        return '<form action="' . $urlPost . '" method="post" id="' . $nomFormulario . '">' . $inputs . '</form>';
    }

    static function obtenerFormularioTPV_WordPress($urlPost, $importeFinal, $moneda, $codigoPedido, $idComercio, $terminal, $tipoTransaccion, $titular, $nomComercio, $urlRespuestaTPV, $carritoCompra, $urlOk, $urlKO, $idioma_tpv, $nomFormulario, $merchantData, $DsMerchantParameters, $DsSignature){
        
        $inputs = SabadellTPV::ObtenerInputs($importeFinal, $moneda, $codigoPedido, $idComercio, $terminal, $tipoTransaccion, $titular, $nomComercio, $urlRespuestaTPV, $carritoCompra, $urlOk, $urlKO, $idioma_tpv, $nomFormulario, $merchantData, null, $DsMerchantParameters, $DsSignature);
         return '<div data-action="' . $urlPost . '" data-method="post" id="' . $nomFormulario . '">' . $inputs . '</div>';         
    }

    static function ObtenerInputs( $importeFinal, $moneda, $codigoPedido, $idComercio, $terminal, $tipoTransaccion, $titular, $nomComercio, $urlRespuestaTPV, $carritoCompra, $urlOk, $urlKO,  $idioma_tpv, $nomFormulario, $merchantData, $merchantIdentifier, $DsMerchantParameters, $DsSignature){

        return '<input type="hidden" name="Ds_Merchant_Amount" id="Ds_Merchant_Amount" value="' . $importeFinal . '" />
                  <input disabled type="hidden" name="Ds_Merchant_Currency" id="Ds_Merchant_Currency" value="' . $moneda . '" />
                  <input disabled type="hidden" name="Ds_Merchant_Order" id="Ds_Merchant_Order" value="' . $codigoPedido . '" />
                  <input disabled type="hidden" name="Ds_Merchant_MerchantCode" id="Ds_Merchant_MerchantCode" value="' . $idComercio . '" />
                  <input disabled type="hidden" name="Ds_Merchant_Terminal" id="Ds_Merchant_Terminal" value="' . $terminal . '" />
                  <input disabled type="hidden" name="Ds_Merchant_TransactionType" id="Ds_Merchant_TransactionType" value="' . $tipoTransaccion . '" />
                  <input disabled type="hidden" name="Ds_Merchant_Titular" id="Ds_Merchant_Titular" value="' . $titular . '" />
                  <input disabled type="hidden" name="Ds_Merchant_MerchantName" id="Ds_Merchant_MerchantName"  value="' . $nomComercio . '" />
                  <input disabled type="hidden" name="Ds_Merchant_ProductDescription" id="Ds_Merchant_ProductDescription" value="' . $carritoCompra . '" />
                  <input disabled type="hidden" name="Ds_Merchant_MerchantURL"  id="Ds_Merchant_MerchantURL" value="' . $urlRespuestaTPV . '" />
                  <input disabled type="hidden" name="Ds_Merchant_UrlOK" id="Ds_Merchant_UrlOK" value="' . $urlOk . '" />
                  <input disabled type="hidden" name="Ds_Merchant_UrlKO" id="Ds_Merchant_UrlKO" value="' . $urlKO . '" />
                  <input disabled type="hidden" name="Ds_Merchant_ConsumerLanguage" id="Ds_Merchant_ConsumerLanguage" value="' . $idioma_tpv . '" />
                  <input disabled type="hidden" name="Ds_Merchant_MerchantData" id="Ds_Merchant_MerchantData" value="' . $merchantData . '" />
                  <input disabled type="hidden" name="Ds_Merchant_Identifier" id="Ds_Merchant_Identifier" value="' . $merchantIdentifier . '" />
                  <input type="hidden" name="Ds_SignatureVersion"  id="Ds_SignatureVersion" value="' . self::VERSION_CIFRADO . '"/>
				  <input type="hidden" name="Ds_MerchantParameters"  id="Ds_MerchantParameters" value="'. $DsMerchantParameters .'"/>
                  <input type="hidden" name="Ds_Signature"  id="Ds_Signature" value="'. $DsSignature .'"/>

                  ';

    }

}

?>