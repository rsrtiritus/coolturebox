<?php
include_once 'apiRedsys.php';


function decrypt_3DES($message, $key)
{
    // Se establece un IV por defecto
    $bytes = array(0, 0, 0, 0, 0, 0, 0, 0); //byte [] IV = {0, 0, 0, 0, 0, 0, 0, 0}
    $iv = implode(array_map("chr", $bytes)); //PHP 4 >= 4.0.2

    // Se cifra
    $ciphertext = mcrypt_decrypt(MCRYPT_3DES, $key, $message, MCRYPT_MODE_CBC, $iv); //PHP 4 >= 4.0.2
    return $ciphertext;
}

function ObtenerClaveEncriptacion($hash, $ds_merchant_order)
{

    $miObj = new RedsysAPI;
    $kcEncriptado = $miObj->base64_url_decode($hash);
    $clave = substr($ds_merchant_order, 0, 5);
    $kc = decrypt_3DES($kcEncriptado, $clave);
    return $kc;

}


$ds_merchant_amount = "";
$ds_merchant_order = "";
$ds_merchant_merchantcode = "";
$ds_merchant_currency = "";
$ds_merchant_transactiontype = "";
$ds_merchant_terminal = "";
$ds_merchant_merchanturl = "";
$ds_merchant_urlok = "";
$ds_merchant_urlko = "";

$ds_Merchant_Titular = null;
$ds_Merchant_MerchantName = null;
$ds_Merchant_ProductDescription = null;
$ds_Merchant_ConsumerLanguage = null;
$ds_Merchant_Identifier = null;
$hashKc = "";
$msgError = "";

if (isset($_POST['DS_MERCHANT_AMOUNT']))
    $ds_merchant_amount = $_POST["DS_MERCHANT_AMOUNT"];
else
    $msgError = "ds_merchant_amount - ";

if (isset($_POST['DS_MERCHANT_ORDER']))
    $ds_merchant_order = $_POST["DS_MERCHANT_ORDER"];
else
    $msgError = $msgError . "ds_merchant_order - ";

if (isset($_POST['DS_MERCHANT_MERCHANTCODE']))
    $ds_merchant_merchantcode = $_POST["DS_MERCHANT_MERCHANTCODE"];
else
    $msgError = $msgError . "ds_merchant_merchantcode - ";

if (isset($_POST['DS_MERCHANT_CURRENCY']))
    $ds_merchant_currency = $_POST["DS_MERCHANT_CURRENCY"];
else
    $msgError = $msgError . "ds_merchant_currency - ";

if (isset($_POST['DS_MERCHANT_TRANSACTIONTYPE']))
    $ds_merchant_transactiontype = $_POST["DS_MERCHANT_TRANSACTIONTYPE"];
else
    $msgError = $msgError . "ds_merchant_transactiontype - ";

if (isset($_POST['DS_MERCHANT_TERMINAL']))
    $ds_merchant_terminal = $_POST["DS_MERCHANT_TERMINAL"];
else
    $msgError = $msgError . "ds_merchant_terminal - ";

if (isset($_POST['DS_MERCHANT_MERCHANTURL']))
    $ds_merchant_merchanturl = $_POST["DS_MERCHANT_MERCHANTURL"];
else
    $msgError = $msgError . "ds_merchant_merchanturl - ";

if (isset($_POST['DS_MERCHANT_URLOK']))
    $ds_merchant_urlok = $_POST["DS_MERCHANT_URLOK"];
else
    $msgError = $msgError . "ds_merchant_urlok - ";

if (isset($_POST['DS_MERCHANT_URLKO']))
    $ds_merchant_urlko = $_POST["DS_MERCHANT_URLKO"];
else
    $msgError = $msgError . "ds_merchant_urlko - ";

if (isset($_POST['HASH']))
    $hashKc = $_POST["HASH"];
else
    $msgError = $msgError . "HASH - ";


if (isset($_POST['DS_MERCHANT_TITULAR']))
    $ds_Merchant_Titular = $_POST["DS_MERCHANT_TITULAR"];

if (isset($_POST['DS_MERCHANT_MERCHANTNAME']))
    $ds_Merchant_MerchantName = $_POST["DS_MERCHANT_MERCHANTNAME"];

if (isset($_POST['DS_MERCHANT_PRODUCTDESCRIPTION']))
    $ds_Merchant_ProductDescription = $_POST["DS_MERCHANT_PRODUCTDESCRIPTION"];

if (isset($_POST['DS_MERCHANT_CONSUMERLANGUAGE']))
    $ds_Merchant_ConsumerLanguage = $_POST["DS_MERCHANT_CONSUMERLANGUAGE"];

if (isset($_POST['DS_MERCHANT_IDENTIFIER']))
    $ds_Merchant_Identifier = $_POST["DS_MERCHANT_IDENTIFIER"];


//Comrprobamos que esté TODO OK
if (!isset($ds_merchant_amount) || $ds_merchant_amount == "" ||
    !isset($ds_merchant_order) || $ds_merchant_order == "" ||
    !isset($ds_merchant_merchantcode) || $ds_merchant_merchantcode == "" ||
    !isset($ds_merchant_currency) || $ds_merchant_currency == "" ||
    !isset($ds_merchant_transactiontype) || $ds_merchant_transactiontype == "" ||
    !isset($ds_merchant_terminal) || $ds_merchant_terminal == "" ||
    !isset($hashKc) || $hashKc == ""
) {
    echo $msgError;
} else {


    $miObj = new RedsysAPI;
    // Se Rellenan los campos
    $miObj->setParameter("DS_MERCHANT_AMOUNT", $ds_merchant_amount);
    $miObj->setParameter("DS_MERCHANT_ORDER", strval($ds_merchant_order));
    $miObj->setParameter("DS_MERCHANT_MERCHANTCODE", $ds_merchant_merchantcode);
    $miObj->setParameter("DS_MERCHANT_CURRENCY", $ds_merchant_currency);
    $miObj->setParameter("DS_MERCHANT_TRANSACTIONTYPE", $ds_merchant_transactiontype);
    $miObj->setParameter("DS_MERCHANT_TERMINAL", $ds_merchant_terminal);
    $miObj->setParameter("DS_MERCHANT_MERCHANTURL", $ds_merchant_merchanturl);
    $miObj->setParameter("DS_MERCHANT_URLOK", $ds_merchant_urlok);
    $miObj->setParameter("DS_MERCHANT_URLKO", $ds_merchant_urlko);

    if (isset($ds_Merchant_Titular) && $ds_Merchant_Titular != "")
        $miObj->setParameter("DS_MERCHANT_TITULAR", $ds_Merchant_Titular);

    if (isset($ds_Merchant_MerchantName) && $ds_Merchant_MerchantName != "")
        $miObj->setParameter("DS_MERCHANT_MERCHANTNAME", $ds_Merchant_MerchantName);

    if (isset($ds_Merchant_ProductDescription) && $ds_Merchant_ProductDescription != "")
        $miObj->setParameter("DS_MERCHANT_PRODUCTDESCRIPTION", $ds_Merchant_ProductDescription);

    if (isset($ds_Merchant_ConsumerLanguage) && $ds_Merchant_ConsumerLanguage != "")
        $miObj->setParameter("DS_MERCHANT_CONSUMERLANGUAGE", $ds_Merchant_ConsumerLanguage);

    if (isset($ds_Merchant_Identifier) && $ds_Merchant_Identifier != "")
        $miObj->setParameter("DS_MERCHANT_IDENTIFIER", $ds_Merchant_Identifier);


    //Datos de configuración
    $kc = ObtenerClaveEncriptacion($hashKc, $ds_merchant_order);
    $params = $miObj->createMerchantParameters();
    $signature = $miObj->createMerchantSignature($kc);

    echo '{"Ds_MerchantParameters":"' . $params . '","Ds_Signature":"' . $signature . '"}';

}


?>