<?php
/* Plugin Name: Intranet para trabajadores de Festibox
 * Description: Este pluging desarrolla toda la intranet para los trabajadores y la gestión de Festibox.
 * Author: Cuatro Notas
 * Author URI: http://cuatronotas.com
 * Version: 1.0
 */

/*
Metemos las Inludes.
*/
require_once('includes/activaciones_caja.php');
require_once('includes/telecor.php');
require_once('includes/reservas.php');
require_once('includes/usuarios.php');
require_once('includes/function.php');
require_once('includes/roles.php');
require_once('includes/distribuidor.php');

/*
Asignamos los roles .
*/
add_role( 'festibox', __('Personal de Festibox2', 'your-plugin-textdomain' ), $capabilities  );

/*
Declarams los Short. .
*/
add_shortcode('gestion_hacer_reservas','gestion_hacer_reservas_short');
add_shortcode('activaciones_resumen','activaciones_resumen_short');
add_shortcode('activar_caja','activar_caja_short');
add_shortcode('hipercor','hipercor_short');
add_shortcode('usuario_colaborador','usuario_colaborador_short');
add_shortcode('anadir_colaborador','anadir_colaborador_short');
add_shortcode('asignar_colaborador','asignar_colaborador_short');
add_shortcode('cajas_nuevas','cajas_nuevas_short');
add_shortcode('anadir_distribuidor','anadir_distribuidor_short');
add_shortcode('hipercor','hipercor_short');

/*
Decidimos como metemos el resumne. .
*/
add_shortcode('festibox_intranet','festibox_intranet_short');
add_action( 'woocommerce_before_my_account','festibox_intranet_short');

/*
Asignamos el AJAX para los servicios
*/
add_action( 'wp_ajax_nopriv_eventos_usuarios_ajax', 'eventos_usuarios_ajax');
add_action( 'wp_ajax_eventos_usuarios_ajax', 'eventos_usuarios_ajax');

/*
SHORTCODE CANCELAR A BORRAR CUADNO se HAYA INSTALADO EL NUEVO
*/
add_action( 'wpcf7_init', 'custom_add_shortcode_usuarios_form' );
function custom_add_shortcode_usuarios_form() {
    wpcf7_add_shortcode( 'usuario', 'custom_usuarios_form_shortcode_handler' ); // "clock" is the type of the form-tag
}

//Epaquetamos los datos del uruario para contact form.
function custom_usuarios_form_shortcode_handler( $tag ) {
	$current_user = wp_get_current_user();
	$hidden = $email = $nombre = $apellidos = $telefono = '';
	if ( is_user_logged_in() && !current_user_can( 'festibox' ) && !current_user_can( 'administrator' ) ) {
		$email = $current_user->user_email;
		$nombre = $current_user->user_firstname;
		$apellidos = $current_user->user_lastname;
		$telefono = get_user_meta($current_user->ID,'user_phone',true);
	}
	$salida = '
	<p>Nombre (obligatorio)</br>
    <span class="wpcf7-form-control-wrap">
        <input type="text" name="your-name" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" value="'.$nombre.'">
    </span>
    </p>

	<p>Apellidos (obligatorio)</br>
    <span class="wpcf7-form-control-wrap tel-379">
        <input type="text" name="your-surname" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" value="'.$apellidos.'">
    </span>
	</p>

	<p>Su e-mail (obligatorio)</br>
    <span class="wpcf7-form-control-wrap">
    	<input type="email" name="your-email" size="40" class="wpcf7-form-control wpcf7-text  wpcf7-validates-as-required" aria-required="true" aria-invalid="false" value="'.$email.'">
    </span>
    </p>

    <input type="hidden" name="your-ID" value="'.$current_user->ID.'">';
//  A poner antes del input para reservas
//	<p>Teléfono (obligatorio) (este número servirá para avisarte en caso de que haya alguna cancelación en relación al festival o espectáculo seleccionado)</br>
//    <span class="wpcf7-form-control-wrap">
//        <input type="tel" name="tel" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" value="'.$telefono.'">
//    </span>
//    </p>
	return $salida;
}

add_action('wpcf7_before_send_mail', 'cancelar_reserva_form');

function cancelar_reserva_form($wpcf7){
    $submission = WPCF7_Submission::get_instance();
	$formulario = $wpcf7->title();
	if ( $formulario == 'cancelar' && $submission){
		$data = $submission->get_posted_data();
		cancelar_reserva($data['codigo']);
	}
}

function confirmar_cancelar_short(){
	if ( $_POST['codigo'] ){
		global $wpdb;
		$salida = '<div class="wpcf7-response-output wpcf7-display-none wpcf7-mail-sent-ok" style="display: block;" role="alert"><p>Le confirmamos que su cancelación ha sido tramitada.</p></div>';
		cancelar_reserva($_POST['codigo']);
		
		$consulta = "SELECT * FROM wp_reservas WHERE code = '".$_POST['codigo']."'";
		$reservas = $wpdb->get_results($consulta);
		$wpdb->show_errors();
		if ( $reservas[0] -> usuario ){
			$user_data = get_userdata( $reservas[0] -> usuario );
			$email = $user_data->user_email;
			$nombre = $user_data->user_firstname;
			$apellidos = $user_data->user_lastname;
		}else{
			$email = $reservas[0]->email;
			$nombre = $reservas[0]->nombre;
			$apellidos = $reservas[0]->apellidos;
		}

		//Filtro para indicar que email debe ser enviado en modo HTML
		add_filter('wp_mail_content_type',create_function('', 'return "text/html";'));

		$to = array('hola@coolturebox.com','reservas@coolturebox.com');

		$subject = 'Cancelación '.$_POST['codigo'];

		$message = 'Se ha realizado una cancelación con el código '.$_POST['codigo'];

		$headers = 'From:  '.$nombre.' '.$apellidos.' <'.$email.'>';

		wp_mail( $to, $subject, $message, $headers );
		
		$subject = 'Coolture Cancelación '.$_POST['codigo'];
		
		$message = 'Hola '.$nombre.' '.$apellidos.',<br/><br/>';
		$message .= 'Le informamos de que hemos recibido su solicitud de cancelación. En estos momentos nuestro departamento de reservas está procesando su solicitud y en un plazo entre 7 y 15 días recibirá el reembolso del importe si es un servicio de Ticket Premium.<br/><br/>';
		$message .= 'La solicitud realizada es la siguiente:<br/>';
		$message .= $nombre.' '.$apellidos.'<br/>';
		$message .= 'Código reserva: '.$_POST['codigo'].'<br/>';
		$message .= 'Para cualquier información complementaria, escríbanos a reservas@coolturebox.com. Nuestro personal estará encantado de atenderle.<br/><br/>';
		$message .= 'Reciba un cordial saludo.<br/><br/>';
		$message .= 'El equipo de Coolturebox.<br/>';
				
		$headers = 'From: Coolture Reservas<reservas@coolturebox.com>';

		wp_mail( $email, $subject, $message, $headers );
	}else{
		$salida = '
			<form action="/cancelar-reserva/" method="post" class="wpcf7-form sent">
			<label for="codigo">Confirma para cancelar el código. <input type="hidden" name="codigo" value="'.$_GET['codigo'].'" id="codigo"><input type="submit" value="Confirmar" id="cancelar_reserva" name="confirmar"></label>
			</form>';
	}
	return $salida;
}
add_shortcode('confirmar_cancelar','confirmar_cancelar_short');

