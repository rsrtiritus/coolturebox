<?php

function usuario_colaborador_short(){
	do_shortcode('[anadir_colaborador]');
	do_shortcode('[asignar_colaborador]');
}

function anadir_colaborador_short(){
	if ( is_user_logged_in() && ( current_user_can( 'festibox' ) || current_user_can( 'administrator' ) ) ) {
		?>
        <div class="woocommerce intranet_gestion">
		
		<?php
		if (isset($_POST['crear'])) {
			$username = $_POST['nick'];
			$email = $_POST['email'];
			$password = wp_generate_password( $length=12, $include_standard_special_chars=false );
		
			$user_id = username_exists( $username );
			if ( !$user_id && email_exists($email) == false ) {
				$user_id = wp_create_user( $username, $password, $email );
				if( !is_wp_error($user_id) ) {
					$user = get_user_by( 'id', $user_id );
					$user->set_role( $_POST['role'] );
					
					wp_new_user_notification( $user_id, null, 'both' );

				}
				echo '<h2>Usuario creado.</h2>';
			} else {
				echo '<h2>El usuario ya existe.</h2>';
			}	
		}else{
			echo '<h2>Crea tu usuario.</h2>';
		}
		?>
		
        <form method="post" enctype="multipart/form-data">
			<label for="nick">
				<span style="display:block">Nick: </span>
				<input type="text" name="nick" style="display:inline-block"/>
			 </label>
			<label for="email">
				<span style="display:block">Email: </span>
				<input type="email" name="email" style="display:inline-block"/>
			 </label>
			<label for="role">
				<span style="display:block">Role: </span>
				<select name="role" style="display:inline-block" required>
					<option value="festibox" selected>Trabajador Festibox</option>
					<option value="colaborador" >Festibox Colaborador</option>
					<option value="customer" >Cliente</option>
				</select>
			 </label>
			<input type="submit" value="Crear" class="button" name="crear" >
		</form>
	</div>
	<?php
	}
}

function asignar_colaborador_short(){
	if ( is_user_logged_in() && ( current_user_can( 'festibox' ) || current_user_can( 'administrator' ) ) ) {
		?>
        <div class="woocommerce intranet_gestion">
		
		<?php
		if (isset($_POST['asignar'])) {
			update_user_meta( $_POST['usuario'], '_referencias', $_POST['referencia'] );
			echo '<h2>Valores asignados.</h2>';
		}else{
			echo '<h2>Asigna los valores a tu colaborador.</h2>';
		}
		$users = get_users( array( 'orderby' => 'nicename', 'role' => 'colaborador' ) );

		?>
		
        <form method="post" enctype="multipart/form-data">
			<label for="usuario">
				<span>Usuario: </span>
				<select name="usuario" required id="usuario">
					<option value="" selected>Elige un usuario</option>
					<?php foreach ($users as $user){
						echo '<option value="'.$user->ID.'">'.$user->user_login.'</option>';
					}?>
				</select>
			 </label>
			<input type="submit" value="Asignar" class="button" name="asignar">
            <div id="resultado_evento">
            </div>
 		</form>
   </div>
	<script type="text/javascript">
        (function($){
            $('#usuario').change(function(){
//                alert ( $('#usuario').val() );
				$.ajax({
                    url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
                    type: 'get',
                    data: 'action=eventos_usuarios_ajax&usuario=' + $('#usuario').val(),
                    success: function(output){
                        $('#resultado_evento').html(output);
                    }
                });
            });
        })(jQuery);
    </script>
	<?php
	}
}

function eventos_usuarios_ajax(){
	if ( isset( $_GET['usuario'] ) && $_GET['usuario'] != '' ){
		$referencias = array('0');
		if ( get_the_author_meta( '_referencias', $_GET['usuario'] ) ) 
			$referencias = get_the_author_meta( '_referencias', $_GET['usuario'] );
		
		$data = array( 
			'post_type' 	 => 'festival',
			'posts_per_page' => -1,
			'post_status' 	 => 'publish',
			'order' 		 => 'ASC',
			'orderby' 		 => 'title'
			);
		$festivales =  get_posts( $data );
//		print_r($festivales);
		
		$data = array( 
			'post_type' 	 => 'concierto',
			'posts_per_page' => -1,
			'post_status' 	 => 'publish',
			'order' 		 => 'ASC',
			'orderby' 		 => 'title'
			);
		$conciertos =  get_posts( $data );
//		print_r($conciertos);
		
		$data = array( 
			'post_type' 	 => 'espectaculo',
			'posts_per_page' => -1,
			'post_status' 	 => 'publish',
			'order' 		 => 'ASC',
			'orderby' 		 => 'title'
			);
		$espectaculos =  get_posts( $data );
//		print_r($espectaculos);

		if ($festivales) {
		?>
		<div style="width:100%">
			<label for="festivales" style="width:100%">
				<h2>Festivales: </h2>
				<?php foreach ($festivales as $festival){
                    ?>
                    <div class="data" style="display:inline-block;width:15%;margin:3px; border-bottom:1px thin #CCC; vertical-align:top ">
                        <input type="checkbox"  value="<?php echo $festival -> ID;?>" name="referencia[]" <?php if ( in_array( $festival -> ID, $referencias ) ) echo "checked";?> />
                        <span><?php echo get_the_title($festival -> ID);?></span>
                    </div>
                    <?php
                }?>
			 </label>
		</div>
        <?php
		}
		if ($espectaculos) {
		?>
		<div style="width:100%">
			<label for="espectaculos" style="width:100%">
				<h2>Espectáculos: </h2>
				<?php 
				foreach ($espectaculos as $espectaculo){
					?>
					<div class="data" style="display:inline-block;width:15%;margin:3px; border-bottom:1px thin #CCC; vertical-align:top ">
						<input type="checkbox"  value="<?php echo $espectaculo -> ID;?>" name="referencia[]" <?php if ( in_array( $espectaculo -> ID, $referencias ) ) echo "checked";?> />
						<span><?php echo get_the_title($espectaculo -> ID);?></span>
					</div>
				<?php
                }?>
			 </label>
		</div>
        <?php
		}
		if ($conciertos) {
		?>
		<div style="width:100%">
			<label for="conciertos" style="width:100%">
				<h2>Conciertos: </h2>
				<?php 
				foreach ($conciertos as $concierto){
					?>
					<div class="data" style="display:inline-block;width:15%;">
						<input type="checkbox"  value="<?php echo $concierto -> ID;?>" name="referencia[]" <?php if ( in_array( $concierto -> ID, $referencias ) ) echo "checked";?> />
						<span><?php echo get_the_title($concierto -> ID);?></span>
					</div>
				<?php
                }?>
			 </label>
		</div>
		<?php
		}
	}
}

