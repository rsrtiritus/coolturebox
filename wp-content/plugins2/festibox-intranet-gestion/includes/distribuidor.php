<?php
function cajas_nuevas_short(){
	if ( is_user_logged_in() && ( current_user_can( 'festibox' ) || current_user_can( 'administrator' ) ) ) {
		if (isset($_POST['insertar_caja'])) {
			global $wpdb_ws;
			$archivos = $_FILES['archivos'];
			$fileCount = count($archivos["name"]);
			
			echo '<h2>Actualiza o inserta las cajas</h2>';
            for ($i = 0; $i < $fileCount; $i++) {
				?>
				<h3>File #<?= $i+1 ?>:</h3>
				<p>
					Name: <?= $archivos["name"][$i] ?><br>
					Temporary file: <?= $archivos["tmp_name"][$i] ?><br>
					Type: <?= $archivos["type"][$i] ?><br>
					Size: <?= $archivos["size"][$i] ?><br>
					Error: <?= $archivos["error"][$i] ?><br>
				</p>
					
				<?php
				$ar=fopen($archivos["tmp_name"][$i],"r") or
				  die("No se pudo abrir el archivo");
				while (!feof($ar))
				{
					$linea=fgets($ar);
					list($codigo,$modelo,$cod_reser,$codigo_tienda,$valid,$producto,$usuacio) = explode(',',$linea);
					$caja = array(
						'codigo'  		=> trim($codigo),
						'modelo'		=> trim($modelo),
						'codigo_tienda'	=> trim($codigo_tienda),
						'cod_reser'		=> trim($cod_reser),
						'valid'			=> trim($valid),
						'producto' 		=> trim($producto),
						'usuacio'  		=> trim($usuacio)
					);
					
					if ($caja['usuacio'] == '') $caja['usuacio'] = 'COOLTUREBOX';
					//$caja['usuacio'] = 'COOLTUREBOX';
					
					require_once(ABSPATH . 'ws_conect.php');
					if( !$wpdb_ws -> get_results( "SELECT codigo FROM ws WHERE codigo = ".$codigo ) )
					{
						$caja['fecha_IN'] = date('Y-m-d H:i:s');
						insert_ws ($caja);
						echo '<p>El código '.$codigo.' se ha insertado correctamente</p>';
					}else{
						$caja_where = array('codigo'  	=> $codigo);
						update_ws ($caja_where, $caja);
						echo '<p>El código '.$codigo.' se ha actualizado correctamente</p>';
					}
				}
			}
			
			echo '<h2>Si quieres subir más archivos, continua.</h2>';
		}else{
			echo '<h2>Selecciona los archivos a subir.</h2>';
		}
		echo '
		<form method="post" enctype="multipart/form-data">
			<input type="file" name="archivos[]" multiple>
			<input type="submit" value="Insertar" name="insertar_caja">
		</form>
        <div class="wpcf7-response-output wpcf7-mail-sent-ok" style="display: block;" role="alert">
            <p>El formato del archivo tiene que ser un csv separado por comas en el orden: código de la caja, modelo código de reserva, código de tienda, Fecha de caducidad (YYYY-MM-DD), Código del Producto, Tienda,VACIO. Sin espacios entre las comas.</p>
        </div>';
	}
}

function anadir_distribuidor_short(){
	if ( is_user_logged_in() && ( current_user_can( 'festibox' ) || current_user_can( 'administrator' ) ) ) {
		global $wpdb_ws;
		require_once(ABSPATH . 'ws_conect.php');
		?>
        <div class="woocommerce intranet_gestion">
		<?php
		if (isset($_POST['crear'])) {
			$tienda = $_POST['tienda'];
			$grupo = $_POST['grupo'];
			$password = md5( $_POST['password'] );
			if ( !$wpdb_ws -> get_results('SELECT * FROM ws_usuario WHERE usuario = \''.$tienda.'\'') ) {
            	?>
                <h2>Tienda creada.</h2>
            	<?php
			} else {
            	?>
				<h2>La tienda ya existe.</h2>
            	<?php
			}	
		}else{
            ?>
			<h2>Crea una tienda.</h2>
            <?php
		}
		?>
		
        <form method="post" enctype="multipart/form-data">
			<label for="tienda">
				<span>Tienda: </span>
				<input type="text" name="tienda"/>
			 </label>
			<label for="Grupo">
				<span>Grupo: </span>
				<input type="text" name="grupo"/>
			 </label>
			 <label for="password">
				<span>Password: </span>
				<input type="password" name="password"/>
			 </label>
			<input type="submit" value="Crear" class="button" name="crear" >
		</form>
	</div>
    <div class="woocommerce reservas_historico_cliente">
        <h2>Tiendas</h2>
        <?php
		
		$tiendas = $wpdb_ws -> get_results('SELECT * FROM ws_usuario ORDER BY usuario ASC');
		$wpdb_ws->show_errors();
        ?>
        <table class="shop_table shop_table_responsive tiendas">
            <thead><tr>
                <th class="tienda">Tienda</th>
                <th class="tienda_grupo">GRupo de Tienda</th>
                <th></th>
            </tr></thead>
            <tbody>
            <?php
            foreach($tiendas as $tienda){
                ?>
                <tr>
                    <td class="tienda"><?php echo $tienda->usuario;?></td>
                    <td class="tienda_grupo"><?php echo $tienda->grupo;?></td>
                </tr>
            <?php }?>
            </tbody>
        </table>
    </div>
	<?php
	}
}


