<?php

function hipercor_short(){
$current_user = wp_get_current_user();
    echo '<h2>Username: ' . $current_user->user_login . '</h2>';
	if ( is_user_logged_in() && ( current_user_can( 'coolturebox' ) || current_user_can( 'administrator' ) ) ) {
		if (isset($_FILES['archivos']) && isset($_POST['telecor'])) {
			$archivos = $_FILES['archivos'];
			$fileCount = count($archivos["name"]);
	
			echo '<h2>Los Archivos se están cargando correctamente.</h2>';
			for ($i = 0; $i < $fileCount; $i++) {
				?>
				<h3>File #<?= $i+1 ?>:</h3>
				<p>
					Name: <?= $archivos["name"][$i] ?><br>
					Temporary file: <?= $archivos["tmp_name"][$i] ?><br>
					Type: <?= $archivos["type"][$i] ?><br>
					Size: <?= $archivos["size"][$i] ?><br>
					Error: <?= $archivos["error"][$i] ?><br>
				</p>
					
				<?php
				$ar=fopen($archivos["tmp_name"][$i],"r") or
				  die("No se pudo abrir el archivo");
				while (!feof($ar))
				{
					$linea=fgets($ar);
					if (substr($linea,0,4) == '6080')
					{
						$codigo = ltrim(trim(substr($linea,10,25)),0);
						//Vamos a crear otra variable para comparar directamente con el código de la caja (15/01/2017)
						if (ltrim(trim(substr($linea,10,6)),0) == 'Teatro')
						{
							$codigo_caja = ltrim(trim(substr($linea,16,9)),0);
						}
						else if (ltrim(trim(substr($linea,10,9)),0) == 'Musicales')
						{
							$codigo_caja = ltrim(trim(substr($linea,19,9)),0);
						}
						else if (ltrim(trim(substr($linea,10,11)),0) == 'Felicidades')
						{
							$codigo_caja = ltrim(trim(substr($linea,21,9)),0);
						}
						else if (ltrim(trim(substr($linea,10,5)),0) == 'Humor')
						{
							$codigo_caja = ltrim(trim(substr($linea,15,9)),0);
						}
						else if (ltrim(trim(substr($linea,10,10)),0) == 'Festivales')
						{
							$codigo_caja = ltrim(trim(substr($linea,20,9)),0);
						}
						else if (ltrim(trim(substr($linea,10,6)),0) == 'Museos')
						{
							$codigo_caja = ltrim(trim(substr($linea,16,9)),0);
						}
						else if (ltrim(trim(substr($linea,10,5)),0) == 'Danza')
						{
							$codigo_caja = ltrim(trim(substr($linea,15,9)),0);
						}
						else if (ltrim(trim(substr($linea,10,4)),0) == 'Cena')
						{
							$codigo_caja = ltrim(trim(substr($linea,14,9)),0);
						}
						else if (ltrim(trim(substr($linea,10,5)),0) == 'Magia')
						{
							$codigo_caja = ltrim(trim(substr($linea,15,9)),0);
						}
						else if (ltrim(trim(substr($linea,10,7)),0) == 'Clasica') //Se modifica para leer correctamente el fichero (en el fichero, la palabra Clásica debe ir sin tilde)
						{
							$codigo_caja = ltrim(trim(substr($linea,17,9)),0);
						}
						//Tratamiento para cajas antiguas de FESTIBOX
						if (ltrim(trim(substr($linea,10,6)),0) == 'ORANGE')
						{
							$codigo_caja = ltrim(trim(substr($linea,16,9)),0);
						}

						if (ltrim(trim(substr($linea,10,4)),0) == 'PINK')
						{
							$codigo_caja = ltrim(trim(substr($linea,14,9)),0);
						}

						if (ltrim(trim(substr($linea,10,4)),0) == 'BLUE')
						{
							$codigo_caja = ltrim(trim(substr($linea,14,9)),0);
						}
						if (ltrim(trim(substr($linea,10,5)),0) == 'GREEN')
						{
							$codigo_caja = ltrim(trim(substr($linea,13,5)),0);
						}
						if (ltrim(trim(substr($linea,10,7)),0) == 'YELLOW')
						{
							$codigo_caja = ltrim(trim(substr($linea,17,9)),0);
						}
						if (ltrim(trim(substr($linea,10,6)),0) == 'PURPLE')
						{
							$codigo_caja = ltrim(trim(substr($linea,16,9)),0);
						}
						if (ltrim(trim(substr($linea,10,5)),0) == 'BLACK')
						{
							$codigo_caja = ltrim(trim(substr($linea,15,9)),0);
						}
						if (ltrim(trim(substr($linea,10,3)),0) == 'RED')
						{
							$codigo_caja = ltrim(trim(substr($linea,13,9)),0);
						}
						//trazas:
						//echo '<H2>linea...' .$linea. ' </H2>';
						//echo '<h2>ltrim linea...' .ltrim(trim(substr($linea,10,7)),0). '</h2>';
						//echo '<H2>codigo_caja...' .$codigo_caja. ' </H2>';
						$fecha = date("Y-m-d", mktime(0, 0, 0, trim(substr($linea,37,2)), trim(substr($linea,35,2)), trim(substr($linea,39,2))));
						//echo '<H2>$fecha...' .$fecha. ' </H2>';
						$Estado = abs(trim(substr($linea,80,1)-1));
						//echo '<H2>Estado...' .$Estado. ' </H2>';						
						$usuario = 'Hipercor '.substr($linea,81,2);
						//echo '<H2>usuario...' .$usuario. ' </H2>';	
						require_once(ABSPATH . 'ws_conect.php');
						//Comparamos directamente con el codigo de la caja (15/01/2017)
						//$signs = $wpdb_ws->get_results("SELECT * FROM ws WHERE codigo_tienda = '".$codigo."'");
						$signs = $wpdb_ws->get_results("SELECT * FROM ws WHERE codigo = '".$codigo_caja."'");
						$ok = 0;
						foreach ($signs as $sign) 
						{
							//Añadimos a la actualizacion el usuario2 --> HIPERCOR para identificar las cajas vendidas y activadas desde HIPERCOR (15/01/2017)
							$data_array = array( 'activ' 	=> $Estado, 'usuacio2' => 'HIPERCOR');
							$where = array( 
								'codigo' 	=> $sign->codigo
							);
							update_ws ($where, $data_array);
				
							$historico = array(
								'codigo'  => $sign->codigo,
								'fecha'	  => $fecha,
								'usuario' => $usuario,
								'Estado'  => $Estado
							);
							insert_historico ($historico);
							$ok = 1;
						}						
						if ($ok == 1)
						{
							echo '<p>El código '.$codigo.' se ha insertado correctamente</p>';
						}
						else
						{
							echo '<p>El código '.$codigo.' no se encuentra</p>';
						}
					}
				}
			}
			
			echo '<h2>Activa más cajas de Telecor.</h2>';
		}else{
			echo '<h2>Activar las cajas de Telecor.</h2>';
		}
		echo '
		<form method="post" enctype="multipart/form-data">
			<input type="file" name="archivos[]" multiple>
			<input type="submit" value="Enviar" name="telecor">
		</form>';
	}
}