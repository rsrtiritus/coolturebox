<?php

function activaciones_resumen_short(){
	if ( is_user_logged_in() && ( current_user_can( 'festibox' ) || current_user_can( 'administrator' ) ) ) {
		
		do_shortcode( '[activar_caja]' );
		do_shortcode( '[hipercor]' );
		$filtro = array();
		if ( isset( $_POST['cj'] ) && $_POST['cj'] != '' ) $filtro['caja'] = $_POST['cj'];
		if ( isset( $_POST['usuario'] ) && $_POST['usuario'] != '' ) $filtro['usuario'] = $_POST['usuario'];
		if ( isset( $_POST['st'] ) && $_POST['st'] != '' ) $filtro['st'] = $_POST['st'];
		if ( isset( $_POST['fi'] ) && $_POST['fi'] != '' ) $filtro['fi'] = $_POST['fi'];
		if ( isset( $_POST['ff'] ) && $_POST['ff'] != '' ) $filtro['ff'] = $_POST['ff'];
		if ( isset( $_POST['orderby'] ) &&  isset( $_POST['orderact'] ) ){
			$filtro['by'] = $_POST['orderby'];
			$filtro['act'] = $_POST['orderact'];
		}

		$historicos = historico($filtro);
		//En la V2 hacer con AJAX y paginación meter el número de entradas visibles en cada página.
		?>
		<div class="woocommerce historico_activacion">
            <h2>Histórico de las Cajas</h2>
			<?php
            historico_filtro($filtro);
            ?>
            <table class="shop_table shop_table_responsive reservas_clientes">
                <thead><tr>
					<td class="historico_contador"></td>
					<td class="historico_codigo">Código</td>
					<td class="historico_fecha">Fecha</td>
					<td class="historico_usuario">Usuario</td>
					<td class="historico_estado">Estado</td>
					<td class="historico_acción"></td>
                </tr></thead>
                <tbody>
					<?php
                    $i = 1;
					foreach($historicos as $historico){
						?>
                        <tr>
                            <td class="historico_contador"><?php echo $i;?></td>
                            <td class="historico_codigo"><?php echo $historico -> codigo;?></td>
                            <td class="historico_fecha"><?php echo $historico -> fecha;?></td>
                            <td class="historico_usuario"><?php echo $historico -> usuario;?></td>
                            <td class="historico_estado">
							<?php if ($historico -> Estado == 1 ){
								echo 'Activación';
							}else{
								echo 'Desactivación';
							}
							?>
                            </td>
                        </tr>
                        <?php      
                        $i++;
                    }
                    ?>
                </tbody>
            </table>
        </div>
		<?php
	}
}

function activar_caja_short(){
	if ( is_user_logged_in() && ( current_user_can( 'festibox' ) || current_user_can( 'administrator' ) ) ) {
		if (isset($_POST['activar']) || isset($_POST['desactivar']) ) {
			$current_user = wp_get_current_user();
			$data_array = array( 
				'usuacio2'  => $current_user -> display_name,
				'activ' 	=> $_POST['estado']
			);
			$where = array( 
				'codigo' 	=> $_POST['codigo']
			);
			update_ws ($where, $data_array);

			$historico = array(
				'codigo'  => $_POST['codigo'],
				'usuario' => $current_user -> display_name,
				'Estado'  => $_POST['estado']
			);
			insert_historico ($historico);

			echo '<h2>Se ha realizado la activación o desactivación. Compruebe que se ha realizado correctamente.</h2>';
		}
		?>
		<div class="woocommerce historico_activacion">
            <h2>Activar cajas.</h2>
            <p>Puedes activar una caja esté donde esté, pero simepre aparecerá en el Histórico como el usuario que la ha activado. Si está asignada a una tienda puede crear errores futuros.</p>  
            <form method="post" enctype="multipart/form-data">
                <label for="codigo">
                    <span>Código:</span>
                    <input type="number" name="codigo" min="0">
 				</label>
                <label for="estado">
                	<span>Estado:</span>
                    <select name="estado">
                        <option value="1">Activar</option>
                        <option value="0">Desactivar</option>
                    </select>
                </label>
                <input type="submit" value="Activar" class="button" name="activar" >
            </form>
        </div>
        <?php    
	}
}
