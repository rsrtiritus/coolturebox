<?php
function festibox_intranet_short(){
	if ( is_user_logged_in() && ( current_user_can( 'festibox' ) || current_user_can( 'administrator' ) ) ) {
		?>
		<div class="woocommerce intranet_gestion">
            <h2>Elige opciones de Festibox</h2>
            <a href="<?php echo site_url('/activaciones/');?>" class="button ver">Cajas</a>
            <a href="<?php echo site_url('/reservas/');?>" class="button ver">Reservas</a>
            <a href="<?php echo site_url('/colaboradores/');?>" class="button activar">Colaboradores</a>
			<?php if ( current_user_can( 'administrator' ) ){ ?>
                <a href="<?php echo site_url('/distribuidores/');?>" class="button activar">Distribuidores</a>
			<?php } ?>
        </div>
        <?php
	}
}

function historico($data_args){
	global $wpdb_ws;
//	$defaults = array( 
//		'caja' 	  => 'codigo LIKE \'%%\'',
//		'usuario' => 'usuario LIKE \'%%\'',
//		'st' 	  => '',
//		'fi' 	  => 0,
//		'ff' 	  => date('Y-m-d H:i:s'),
//		'by' 	  => 'fecha' ,
//		'act' 	  => 'DESC'
//	);
//
//	$data_args = wp_parse_args( $data_args, $defaults );
	if ($data_args['fi'] == '') $data_args['fi'] = 0;
	if ($data_args['ff'] == '') $data_args['ff'] = date('Y-m-d H:i:s', mktime( 0, 0, 0, date("m")  , date("d")+1, date("Y") ) );;
	if ($data_args['select'] == '') $data_args['select'] = '*';
	if ($data_args['act'] == '') $data_args['act'] = 'DESC';
	if ($data_args['by'] == '') $data_args['by'] = 'fecha';
	
	if ($data_args['usuario'] != ''){
		$data_args['usuario'] = 'LIKE \'%'.$data_args['usuario'].'%\'';
	}else{
		$data_args['usuario'] = 'LIKE \'%%\'';
	}
	if ($data_args['caja'] != ''){
		$data_args['caja'] = '= '.$data_args['caja'];
	}else{
		$data_args['caja'] = 'LIKE \'%%\'';
	}

	$consulta = "SELECT ".$data_args['select']." FROM ws_historico WHERE usuario ".$data_args['usuario']." AND Estado LIKE '%".$data_args['st']."%' AND codigo ".$data_args['caja']." AND (fecha BETWEEN '".str_replace('T',' ',$data_args['fi'])."' AND '".str_replace('T',' ',$data_args['ff'])."') ORDER BY ".$data_args['by']." ".$data_args['act'];
	require_once(ABSPATH . 'ws_conect.php');
	
	$signs = $wpdb_ws -> get_results($consulta);
	$wpdb_ws->show_errors();

	return $signs;
}

function historico_filtro($filtro, $ofset){

	?>
	<div class="filter">
		<form method="POST" enctype="multipart/form-data">
			<label for="Caja">
				<span style="display:block">Caja: </span>
				<input type="number" name="cj" value="<?php echo $filtro['caja'];?>" min="0" style="display:inline-block"/>
			 </label>
			<label for="Usuario">
				<span style="display:block">Usuario: </span>
				<input type="text" name="usuario" value="<?php echo $filtro['usuario'];?>" style="display:inline-block"/>
			</label>
			<label for="Estado">
				<span style="display:block">Estado: </span>
				<select name="st" style="display:inline-block">
					<option value="" selected>Ninguno</option>
					<option value="1" <?php if (isset($filtro['st']) && $filtro['st'] == 1) echo 'selected';?>>Activado</option>
					<option value="0" <?php if (isset($filtro['st']) && $filtro['st'] == 0) echo 'selected';?>>Desactivado</option>
				</select>
			</label>
<!--            <label for="fecha">
                <span  style="display:block">Rango de fechas: </span>
                <input type="date" name="fi" style="display:inline-block" />
                <span> - </span>
                <input type="date" name="ff" style="display:inline-block"/>
            </label>
-->			<label for="Order">
				<span style="display:block">Ordenado por: </span>
				<select name="orderby" style="display:inline-block">
					<option value="fecha" <?php if ($filtro['by'] == 'fecha') echo 'selected';?>>Fecha</option>
					<option value="codigo" <?php if ($filtro['by'] == 'codigo') echo 'selected';?>>Caja</option>
					<option value="Estado" <?php if ($filtro['by'] == 'Estado') echo 'selected';?>>Estado</option>
				</select>
				 <select name="orderact" style="display:inline-block">
					<option value="DESC" <?php if ($filtro['act'] == 'DESC') echo 'selected';?>>Descente</option>
					<option value="ASC" <?php if ($filtro['act'] == 'ASC') echo 'selected';?>>Ascendente</option>
				</select>
			</label>
			<input type="submit" name="filtrar" value="Filtrar"class="button ver" />
		</form>
	</div>
    <?php
}


/*
Insertamos en el historico del Webservice
*/
function insert_historico ($historico){
	global $wpdb_ws;
	require_once(ABSPATH . 'ws_conect.php');
	$wpdb_ws->insert( 'ws_historico', $historico );
	$wpdb_ws->show_errors();
}

/*
Actualizamos la tabla que indica que los códigos se han activado
*/
function update_ws ($where, $data_array){
	global $wpdb_ws;
	require_once(ABSPATH . 'ws_conect.php');
	$wpdb_ws->update( 'ws', $data_array, $where );
	$wpdb_ws->show_errors();
}
function insert_ws ($data_array){
	global $wpdb_ws;
	require_once(ABSPATH . 'ws_conect.php');
	$wpdb_ws->insert( 'ws', $data_array );
	$wpdb_ws->show_errors();
}

if( !function_exists('reservas_filtro')) {
	/*
	Filtro para obtener_reseras.
	*/
	function reservas_filtro($data_args){
		?>	
			
		<div class="filter">
			<form method="POST" enctype="multipart/form-data">
	            <label for="Codigo">
	                <span style="display:block">Codigo: </span>
	                <input type="text" name="codigo" style="display:inline-block" value="<?php echo $data_args['codigo'];?>"/>
	            </label>
		<?php
//	            <label for="Usuario">
//	                <span style="display:block">Usuario: </span>
//	                <input type="text" name="usuario" style="display:inline-block"/>
//	            </label>
	//            <label for="Festival">
	//                <span style="display:block">Evento: </span>
	//                <input type="text" name="festival" style="display:inline-block"/>
	//             </label>
	//            <label for="hotel">
	//                <span style="display:block">Hotel: </span>
	//                <input type="text" name="hotel" style="display:inline-block"/>
	//             </label>
	//            <label for="Pedido">
	//                <span style="display:block">Pedido: </span>
	//                <input type="number" name="order" value=" echo $data_args['order'];" min="0" style="display:inline-block"/>
	//             </label>
		?>	
<!--				<label for="fecha">
					<span  style="display:block">Rango de fechas: </span>
					<input type="date" name="fi" style="display:inline-block" style="display:inline-block"/>
					<span> - </span>
					<input type="date" name="ff" style="display:inline-block" style="display:inline-block"/>
				</label>
-->				<label for="Order">
					<span style="display:block">Ordenado por: </span>
					<select name="orderby" style="display:inline-block">
						<option value="fecha_in" <?php if ($data_args['by'] == 'fecha_in') echo 'selected';?>>Fecha</option>
						<!--<option value="code" <?php if ($data_args['by'] == 'festival') echo 'selected';?>>Evento</option>-->
					</select>
					 <select name="orderact" style="display:inline-block">
						<option value="DESC" <?php if ($data_args['act'] == 'DESC') echo 'selected';?>>Descente</option>
						<option value="ASC" <?php if ($data_args['act'] == 'ASC') echo 'selected';?>>Ascendente</option>
					</select>
				</label>
				<input type="submit" name="filtrar" value="Filtrar"class="button ver" />
			</form>
		</div>
		<?php
	}
}

if( !function_exists('obtener_reserva')) {
	/*
	BUSQUEDA DE RESERVAS
	*/
	function obtener_reserva($data_args){
		global $wpdb;
	//	$defaults = array( 
	//		'usuario'  => 'LIKE \'%%\'',
	//		'festival' => 'LIKE \'%%\'',
	//		'servicio' => 'LIKE \'%%\'',
	//		'hotel'    => 'LIKE \'%%\'',
	//		'order'    => 'LIKE \'%%\'',
	//		'fi' 	   => 0,
	//		'ff' 	   => date('Y-m-d H:i:s'),
	//		'by' 	   => 'fecha_in' ,
	//		'act' 	   => 'DESC',
	//		'select'   => '*'
	//	);
	
	//	$data_args = wp_parse_args( $data_args, $defaults );
	
		if ( $data_args['fi'] != '' && $data_args['ff'] = '') $data['fecha'] = "(fecha_in BETWEEN '".str_replace('T',' ',$data_args['fit'])."' AND '".str_replace('T',' ',$data_args['ff'])."')";
		
		if ($data_args['select'] == '') $data_args['select'] = '*';
		if ($data_args['act'] == '') $data_args['act'] = 'DESC';
		if ($data_args['by'] == '') $data_args['by'] = 'fecha_in';
		
		if ($data_args['usuario'] != ''){
			$data['usuario'] = 'usuario = '.$data_args['usuario'];
		}
		if ($data_args['codigo'] != ''){
			$data['codigo'] = 'code = \''.$data_args['codigo'].'\'';
		}
		if ($data_args['order'] != ''){
			$data['order'] = 'pedido = '.$data_args['order'];
		}
		if ($data_args['hotel'] != ''){
			$data['hotel'] = 'hotel = '.$data_args['hotel'];
		}
		if ($data_args['festival'] != ''){
			if ( is_array( $data_args['festival'] ) ){
				$data['festival'] = 'festival IN ('.implode(',',$data_args['festival']).')';
			}else{
				$data['festival'] = 'festival = '.$data_args['festival'];
			}
		}
		if ($data_args['servicio'] != ''){
			$data['servicio'] = 'servicio = '.$data_args['servicio'];
		}
		
		$consulta2 = implode(' AND ',$data);
		
		if ($consulta2 != '') $consulta2 = "WHERE ".$consulta2;
		
		$consulta = "SELECT ".$data_args['select']." FROM wp_reservas ".$consulta2." ORDER BY `".$data_args['by']."` ".$data_args['act'];
		$reservas = $wpdb->get_results($consulta);
	
		$wpdb->show_errors();
		
		return $reservas;
	}
}

if( !function_exists('cancelar_reserva')) {
	/*
	Eliminamos la reserva en wp_reservas
	*/
	function cancelar_reserva($data){
		global $wpdb, $wpdb_ws;
//		if (!wc_get_order( $data )){
			
//			$wpdb->query("DELETE FROM wp_reservas WHERE code = '".$data."'");
			$wpdb->delete('wp_reservas', array( 'code' => $data ) );
			$wpdb->show_errors();
			require_once(ABSPATH . 'ws_conect.php');
			if ($data <> '999999999' && $data <> '888888888' && $wpdb_ws -> get_results( 'SELECT * FROM ws WHERE codigo = \''.$data.'\'' ) ){
				update_ws(array( 'codigo' => $_GET['codigo'] ), array( 'reserv' => 0));
			}
//		}else{
//			$wpdb->delete( 'wp_reservas', array( 'pedido' => $data) );
//			$wpdb->show_errors();
//		}
	}
}

if( !function_exists('festibox_data')) {
	function festibox_data($data) {
	
		//No realza comprobaciones cruzadas.
		$defaults = array(
			'buscar' 	  => array('festival', 'concierto', 'espectaculo', 'alojamiento' , 'product'),
			'festival'    => '',
			'alojamiento' => '',
			'producto' 	  => '',
			'estado' 	  => array( 'publish')
	//		'estado' 	  => array( 'publish', 'pending', 'draft', 'auto-draft', 'future', 'private',  'inherit', 'trash')
		);
	
		if ( in_array( 'servicios', $data['buscar'] ) && !in_array( 'private', $data['estado'] ) ) $data['estado'][] = 'private';
		if ($data['buscar'] == array('servicios')) 
		$args = array( 
			'post_type' 	 => $data['estado'],
			'posts_per_page' => -1,
			'post_status' 	 => $data['estado'],
			'order' 		 => 'ASC',
			'orderby' 		 => 'title'
			);
	
		$posts = get_posts( $args  );	
	
		foreach ($posts as $post){
			$ok_f = $ok_a = $ok_p = 1;
			if ( $data['producto'] != '' || $data['festival'] != '' || $data['alojamiento'] != '' ){
				if ( get_post_meta( $post -> ID , 'f_productos' , true ) && $data['producto'] != '' ){
	
					foreach ( $data['producto'] as $producto ){
						if( in_array( $producto , explode( ',',get_post_meta( $post -> ID , '_productos' , true ) ) ) ){
							$ok_p = 0;
						}
					}
				}
				
				if ( get_post_meta( $post -> ID , 'f_festivales' , true )  && $data['festival'] != '' ){
				
					foreach ( $data['festival'] as $festival ){
						if( in_array( $festival , explode( ',',get_post_meta( $post -> ID , '_festivales' , true ) ) ) ){
							$ok_f = 0;
						}
					}
				}
	
				if ( get_post_meta( $post -> ID , 'f_alojamientos' , true )  && $data['alojamiento'] != '' ){
	
					foreach ( $data['alojamiento'] as $alojamiento ){
						if( in_array( $alojamiento , explode( ',', get_post_meta( $post -> ID , '_alojamientos' , true ) ) ) ){
							$ok_a = 0;
						}
					}
				}
	
				if ( $ok_a == 1 && $ok_f == 1 && $ok_p == 1 ) {
					$salida[] = $post -> ID;
	
				}
			}else{
				$salida[] = $post -> ID;
			}
		}
	
		if ( count($salida) == 1 ){
			return $salida[0];
		}else{
			return $salida;
		}
	}
}
