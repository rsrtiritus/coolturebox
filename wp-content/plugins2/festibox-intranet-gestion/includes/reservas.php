<?php

function gestion_hacer_reservas_short(){
	global $user_ID;
	if ( is_user_logged_in() && ( current_user_can( 'coolturebox' )|| current_user_can( 'administrator' ))) 
	{
	
		$filtro = array();
		if ( isset( $_POST['usuario'] ) && $_POST['usuario'] != '' ) $filtro['usuario'] = $_POST['usuario'];
		if ( isset( $_POST['codigo'] ) && $_POST['codigo'] != '' ) $filtro['codigo'] = $_POST['codigo'];
		if ( isset( $_POST['festival'] ) && $_POST['festival'] != '' ) $filtro['festival'] = $_POST['festival'];
		if ( isset( $_POST['hotel'] ) && $_POST['hotel'] != '' ) $filtro['hotel'] = $_POST['hotel'];
		if ( isset( $_POST['order'] ) && $_POST['order'] != '' ) $filtro['order'] = $_POST['order'];
		if ( isset( $_POST['fi'] ) && $_POST['fi'] != '' ) $filtro['fi'] = $_POST['fi'];
		if ( isset( $_POST['ff'] ) && $_POST['ff'] != '' ) $filtro['ff'] = $_POST['ff'];
		if ( isset( $_POST['orderby'] ) &&  isset( $_POST['orderact'] ) )
		{
			$filtro['by'] = $_POST['orderby'];
			$filtro['act'] = $_POST['orderact'];
		}
		if ( current_user_can( 'colaborador' ) )
		{
			$user_ID = get_current_user_id ();
			if ( get_the_author_meta( '_referencias', $user_ID ) ){
				$filtro['festival'] = get_the_author_meta( '_referencias', $user_ID );
			}else{
				$filtro['festival'] = 1;
			}
			if ( isset( $_GET['fv'] ) ){
				if ( in_array( $_GET['fv'], $filtro['festival'] ) )
					$filtro['festival'] = $_GET['fv'];
			}
		}
		if ( current_user_can( 'customer' ) ) $filtro['usuario'] = get_current_user_id ();
		//Las funciones están ubicadas en reservas
		$reservas = obtener_reserva( $filtro );
		?>
		<div class="woocommerce reservas_historico_cliente">
            <h2>Resumen de Reservas</h2>
			<?php
			reservas_filtro($filtro);
            ?>
            <table class="shop_table shop_table_responsive reservas_clientes">
                <thead><tr>
                    <th class="reserva_imagen"></th>
                    <th class="reserva_codigo">Código Reserva</th>
                    <?php if ( !current_user_can( 'customer' ) ) echo '<th class="reserva_usuario">Usuario</th>';?>
                    <?php if ( current_user_can( 'festibox' ) || current_user_can( 'administrator' ) ) echo '<th class="reserva_usuario">Teléfono</th><th class="reserva_usuario">Email</th>';?>
                    <th class="reserva_festival">Festival</th>
                    <th class="reserva_hotel">Fecha</th>
                    <th></th>
                </tr></thead>
                <tbody>
                <?php
                foreach($reservas as $reserva){
                    if ( $reserva->usuario != ''){ 
						$user_info = get_userdata($reserva->usuario);
						$name = $user_info->first_name.' '.$user_info->last_name;
						$tlf = get_user_meta( $user_info->ID, 'billing_phone', true );
						$email = $user_info->user_email;
					}else{
						$name = $reserva->nombre.' '.$reserva->apellidos;
						$tlf = $reserva->tlf;
						$email = $reserva->email;
					}
//					if ( !isset( $filtro['usuario'] ) || ( $filtro['usuario'] != '' && strpos( $filtro['usuario'], $name ) !== 'FALSE' ) ){
					?>
                    <tr>
                        <td class="reserva_imagen">
                            <a href="<?php echo get_permalink($reserva->servicio);?>" title="<?php echo get_the_title($reserva->servicio);?>" alt="<?php echo get_the_title($reserva->servicio);?>">
                                <?php get_the_post_thumbnail( $reserva->servicio, 'thumbnail' );?>
                            </a>
                        </td>
                        <td class="reserva_codigo"><?php echo $reserva->code;?></td>
                        <?php if ( !current_user_can( 'customer' ) ) echo '<td class="reserva_usuario">'.$name.'</td>';?>
                   		<?php if ( current_user_can( 'festibox' ) || current_user_can( 'administrator' ) ) echo '<td class="reserva_usuario">'.$tlf.'</td><td class="reserva_usuario">'.$email.'</td>';?>
                        <td class="reserva_festival">
                            <a href="<?php echo get_permalink($reserva->festival);?>" title="<?php echo get_the_title($reserva->festival);?>" alt="<?php echo get_the_title($reserva->festival);?>">
								<?php echo get_the_title($reserva->festival);?>
                            </a>
                        </td>
                        <td class="reserva_fecha"><?php echo $reserva -> fecha_in;?></td>
                        <td>
                        	<?php //Se ha decidido que sea solo el administrador.?>
                            <?php 
//							$fecha = obtener_fechas_festival( $reserva->festival );
//							$cancelar = 0;
//							// En la  versión 2 basta con poner solo la primeera fecha.
//							if ( $fecha[0] != 'Aún por confirmar' ){
//								list( $dia,$mes,$ano ) = explode( '/', $fecha[0] );
//								$ini = new DateTime();
//								$fin = new DateTime($ano.'-'.$mes.'-'.$dia);
//								$ff = new DateTime($reserva -> fecha_in);
//								$interval = $ini->diff($fin);
//								$interval2 = $ff->diff($fin);
//								if ( $interval->format('%R%a') > 7 &&  $interval->format('%R%a') > 0 &&  $interval->format('%R%a') < 365 ) $cancelar = 1;
//							}

//							if ( !current_user_can( 'colaborador' ) && $cancelar == 1){
							if ( current_user_can( 'festibox' ) || current_user_can( 'administrator' ) ){ ?>
                            	<a href="<?php echo site_url('/cancelar-reserva/');?>?codigo=<?php echo $reserva->code;?>" class="button cancelar">Cancelar</a>
                            <?php }?>
                            <?php if ($reserva->order != 0 && current_user_can( 'customer' )){ ?>
                                <a href="/wp-content/plugins/festibox-eticketing/pdf.php?order=<?php echo $reserva->order;?>&fv=<?php echo $reserva->festival;?>" class="button descarvar">Descargar</a>
                            <?php }?>
                        </td>
                    </tr>
                	<?php 
//					}
				}?>
                </tbody>
            </table>
        </div>
    <?php
	}
}
