<?php 
/*
Plugin Name: experiencias en campos personalizados
Plugin URI: http://www.emenia.es/plugin-subir-experiencia-campo-personalizado-wordpress
*/

function custom_experiencias_type() {

// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Experiencias', 'Post Type General Name', 'festibox' ),
		'singular_name'       => _x( 'Experiencia', 'Post Type Singular Name', 'festibox' ),
		'menu_name'           => __( 'Experiencias', 'festibox' ),
		'parent_item_colon'   => __( 'Experiencia Padre', 'festibox' ),
		'all_items'           => __( 'Todos los experiencias', 'festibox' ),
		'view_item'           => __( 'Ver experiencia', 'festibox' ),
		'add_new_item'        => __( 'Añadir nuevo experiencia', 'festibox' ),
		'add_new'             => __( 'Añadir nuevo', 'festibox' ),
		'edit_item'           => __( 'Editar experiencia', 'festibox' ),
		'update_item'         => __( 'Actualizar experiencia', 'festibox' ),
		'search_items'        => __( 'Buscar experiencia', 'festibox' ),
		'not_found'           => __( 'No encontrado', 'festibox' ),
		'not_found_in_trash'  => __( 'No encontrado en la papelera', 'festibox' ),
	);

// Otras opciones para el tipo de entrada personalizada

	$args = array(
		'label'               => $labels,
		'description'         => __( 'Experiencias Festibox', 'festibox' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'page_layout', ),
		// Puedes asociar este CPT con una taxonomía por defecto o una taxonomía personalizada
		'taxonomies'          => array( 'caja','cuando','edad','provincia'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);

	// Registro del tipo de entrada personalizada (Customs Post Type)
	register_post_type( 'experiencia', $args );
}

add_action( 'init', 'custom_experiencias_type', 0 );

//hook para crear el meta box
add_action( 'add_meta_boxes', 'em_mtbx_experiencia' );

function em_mtbx_experiencia() {

	// Creamos el meta box personalizado
	add_meta_box( 
                'em-experiencia', // atributo ID
                'Datos de la expereincia', // Título
                'em_mtbx_experiencia_function', // Función que muestra el HTML que aparecerá en la pantalla
                'experiencia', // Tipo de entrada. Puede ser 'post', 'page', 'link', o 'custom_post_type'
                'normal', // Parte de la pantalla donde aparecerá. Puede ser 'normal', 'advanced', o 'side'
                'default' // Prioridad u orden en el que aparecerá. Puede ser 'high', 'core', 'default' o 'low'
                );
	
}

function em_mtbx_experiencia_function( $post ) {
    
    echo '<strong>experiencia</strong>';    
    //si ya existe esa imagen obtemos su valor
    $f_inicio = get_post_meta( $post->ID, 'f_inicio', true );
    $f_fin = get_post_meta( $post->ID, 'f_fin', true );
    $f_tipo = get_post_meta( $post->ID, 'f_tipo', true );
	$f_productos = get_post_meta( $post->ID, 'f_productos', true );
    ?>
    <h4><?php _e("Fecha Inicio"); ?></h4>
    <input class="em_mtbx_img"name="f_inicio" id="f_inicio" type="text" value="<?php if($f_inicio && $f_inicio != '') echo $f_inicio; ?>">
    <h4><?php _e("Fecha Fin"); ?></h4>
    <input class="em_mtbx_img"name="f_fin" id="f_fin" type="text" value="<?php if($f_fin && $f_fin != '') echo $f_fin; ?>">
    <h4><?php _e("Tipo de Experiencia"); ?></h4>
    <input class="em_mtbx_img"name="f_tipo" id="f_tipo" type="text" value="<?php if($f_tipo && $f_tipo != '') echo $f_tipo; ?>">
    <h4><?php _e("Caja"); ?></h4>
    <select multiple name="f_productos_experiencia[]">
    <?php  listado('product', $f_productos);?>
    </select>
    <div class="clear"></div>
          
	<?php       
}

//para cargar el archivo experiencia-meta-box.js sólo al crear y editar una entrada
add_action('admin_print_scripts-post.php', 'em_mtbx_experiencia_admin_scripts');
add_action('admin_print_scripts-post-new.php', 'em_mtbx_experiencia_admin_scripts');

function em_mtbx_experiencia_admin_scripts() {
	wp_enqueue_script( 'em-image-upload', plugins_url( '/experiencia-meta-box/experiencia-meta-box.js' ), array( 'jquery','media-upload','thickbox' ) );
}

//usando thickbox para subir la imagen al crear o editar una entrada
add_action('admin_print_styles-post.php', 'em_mtbx_experiencia_admin_styles');
add_action('admin_print_styles-post-new.php', 'em_mtbx_experiencia_admin_styles');

function em_mtbx_experiencia_admin_styles() {
	wp_enqueue_style( 'thickbox' );
}

//Para grabar los campos. Añadir uno nuevo por cada campo existente, en este caso tres.
add_action( 'save_post', 'em_mtbx_experiencia_save_meta' );

function em_mtbx_experiencia_save_meta( $post_id ) {

	if ( isset( $_POST['f_productos_experiencia'] ) ) {
		foreach ($_POST['f_productos_experiencia'] as $f_producto){$f_productos .= ",".$f_producto;}
		update_post_meta( $post_id, 'f_productos', $f_productos.","  );

	}
	if ( isset( $_POST['f_inicio'] ) ) {
	
		update_post_meta( $post_id, 'f_inicio', $_POST['f_inicio']  );

	}
	if ( isset( $_POST['f_fin'] ) ) {
	
		update_post_meta( $post_id, 'f_fin', $_POST['f_fin']  );

	}
	if ( isset( $_POST['f_tipo'] ) ) {
	
		update_post_meta( $post_id, 'f_tipo', $_POST['f_tipo']  );

	}
}

function add_custom_meta_box2() {
	add_meta_box(
				'custom_meta_box2', // id
				'Fotos, ediciones y logos.', // título
				'show_custom_meta_box', // función a la que llamamos
				'experiencia', // sólo para páginas
				'normal', // contexto
				'high'); // prioridad
}
add_action('add_meta_boxes', 'add_custom_meta_box2');
