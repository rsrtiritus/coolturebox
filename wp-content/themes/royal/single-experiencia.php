<?php 
	get_header();
?>

<?php 
	$today = strtotime("now");
    $f_inicio = get_post_meta( $post->ID, 'f_inicio', true );
    $f_fin = get_post_meta( $post->ID, 'f_fin', true );
    $f_tipo = get_post_meta( $post->ID, 'f_tipo', true );
	list($f_dia,$f_mes,$f_ano) = explode('/',$f_fin);
	list($i_dia,$i_mes,$i_ano) = explode('/',$f_inicio);
	$f_falta_s = mktime(0,0,0,$f_mes,$f_dia,$f_ano);
	$f_inicio_s = mktime(0,0,0,$i_mes,$i_dia,$i_ano);
	$segundos_diferencia = $f_falta_s - $today;
	$dias_diferencia = $segundos_diferencia / (60 * 60 * 24);
	$dias_diferencia = abs($dias_diferencia);
	$dias_diferencia = floor($dias_diferencia);

    extract(etheme_get_page_sidebar());
    $blog_slider = etheme_get_option('blog_slider');
    $postspage_id = get_option('page_for_posts');
    $post_format = get_post_format();
    
    $post_content = $post->post_content;
    preg_match('/\[gallery.*ids=.(.*).\]/', $post_content, $ids);
    if(!empty($ids)) {
	    $attach_ids = explode(",", $ids[1]);
	    $content =  str_replace($ids[0], "", $post_content);
	    $filtered_content = apply_filters( 'the_content', $content);
    }
    
    $slider_id = rand(100,10000);
?>


<?php if ($page_heading != 'disable' && ($page_slider == 'no_slider' || $page_slider == '')): ?>
	<div class="page-heading bc-type-<?php echo $breadcrumb_type; ?>">
		<div class="container">
			<div class="row">
				<div class="col-md-12 a-center">
				<h1 class="title"><span><?php echo get_the_title($postspage_id); ?></span></h1>
					<?php etheme_breadcrumbs(); ?>
				</div>
			</div>
		</div>
	</div>
<?php endif ?>

<?php if($page_slider != 'no_slider' && $page_slider != ''): ?>
	
	<?php echo do_shortcode('[rev_slider_vc alias="'.$page_slider.'"]'); ?>

<?php endif; ?>

<div class="container">
	<div class="page-content sidebar-position-<?php echo $position; ?> responsive-sidebar-<?php echo $responsive; ?>">
		<div class="row">
			<?php if($position == 'left' || ($responsive == 'top' && $position == 'right')): ?>
				<div class="<?php echo $sidebar_span; ?> sidebar sidebar-left">
					<?php etheme_get_sidebar($sidebarname); ?>
				</div>
			<?php endif; ?>

			<div class="content <?php echo $content_span; ?>">
				<?php if(have_posts()): while(have_posts()) : the_post(); ?>
				
					<article <?php post_class('blog-post post-single'); ?> id="post-<?php the_ID(); ?>" >
                        <div class="fest_img">
                            <h2 style="font-size: -webkit-xxx-large;line-height: initial;"><?php echo get_the_title($postspage_id); ?></h2>
                            <div>
                                <h3><?php echo get_post_meta($post->ID, 'f_comunidad', true); ?></h3>
                            </div>
                            </div>
                            <div class="content-article">
                                <?php the_content(); ?>
                            </div>
                            <div>
								<?php if ($dias_diferencia > 0 && $f_inicio_s > $today){
                                    echo '<h4>LA INSCRIPCIÓN SE ABRIRÁ EL: '.$f_inicio.'</h4>';
                                }elseif ($dias_diferencia > 0 && $f_inicio_s <= $today){
                                    echo '<h4>LA INSCRIPCIÓN SE CERRARÁ EL: '.$f_fin.'</h4>';
                                }else{
                                    echo '<h4>LA INSCRIPCIÓN SE HA CERRADO</h4>';
                                }?>
                            </div>
                            <div class="wp-picture">
								<?php
								if (get_post_meta($post->ID, 'youtube_id', true)){
								?>
                                	<iframe src="http://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'youtube_id', true); ?>" width="588" height="331" frameborder="0" allowfullscreen></iframe>
                                <?php
								}else{
									the_post_thumbnail('large');
								?>
                                    <div class="zoom">
                                        <div class="btn_group">
                                            <a href="<?php echo etheme_get_image(); ?>" class="btn btn-black xmedium-btn" rel="pphoto"><span><?php _e('Expandir', ETHEME_DOMAIN); ?></span></a>
                                        </div>
                                        <i class="bg"></i>
                                    </div>
                                <?php }?>
                            </div>
							<?php
                            $attachID = (get_post_meta( $post->ID, 'custom_imagenrepetible', true ));
                            foreach($attachID as $item){
								$imagen = wp_get_attachment_image_src($item, 'thumbnail'); // Obtenemos la imagen "full". En vez de full podemos poner otro que dispongamos en nuestro tema
								$imagen2 = wp_get_attachment_image_src($item, 'full'); // Obtenemos la imagen "full". En vez de full podemos poner otro que dispongamos en nuestro tema
                            ?>
								<div class="wp-picture" style="display:inline-block">
                                    <img src="<?php echo $imagen[0];?>" width="<?php echo $imagen[1];?>" height="<?php echo $imagen[2];?>" class="attachment-post-thumbnail wp-post-image" alt="<?php echo get_post_meta($item, '_wp_attachment_image_alt', true);?>"/>
									<div class="zoom">
										<div class="btn_group">
											<a href="<?php echo $imagen2[0];?>" class="btn btn-black xmedium-btn" rel="pphoto"><span><?php echo _e('Expandir', ETHEME_DOMAIN);?>'</span></a>
										</div>
										<i class="bg"></i>
									</div>
								</div>
                            <?php }?>
                        </div>
                        <div class="fes_detalles">
                            <div>
                                <h3><span>FALTAN</span></h3>
								<?php if ($dias_diferencia > 0){
                                    echo $dias_diferencia." D&iacute;as";
                                }else{
                                    echo "EXPIRADO";
                                }?>
                            </div>
                            <div>
                                <h3><span>APUNTARSE</span></h3>
								<?php if ($dias_diferencia > 0 && $f_inicio_s <= $today){
									echo '<div style="text-align: center;padding:20px">';
									echo '<a class="btn active big filled" href="'.get_site_url().'/inscribirse/?exp='.$post->ID.'"><span>Inscribirse</span></a>';
									echo '</div>';
                                }else{
                                    echo "CERRADO";
                                }?>
                            </div>
                            <div>
                                <h3><span>CAJAS</span></h3>
                                <ul class="cajas_list">
									<?php
                                    $f_productos = get_post_meta( $post->ID, 'f_productos', true );
                                    $productos = explode(',',$f_productos);
                                    foreach ($productos as $producto){
                                        echo  '<li><a href="'.get_permalink($producto).'" title="'.str_replace(' ','_',get_the_title($producto)).'" alt="'.str_replace(' ','_',get_the_title($producto)).'">'.get_the_post_thumbnail($producto,array(75,75)).'</a></li>';
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
					</article>


				<?php endwhile; else: ?>

					<h1><?php _e('No posts were found!', ETHEME_DOMAIN) ?></h1>

				<?php endif; ?>

				<?php comments_template('', true); ?>

			</div>

			<?php if($position == 'right' || ($responsive == 'bottom' && $position == 'left')): ?>
				<div class="<?php echo $sidebar_span; ?> sidebar sidebar-right">
					<?php etheme_get_sidebar($sidebarname); ?>
				</div>
			<?php endif; ?>

		</div>

	</div>
</div>
	
<?php
	get_footer();
?>