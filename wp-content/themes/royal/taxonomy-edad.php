<?php 
	query_posts($query_string .'&posts_per_page=-1');
	get_header();
	$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	$festivales = array();
?>

<div class="page-heading bc-type-<?php etheme_option('breadcrumb_type'); ?>">
	<div class="container">
		<div class="row">
            <div class="col-md-12 a-center">
            	<h1 class="title"><span>Festivales por edad</span></h1>
                <div class="breadcrumbs">
                	<div id="breadcrumb">
                    	<a href="http://festibox.com">Inicio</a>
                        <span class="delimeter">/</span>
                        <span class="current"><?php  echo $term->name; ?></span>
                    </div>
                    <a class="back-history" href="javascript: history.go(-1)">Volver a la página anterior</a>
                </div>
            </div>
		</div>
	</div>
</div>

<div class="container">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<?php 
                if(have_posts()){ while(have_posts()) : the_post();
                    $festivales[] = $post -> ID;	
                endwhile;
                    $festival = implode(",", $festivales);
                    echo do_shortcode('[vc_row][vc_column width="1/1"][vc_posts_grid grid_columns_count="3" pagination="show" grid_content="teaser" posted_block="hide" hover_mask="hide" grid_layout="thumbnail_title" grid_template="grid" orderby="title" order="ASC" grid_posttypes="festival,evento,concierto" grid_teasers_count="12" posts_in="'.$festival.'"][/vc_column][/vc_row]');
                 }else{ ?>

                    <h3><?php _e('Se han encontrado festivales.', ETHEME_DOMAIN) ?></h3>

                <?php } ?>
				<?php //   etheme_pagination($my_query, $paged);?>
			</div>
		</div>


	</div>
</div>

	
<?php
	get_footer();
?>