<?php
/**
 * Empty cart page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

wc_print_notices();

?>

<?php do_action('woocommerce_cart_is_empty'); ?>

<div class="empty-cart-block">
	<i class="icon-shopping-cart"></i>
	
	<?php etheme_option('empty_cart_content'); ?>	
	<p><a class="btn active big filled" href="http://www.coolturebox.com/"; ?><span><?php _e('Volver a la Tienda', ETHEME_DOMAIN) ?></span></a></p>
	
</div>