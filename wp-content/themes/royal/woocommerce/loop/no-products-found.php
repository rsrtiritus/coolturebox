<?php
/**
 * Displayed when no products are found matching the current query.
 *
 * Override this template by copying it to yourtheme/woocommerce/loop/no-products-found.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<div class="empty-category-block">
	<h2>No se ha encontrado ninguna Festibox.</h2>
	<h2>Prueba otra vez.</h2>
	<p><a class="btn medium" href="<?php echo get_permalink(woocommerce_get_page_id('shop')); ?>"><span><?php _e('Volver a la Tienda', ETHEME_DOMAIN) ?></span></a></p>
</div>
