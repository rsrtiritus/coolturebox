<?php
	$caja_query = '';
//	$caja_query = (get_query_var('cj')) ? get_query_var('cj') : 1;
	if (isset ($_GET['cj'])){
		$caja_query = $_GET['cj'];
	}else{
		$caja_query = 1;
	}
	get_header();
    $fechas = obtener_fechas_festival_txt($post->ID);
	$f_falta = obtener_fechas_festival($post->ID);
?>

<?php
	$today = strtotime("now");
	$post_date = strtotime(get_the_date('Y-m-d'));
	$dia = 20;

	extract(etheme_get_page_sidebar());
    $blog_slider = etheme_get_option('blog_slider');
    $postspage_id = get_option('page_for_posts');
    $post_format = get_post_format();

    $post_content = $post->post_content;
    preg_match('/\[gallery.*ids=.(.*).\]/', $post_content, $ids);
    if(!empty($ids)) {
	    $attach_ids = explode(",", $ids[1]);
	    $content =  str_replace($ids[0], "", $post_content);
	    $filtered_content = apply_filters( 'the_content', $content);
    }

    $slider_id = rand(100,10000);
?>


<?php if ($page_heading != 'disable' && ($page_slider == 'no_slider' || $page_slider == '')): ?>
	<div class="page-heading bc-type-<?php echo $breadcrumb_type; ?>">
		<div class="container">
			<div class="row">
				<div class="col-md-12 a-center">
                    <h1 class="title"><span><?php echo get_the_title($postspage_id); //if ($caja_query <> 1) {echo ' <span style="font-size:50%">'.get_the_title($caja_query).'</span>';}?></span></h1>
					<?php
					if ($caja_query == 1){
 						etheme_breadcrumbs();
					}else{?>
                        <div class="breadcrumbs">
                            <div id="breadcrumb">
                                <a href="http://festibox.com">Inicio</a>
                                <span class="delimeter">/</span>
                                <a href="http://festibox.com/cajas/">Cajas</a>
                                <span class="delimeter">/</span>
                                <a href="<?php  echo get_permalink($caja_query); ?>"><?php  echo get_the_title($caja_query); ?></a>
                                <span class="delimeter">/</span>
                                <span class="current"><?php  echo get_the_title(); ?></span>
                            </div>
                            <a class="back-history" href="<?php  echo get_permalink($caja_query); ?>">Volver a la caja</a>
                        </div>
                    <?php }?>
				</div>
			</div>
		</div>
	</div>
<?php endif ?>

<?php if($page_slider != 'no_slider' && $page_slider != ''): ?>

	<?php echo do_shortcode('[rev_slider_vc alias="'.$page_slider.'"]'); ?>

<?php endif; ?>

<div class="container">
	<div class="page-content sidebar-position-<?php echo $position; ?> responsive-sidebar-<?php echo $responsive; ?>">
		<div class="row">
			<?php if($position == 'left' || ($responsive == 'top' && $position == 'right')): ?>
				<div class="<?php echo $sidebar_span; ?> sidebar sidebar-left">
					<?php etheme_get_sidebar($sidebarname); ?>
				</div>
			<?php endif; ?>

			<div class="content <?php echo $content_span; ?>">
				<?php if(have_posts()): while(have_posts()) : the_post(); ?>

					<article <?php post_class('blog-post post-single'); ?> id="post-<?php the_ID(); ?>" >
                        <div class="fest_img">
<!--                        	<div class="entry-post-nav group">
                                <?php $prev_post = get_previous_post(); ?>

                                <?php if ( !empty( $prev_post ) ) : ?>
                                    <div class="post-prev">
                                        <h4><a href="<?php echo get_permalink( $prev_post->ID ); ?>"><< <?php echo $prev_post->post_title; ?></a></h4>
                                    </div>
                                <?php endif; ?>

                                <?php $next_post = get_next_post(); ?>

                                <?php if ( !empty( $next_post ) ) : ?>
                                    <div class="post-next">
                                       <h4><a href="<?php echo get_permalink( $next_post->ID ); ?>"><?php echo $next_post->post_title; ?> >></a></h4>
                                    </div>
                                <?php endif; ?>
                            </div>-->
                            <div>
                                <h2 class="titulo_evento"><?php echo get_the_title($postspage_id); ?></h2>






    <!--                        <div>
                                    <?php // echo get_post_meta($post->ID, 'f_web', true); ?>
                                </div>
    -->                     </div>
   						</div>
                        <div class="fest_img">
                            <div class="content-article">
                                    <?php the_content(); ?>
                            </div>
                            <div>
                                <?php
								if($caja_query <> 1)
								{
									$servicios = obteneter_servicios($caja_query, $post->ID, '');
									$data = array();
									echo '<b><h2 style="padding-bottom:10px; border-top: 1px solid rgba(0, 0, 2, 0.16); padding-top:5px">Con este pack podrás disfrutar de la siguiente experiencia:</b></h2>';
									foreach($servicios as $servicio){
										if (!in_array($servicio['entrada'].'//'.$servicio['nentrada'].'//'.$servicio['noches'].'//'.$servicio['otro'], $data)){
											$data[] = $servicio['entrada'].'//'.$servicio['nentrada'].'//'.$servicio['noches'].'//'.$servicio['otro'];											
											$servicio_texto = servicio_texto($servicio);
											echo '<span>'.$servicio_texto.'</span>';
											echo '<p style="padding-top:12px; font-size:12px; color:#757575">*Si deseas ampliar o modificar esta experiencia, ponte en contacto con nosotros.</p>';
										}
									}
								}
								elseif (isset($_GET['codigo'])){


									$caja_query = $codigo2['productos'];
									$url = '?codigo='.$_GET['codigo'];
									if (isset($_GET['fv']) || $_GET['fv'] <> '') $url .= '&fv='.$_GET['fv'];
									if (isset($_GET['sv']) || $_GET['sv'] <> '') $url .= '&sv='.$_GET['sv'];
									if (isset($_GET['ht']) || $_GET['ht'] <> '') $url .= '&ht='.$_GET['ht'];
									$codigo2 = comprobar_code($_GET['codigo']);
//									echo $codigo2['productos'];
									$servicios = obteneter_servicios($codigo2['productos'], $post->ID, '');
									echo '<h4>Con esta caja '.get_the_title($codigo2['productos']).' puedes disfrutar de:</h4>';
									echo '<form method="GET" action="'.get_site_url().'/reservas/" style="text-align: center;  padding-bottom: 6px;">';
									echo '<select name="sv" style="margin:6px;display:inline-block;padding-right:30px;width: 99%;">';
									$data = array();
									foreach($servicios as $servicio){
										if (!in_array($servicio['entrada'].'//'.$servicio['nentrada'].'//'.$servicio['noches'].'//'.$servicio['otro'], $data)){
											$data[] = $servicio['entrada'].'//'.$servicio['nentrada'].'//'.$servicio['noches'].'//'.$servicio['otro'];
											$servicio_texto = servicio_texto($servicio);
											echo '<option value="'.$servicio['id'].'">'.$servicio_texto.'</option>';
										}
									}
									echo '</select>';
									echo '<input type="hidden" name="fv" value="'.$post->ID.'"/>';
									echo '<input type="hidden" name="codigo" value="'.$_GET['codigo'].'"/>';
									echo '<input type="submit" value="seleccionar">';
									echo '<input type="submit" value="volver" onclick="history.back()" style="margin-left:6px">';
									echo '</form>';
								}?>
                            </div>
                            <div class="wp-picture">
								<?php
								if (get_post_meta($post->ID, 'youtube_id', true)){
								?>
                                	<iframe src="http://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'youtube_id', true); ?>" width="588" height="331" frameborder="0" allowfullscreen></iframe>
                                <?php
								}else{
									the_post_thumbnail('large');
								?>
                                    <div class="zoom">
                                        <div class="btn_group">
                                            <a href="<?php echo etheme_get_image(); ?>" class="btn btn-black xmedium-btn" rel="pphoto"><span><?php _e('Expandir', ETHEME_DOMAIN); ?></span></a>
                                        </div>
                                        <i class="bg"></i>
                                    </div>
                                <?php }?>
                            </div>
							<?php
                            $attachID = (get_post_meta( $post->ID, 'custom_imagenrepetible', true ));
                            foreach($attachID as $item){
								$imagen = wp_get_attachment_image_src($item, 'thumbnail'); // Obtenemos la imagen "full". En vez de full podemos poner otro que dispongamos en nuestro tema
								$imagen2 = wp_get_attachment_image_src($item, 'full'); // Obtenemos la imagen "full". En vez de full podemos poner otro que dispongamos en nuestro tema
                            ?>
								<div class="wp-picture peq" style="display:inline-block">
                                    <img src="<?php echo $imagen[0];?>" width="<?php echo $imagen[1];?>" height="<?php echo $imagen[2];?>" class="attachment-post-thumbnail wp-post-image" alt="<?php echo get_post_meta($item, '_wp_attachment_image_alt', true);?>"/>
									<div class="zoom">
										<div class="btn_group">
											<a href="<?php echo $imagen2[0];?>" class="btn btn-black xmedium-btn" rel="pphoto"><span><?php echo _e('Expandir', ETHEME_DOMAIN);?>'</span></a>
										</div>
										<i class="bg"></i>
									</div>
								</div>
                            <?php }?>
                            <?php if (!isset($_GET['codigo'])){?>
								<?php if ($caja_query == 1){?>
                                    <div>
                                        <?php
                                        $cajas = obteneter_caja($post->ID);
                                        $str = implode (", ", $cajas);
                                        echo do_shortcode('[etheme_products type="slider" style="default" title="Este espectáculo está en:" ids="'.$str.'"]');
                                        ?>
                                    </div>
                                <?php }else{?>
                                    <div>
                                       <!--h2 style="font-size: medium;line-height: initial;">Servicios para --> <?php //echo get_the_title($post->ID);?><!-- /h2 -->
                                       <?php
                                        //echo do_shortcode('[venta_servicios fv="'.$post->ID.'"][/venta_servicios]');
										?>
                                    </div>
                                <?php }?>
							<?php }?>
                        </div>
                        <div class="fes_detalles">
							<?php
							if ($fechas['next'] <> 'AÚN POR CONFIRMAR')
                                                        {
								list($f_dia,$f_mes,$f_ano) = explode('/',$f_falta['0']);
								$f_falta_s = mktime(0,0,0,$f_mes,$f_dia,$f_ano);
								$segundos_diferencia = $f_falta_s - $today;
								$dias_diferencia = $segundos_diferencia / (60 * 60 * 24);
								$dias_diferencia = abs($dias_diferencia);
								$dias_diferencia = floor($dias_diferencia);
							}
/*(04/11/2016)Se comenta este código del botón COMPRAR hasta aclarar si debe o no aparecer
if (!isset($_GET['codigo']) && $fechas['next'] <> 'AÚN POR CONFIRMAR' && $segundos_diferencia > 5*(60 * 60 * 24))
{
  if ($caja_query == 1)

  {
    echo '<div style="text-align: center;padding:20px">';
    echo '<a class="btn active big filled com_fest" href="'.get_site_url().'/servicios-en-venta/?fv='.$post->ID.'">	                                 <span>comprar</span></a>';
    echo '</div>';



  }
  else
  {
    echo '<div style="text-align: center;padding:20px">
    <span class="btn active big filled show-quickly" data-prodid="'.$caja_query.'" style="float: none">comprar</span>




    </div>';
  }
}
*/
?>
<!--SE COMENTA CODIGO (05/12/2016): El campo FALTAN no se mostrará en modo web -->

                            <?php
								if ($fechas['next'] <> '' && $fechas['next'] <> 'AÚN POR CONFIRMAR' && $segundos_diferencia > 0)
								{?>

                                <div>
                                    <!-- <h3><span>FALTAN</span></h3> -->
                                    <span>

                                    <?php
									if ($dias_diferencia >0)
									{
										//echo $dias_diferencia." D&iacute;as";

									}
									else
									{
										$horas_diferencia = $segundos_diferencia / (60 * 60);
										$horas_diferencia = abs($horas_diferencia);
										$horas_diferencia = floor($horas_diferencia);
										//echo $horas_diferencia." Horas";
									}
                                    ?>
                                    </span>
                                </div>
                            <?php }?>







                            <div>
                                <h3><span>CATEGORÍA</span></h3>
                                <span>
				<?php
                                	if (get_the_term_list($post->ID, 'categoria', '', ', ', ''))
					{
				    		$terms = get_the_term_list($post->ID, 'categoria', '', ', ', '');
				    		$terms = strip_tags( $terms );
                                    		echo $terms;
                                	}
					else
					{
                                    		echo get_post_meta($post->ID, 'f_categoria', true);
                                	}
                                ?>
                                </span>
                            </div>

                            <div>
                                <h3>
					<span>FECHAS DEL EVENTO</span>
				</h3>
                                <span>
					<?php echo $fechas['next'];?>

				</span>
                            </div>

                            <div>
                                <h3><span>HORARIOS EVENTO</span></h3>
                                <span>
                                <?php echo get_post_meta($post->ID, 'f_hora', true); ?>
                                </span>
                            </div>
<!--SE COMENTA EL CÓDIGO (29/11/2016:Se decide que no debe aparecer
                            <div>
                                <h3><span>IDIOMA</span></h3>
                                <span>
				<?php
                                	if (get_the_term_list($post->ID, 'idioma', '', ', ', ''))
					{
				    		$terms = get_the_term_list($post->ID, 'idioma', '', ', ', '');
				    		$terms = strip_tags( $terms );
                                    		echo $terms;
                                	}
					else
					{
                                    		echo get_post_meta($post->ID, 'f_idioma', true);
                                	}
                                ?>
                                </span>
                            </div>

-->
<!--SE COMENTA EL CÓDIGO (29/11/2016:Se decide que no debe aparecer
                            <div>
                                <h3><span>UBICACIÓN</span></h3>
                                <span>
                                <?php echo get_post_meta($post->ID, 'f_recinto', true); ?>
                                </span>
                            </div>
-->
                            <div>
                                <h3><span>EDAD</span></h3>
                                <span>
				<?php
                                	if (get_the_term_list($post->ID, 'edad', '', ', ', ''))
					{
				    		$terms = get_the_term_list($post->ID, 'edad', '', ', ', '');
				    		$terms = strip_tags( $terms );
                                    		echo $terms;
                                	}
					else
					{
                                    		echo get_post_meta($post->ID, 'f_edad', true);
                                	}
                                ?>
                                </span>
                            </div>










                            <div>
                                <h3><span>RECINTO</span></h3>
                                <span>
                                <?php echo get_post_meta($post->ID, 'f_recinto', true); ?>
                                </span>
                            </div>

                            <div>
                                <h3><span>DIRECCIÓN</span></h3>
                                <span>
                                <?php echo get_post_meta($post->ID, 'f_dirección', true); ?>
                                </span>
                            </div>
                            <div>
                                <h3><span>LOCALIDAD</span></h3>
                                <span><?php echo get_post_meta($post->ID, 'f_localidad', true); ?></span>
                            </div>
                            <div>
                                <h3><span>PROVINCIA</span></h3>
                                <span>
																	<?php
                                		if (get_the_term_list($post->ID, 'provincia', '', ', ', ''))
																		{
				    													$terms = get_the_term_list($post->ID, 'provincia', '', ', ', '');
				    													$terms = strip_tags( $terms );
                                    		echo $terms;
                                		}
																		else
																		{
                                    		echo get_post_meta($post->ID, 'f_provincia', true);
                                		}
                                	?>
                                </span>
                            </div>
														<div>
			    									<?php
                            	if ($caja_query <> 1 && !isset($_GET['codigo']))
															{
																$servicios = obteneter_servicios($caja_query, $post->ID, '');
																if ($servicios[0]['alojamientos'] <> '')
																{
																	echo '<div>
																	<h3><span>ALOJAMIENTO</span></h3>
																	<span>';
																	foreach ($servicios as $servicio)
																	{
																		$splits = explode(',',$servicio['alojamientos']);
																		foreach($splits as $split)
																		{
																			if ($split <> '')
																			{
																				$hoteles[] = get_the_title($split);
																				echo '<a href= ' .get_permalink($split). '>' .get_the_title($split). ' </a>';
																			}
																		}
																	}
																	//echo implode(', ', $hoteles);
																	echo '</span>
																	</div>';
																}
																else
																{
																	//echo '<div>
																	//<h3><span>ALOJAMIENTO</span></h3>
																	//<span>';
																	//echo '</span>
																	//</div>';
																}
											        }
														?>
                        </div>
					</article>


				<?php endwhile; else: ?>

					<h1><?php _e('No posts were found!', ETHEME_DOMAIN) ?></h1>

				<?php endif; ?>

				<?php comments_template('', true); ?>

			</div>

			<?php if($position == 'right' || ($responsive == 'bottom' && $position == 'left')): ?>
				<div class="<?php echo $sidebar_span; ?> sidebar sidebar-right">
					<?php etheme_get_sidebar($sidebarname); ?>
				</div>
			<?php endif; ?>

		</div>

	</div>
</div>

<?php
	get_footer();
?>
