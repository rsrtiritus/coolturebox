<?php 
	get_header();
?>
<?php 
	extract(etheme_get_blog_sidebar());
	$postspage_id = get_option('page_for_posts');
	$content_layout = $blog_layout;
	if($content_layout == 'mosaic') {
		$content_layout = 'grid';
	}
?>

<?php if ($page_heading != 'disable' && ($page_slider == 'no_slider' || $page_slider == '')): ?>
	<div class="page-heading bc-type-<?php etheme_option('breadcrumb_type'); ?>">
		<div class="container">
			<div class="row">
				<div class="col-md-12 a-center">
					<h1 class="title"><span><?php echo "Eventos"; ?></span></h1>
					<?php etheme_breadcrumbs(); ?>
				</div>
			</div>
		</div>
	</div>
<?php endif ?>

<?php if($page_slider != 'no_slider' && $page_slider != ''): ?>
	
	<?php echo do_shortcode('[rev_slider_vc alias="'.$page_slider.'"]'); ?>

<?php endif; ?>


<div class="container">
	<div class="page-content sidebar-position-<?php echo $position; ?> responsive-sidebar-<?php echo $responsive; ?>">
		<div class="row">
			<?php if($position == 'left' || ($responsive == 'top' && $position == 'right')): ?>
				<div class="<?php echo $sidebar_span; ?> sidebar sidebar-left">
					<?php etheme_get_sidebar($sidebarname); ?>
				</div>
			<?php endif; ?>
			
			
            <div class="content <?php echo $content_span; ?>">
				<?php echo do_shortcode('[vc_row][vc_column width="1/1"][vc_column_text]

No es nada fácil poder complacer a todo el mundo ya que todos tenemos gustos muy diferentes.

¡En Festibox podemos presumir de haberlo conseguido!

Dentro de nuestras Cajas Regalo encontrarás un impresionante catálogo de eventos de todo tipo, para todos las edades y para todos los gustos.

Da igual la edad que tengas o si te apetece disfrutar de los mejores espectáculos, sólo, en pareja, con amigos o con toda la familia, incluidos los más pequeños.

[/vc_column_text][vc_posts_grid grid_columns_count="3" pagination="show" grid_content="teaser" posted_block="hide" hover_mask="hide" grid_layout="thumbnail_title" grid_template="grid" orderby="title" order="ASC" grid_posttypes="evento" grid_teasers_count="all"][/vc_column][/vc_row]'); ?>
			</div>

			<?php if($position == 'right' || ($responsive == 'bottom' && $position == 'left')): ?>
				<div class="<?php echo $sidebar_span; ?> sidebar sidebar-right">
					<?php etheme_get_sidebar($sidebarname); ?>
				</div>
			<?php endif; ?>
		</div>


	</div>
</div>

	
<?php
	get_footer();
?>