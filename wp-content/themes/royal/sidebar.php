<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('main-sidebar')): ?>
	
	<div class="sidebar-widget widget_search">
		<h4 class="widget-title"><?php _e('Buscar', ETHEME_DOMAIN) ?></h4>
		<?php get_search_form(); ?>
	</div>

<?php endif; ?>	