<?php 
	get_header();
?>
<?php 
	extract(etheme_get_blog_sidebar());
	$postspage_id = get_option('page_for_posts');
	$content_layout = $blog_layout;
	if($content_layout == 'mosaic') {
		$content_layout = 'grid';
	}
?>

<?php if ($page_heading != 'disable' && ($page_slider == 'no_slider' || $page_slider == '')): ?>
	<div class="page-heading bc-type-<?php etheme_option('breadcrumb_type'); ?>">
		<div class="container">
			<div class="row">
				<div class="col-md-12 a-center">
					<h1 class="title"><span><?php echo get_search_query(); ?></span></h1>
					<?php etheme_breadcrumbs(); ?>
				</div>
			</div>
		</div>
	</div>
<?php endif ?>

<?php if($page_slider != 'no_slider' && $page_slider != ''): ?>
	
	<?php echo do_shortcode('[rev_slider_vc alias="'.$page_slider.'"]'); ?>

<?php endif; ?>


<div class="container">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<?php 
                if(have_posts()){ while(have_posts()) : the_post();
 //                   if (get_post_type() == "festival"){
						$festivales[] = $post -> ID;
//					}
                endwhile;
                    $festival = implode(",", $festivales);
                    echo do_shortcode('[vc_row][vc_column width="1/1"][vc_posts_grid grid_columns_count="3" pagination="show" grid_content="teaser" posted_block="hide" hover_mask="hide" grid_layout="thumbnail_title" grid_template="grid" orderby="title" order="ASC" grid_posttypes="festival,concierto,espectaculo,product" grid_teasers_count="12" posts_in="'.$festival.'"][/vc_column][/vc_row]');
                 }else{ ?>
                    <div class="empty-category-block">
                        <h2>No se ha encontrado nada en Festibox.</h2>
                        <h2>Prueba otra vez.</h2>
                    </div>

                    <h3><?php _e('No posts were found!', ETHEME_DOMAIN) ?></h3>

                <?php } ?>
				<?php //   etheme_pagination($my_query, $paged);?>
			</div>
		</div>


	</div>
</div>

	
<?php
	get_footer();
?>