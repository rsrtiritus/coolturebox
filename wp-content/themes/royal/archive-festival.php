<?php 
	get_header();
?>
<?php 
	extract(etheme_get_blog_sidebar());
	$postspage_id = get_option('page_for_posts');
	$content_layout = $blog_layout;
	if($content_layout == 'mosaic') {
		$content_layout = 'grid';
	}
?>

<?php if ($page_heading != 'disable' && ($page_slider == 'no_slider' || $page_slider == '')): ?>
	<div class="page-heading bc-type-<?php etheme_option('breadcrumb_type'); ?>">
		<div class="container">
			<div class="row">
				<div class="col-md-12 a-center">
					<h1 class="title"><span><?php echo "Festivales"; ?></span></h1>
					<?php etheme_breadcrumbs(); ?>
				</div>
			</div>
		</div>
	</div>
<?php endif ?>

<?php if($page_slider != 'no_slider' && $page_slider != ''): ?>
	
	<?php echo do_shortcode('[rev_slider_vc alias="'.$page_slider.'"]'); ?>

<?php endif; ?>


<div class="container">
	<div class="page-content sidebar-position-<?php echo $position; ?> responsive-sidebar-<?php echo $responsive; ?>">
		<div class="row">
			<?php if($position == 'left' || ($responsive == 'top' && $position == 'right')): ?>
				<div class="<?php echo $sidebar_span; ?> sidebar sidebar-left">
					<?php etheme_get_sidebar($sidebarname); ?>
				</div>
			<?php endif; ?>
			
			
            <div class="content <?php echo $content_span; ?>">
				<?php echo do_shortcode('[vc_row][vc_column width="1/1"][vc_column_text]

No es nada fácil poder complacer a todo el mundo ya que todos tenemos gustos muy diferentes.

¡En Festibox podemos presumir de haberlo conseguido!

Dentro de nuestras Cajas Regalo encontrarás un impresionante catálogo de festivales de todo tipo, para todos las edades y para todos los gustos.

Da igual la edad que tengas o si te apetece disfrutar de los mejores espectáculos, sólo, en pareja, con amigos o con toda la familia, incluidos los más pequeños.

Tenemos festivales que van desde el pop y el indie más amables, al rock, jazz o blues más puristas, pasando por la electrónica más cañera y de vanguardia, sonidos clásicos, músicas de raíz, música balcánica, reggae, soul, funk, boogaloo y todos los estilos que puedas llegar a imaginar.

Festivales que duran un día, dos o incluso una semana para los más incombustibles. Festivales para disfrute de los más jóvenes y alocados, los más adultos buscan un ambiente más tranquilo y sosegado, los que demandan los sonidos más exquisitos al más puro estilo V.I.P., todos los públicos tienen cabida en FestiBox.

Tenemos festivales con aforos de 500 hasta 50.000 personas, que tienen lugar en las mejores playas, parajes naturales, en grandes ciudades o pueblecitos con encanto, auditorios, lugares privados super exclusivos, teatros, recintos grandes y pequeños, que tienen lugar durante el día o en plena la noche, incluso lugares escondidos y secretos que sólo conocen quienes lo disfrutan, todo ello combinado con los mejores alojamientos, hoteles rústicos, céntricos y vanguardistas hasta los más exclusivos de 5 estrellas con el máximo lujo.

Esto son algunos ejemplos de lo que hablamos y de los que podrás disfrutar con FestiBox.

[/vc_column_text][vc_posts_grid grid_columns_count="3" pagination="show" grid_content="teaser" posted_block="hide" hover_mask="hide" grid_layout="thumbnail_title" grid_template="grid" orderby="title" order="ASC" grid_posttypes="festivales" grid_teasers_count="all"][/vc_column][/vc_row]'); ?>
			</div>

			<?php if($position == 'right' || ($responsive == 'bottom' && $position == 'left')): ?>
				<div class="<?php echo $sidebar_span; ?> sidebar sidebar-right">
					<?php etheme_get_sidebar($sidebarname); ?>
				</div>
			<?php endif; ?>
		</div>


	</div>
</div>

	
<?php
	get_footer();
?>