<?php 
	get_header();
?>

<?php 

    extract(etheme_get_page_sidebar());
    $blog_slider = etheme_get_option('blog_slider');
    $postspage_id = get_option('page_for_posts');
    $post_format = get_post_format();
    
    $post_content = $post->post_content;
    preg_match('/\[gallery.*ids=.(.*).\]/', $post_content, $ids);
    if(!empty($ids)) {
	    $attach_ids = explode(",", $ids[1]);
	    $content =  str_replace($ids[0], "", $post_content);
	    $filtered_content = apply_filters( 'the_content', $content);
    }
    
    $slider_id = rand(100,10000);
?>


<?php if ($page_heading != 'disable' && ($page_slider == 'no_slider' || $page_slider == '')): ?>
	<div class="page-heading bc-type-<?php echo $breadcrumb_type; ?>">
		<div class="container">
			<div class="row">
				<div class="col-md-12 a-center">
				<h1 class="title"><span><?php echo get_the_title($postspage_id); ?></span></h1>
					<?php etheme_breadcrumbs(); ?>
				</div>
			</div>
		</div>
	</div>
<?php endif ?>

<?php if($page_slider != 'no_slider' && $page_slider != ''): ?>
	
	<?php echo do_shortcode('[rev_slider_vc alias="'.$page_slider.'"]'); ?>

<?php endif; ?>

<div class="container">
	<div class="page-content sidebar-position-<?php echo $position; ?> responsive-sidebar-<?php echo $responsive; ?>">
		<div class="row">
			<?php if($position == 'left' || ($responsive == 'top' && $position == 'right')): ?>
				<div class="<?php echo $sidebar_span; ?> sidebar sidebar-left">
					<?php etheme_get_sidebar($sidebarname); ?>
				</div>
			<?php endif; ?>

			<div class="content <?php echo $content_span; ?>">
				<?php if(have_posts()): while(have_posts()) : the_post(); ?>
				
					<article <?php post_class('blog-post post-single'); ?> id="post-<?php the_ID(); ?>" >
                        <div>
                            <h2 class="titulo_evento"><?php echo get_the_title($postspage_id); ?></h2>
                        <div>
                        <div>
                        	<h2>PRODUCTO</h2>
                            <ul>
							<?php
							$datas = explode(',',get_post_meta($post->ID, 'f_productos', true));
							foreach ($datas as $data){
								if ($data <> ''){
								echo "<li>".get_the_title($data)."</li>";
								}
							}
							?>
                            </ul>
                        </div>
                        <div>
                        	<h2>FESTIVALES</h2>
                            <ul>
							<?php
							$datas = explode(',',get_post_meta($post->ID, 'f_festivales', true));							
							foreach ($datas as $data){
								if ($data <> ''){
echo "<li>".get_the_title($data)."</li>";
								}
							}
							?>
                            </ul>
                        </div>
						
                        <div>
                        	<h2>TIPO ENTRADA</h2>
							<?php 
							$entrada = get_post_meta($post->ID, 'f_entrada', true); 
							if ($entrada=="0"){
								echo "Entrada Normal";
							}elseif ($entrada=="1"){
								echo "Entrada VIP";
							}elseif($entrada=="2"){
								echo "Abono Normal";
							}elseif($entrada=="3"){
								echo "Abono VIP";
							}
							?>
                        </div>
                        <div>
                        	<h2>Nº ENTRADAS</h2>
							<?php echo get_post_meta($post->ID, 'f_nentrada', true); ?>
                        </div>
                        <div>
                        	<h2>ALOJAMIENTOS</h2>
                            <ul>
							<?php
							$datas = explode(',',get_post_meta($post->ID, 'f_alojamientos', true));							
							foreach ($datas as $data){
								if ($data <> ''){
echo "<li>".get_the_title($data)."</li>";
								}
							}
							?>
                            </ul>
                        </div>
                        <div>
                        	<h2>Nº NOCHES</h2>
							<?php echo get_post_meta($post->ID, 'f_noches', true); ?>
                        </div>
                        <div>
                        	<h2>OTRO</h2>
							<?php echo get_post_meta($post->ID, 'f_otro', true); ?>
                        </div>

                        <div>
                        	<h2>Pruebas</h2>
                            <ul>
							<?php
							$datas = obtener_caja_festivales2();
							foreach ($datas as $data){
								if ($data <> ''){
									$datas2 = obtener_hoteles_festival2($data);
									$hoteles = implode(",", $datas2);
									echo "<li>".get_the_title($data).";".$data.";,".$hoteles.",</li>";
								}
							}
							?>
                            </ul>
                        </div>
					</article>


				<?php endwhile; else: ?>

					<h1><?php _e('No posts were found!', ETHEME_DOMAIN) ?></h1>

				<?php endif; ?>

				<?php comments_template('', true); ?>

			</div>

			<?php if($position == 'right' || ($responsive == 'bottom' && $position == 'left')): ?>
				<div class="<?php echo $sidebar_span; ?> sidebar sidebar-right">
					<?php etheme_get_sidebar($sidebarname); ?>
				</div>
			<?php endif; ?>

		</div>

	</div>
</div>
	
<?php
	get_footer();
?>