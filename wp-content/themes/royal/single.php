<?php 
	get_header();
?>

<?php 

    extract(etheme_get_page_sidebar());
    $blog_slider = etheme_get_option('blog_slider');
    $postspage_id = get_option('page_for_posts');
    $post_format = get_post_format();
    
    $post_content = $post->post_content;
    preg_match('/\[gallery.*ids=.(.*).\]/', $post_content, $ids);
    if(!empty($ids)) {
	    $attach_ids = explode(",", $ids[1]);
	    $content =  str_replace($ids[0], "", $post_content);
	    $filtered_content = apply_filters( 'the_content', $content);
    }
    
    $slider_id = rand(100,10000);
?>


<?php if ($page_heading != 'disable' && ($page_slider == 'no_slider' || $page_slider == '')): ?>
	<div class="page-heading bc-type-<?php echo $breadcrumb_type; ?>">
		<div class="container">
			<div class="row">
				<div class="col-md-12 a-center">
				<h1 class="title"><span><?php echo get_the_title($postspage_id); ?></span></h1>
					<?php etheme_breadcrumbs(); ?>
				</div>
			</div>
		</div>
	</div>
<?php endif ?>

<?php if($page_slider != 'no_slider' && $page_slider != ''): ?>
	
	<?php echo do_shortcode('[rev_slider_vc alias="'.$page_slider.'"]'); ?>

<?php endif; ?>

<div class="container">
	<div class="page-content sidebar-position-<?php echo $position; ?> responsive-sidebar-<?php echo $responsive; ?>">
		<div class="row">
			<?php if($position == 'left' || ($responsive == 'top' && $position == 'right')): ?>
				<div class="<?php echo $sidebar_span; ?> sidebar sidebar-left">
					<?php etheme_get_sidebar($sidebarname); ?>
				</div>
			<?php endif; ?>

			<div class="content <?php echo $content_span; ?>">
				<?php if(have_posts()): while(have_posts()) : the_post(); ?>
				
					<article <?php post_class('blog-post post-single'); ?> id="post-<?php the_ID(); ?>" >
						<?php // LUIS
						if (in_category('hoteles') ):
						?>
                            <div>
                                <h2 style="font-size: -webkit-xxx-large;line-height: initial;"><?php echo get_the_title($postspage_id); ?></h2>
                                <div>
                                    <h3><?php echo get_post_meta($post->ID, 'f_localidad', true); ?>, <?php echo get_post_meta($post->ID, 'f_provincia', true); ?></h3>
                                </div>
                            </div>
							<div class="fest_img">
                                <div class="wp-picture">
                                    <?php the_post_thumbnail('large'); ?>
                                    <div class="zoom">
                                        <div class="btn_group">
                                            <a href="<?php echo etheme_get_image(); ?>" class="btn btn-black xmedium-btn" rel="pphoto"><span><?php _e('Expandir11', ETHEME_DOMAIN); ?></span></a>
                                        </div>
                                        <i class="bg"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="fes_detalles">
                                <div style="font-size:200%;">
                                    <h3><span>CATEGORÍA</span></h3>
									<?php echo get_post_meta($post->ID, 'f_categoria', true); ?>
                                </div>
                                <div>
                                    <h3><span>IDIOMA</span></h3>
                                    <?php echo get_post_meta($post->ID, 'f_idioma', true); ?>
                                </div>
                                <div>
                                    <h3><span>DIRECCIÓN</span></h3>
                                    <?php echo get_post_meta($post->ID, 'f_direccion', true); ?>
                                </div>
                                <div>
                                    <h3><span>DESCRIPCIÓN</span></h3>
									<?php the_content(); ?>
                                </div>
                            </div>
						<?php // LUIS
						elseif (in_category('festivales') ):
						?>
							<div class="fest_img">
<!--                                <div class="entry-post-nav group">
                                    <?php $prev_post = get_previous_post(); ?>
                                
                                    <?php if ( !empty( $prev_post ) ) : ?>  
                                        <div class="post-prev">
                                            <a href="<?php echo get_permalink( $prev_post->ID ); ?>">? Prev Posts</a>
                                            <h4><a href="<?php echo get_permalink( $prev_post->ID ); ?>"><?php echo $prev_post->post_title; ?></a></h4>
                                        </div>
                                    <?php endif; ?>
                                
                                    <?php $next_post = get_next_post(); ?>
                                
                                    <?php if ( !empty( $next_post ) ) : ?>  
                                        <div class="post-next">
                                            <a href="<?php echo get_permalink( $next_post->ID ); ?>">Next Posts ?</a>
                                            <h4><a href="<?php echo get_permalink( $next_post->ID ); ?>"><?php echo $next_post->post_title; ?></a></h4>
                                        </div>
                                    <?php endif; ?>
                                </div>
-->                                <div>
                                    <h2 style="font-size: -webkit-xxx-large;line-height: initial;"><?php echo get_the_title($postspage_id); ?></h2>
                                    <div>
                                        <h3><?php echo get_post_meta($post->ID, 'f_comunidad', true); ?></h3>
                                    </div>
<!--                                    <div>
                                        <?php echo get_post_meta($post->ID, 'f_web', true); ?>
                                    </div>
-->                                </div>
                                <div class="content-article">
                                        <?php the_content(); ?>
                                </div>
<!--                                <div>
                                 <?php $orange = get_post_meta($post->ID, 'f_orange', true);?>
                                 <?php $pink = get_post_meta($post->ID, 'f_pink', true);?>
                                 <?php $red = get_post_meta($post->ID, 'f_red', true);?>
                                 <?php $blue = get_post_meta($post->ID, 'f_blue', true);?>
                                 <?php $green = get_post_meta($post->ID, 'f_green', true);?>
                                 <?php $yellow = get_post_meta($post->ID, 'f_yellow', true);?>
                                 <?php $purple = get_post_meta($post->ID, 'f_purple', true);?>
                                 <?php $black = get_post_meta($post->ID, 'f_black', true);?>
                                 <p>Este festival está contenido en las cajas:</p>
                                 <p><ul class="cajas_list">
                                 	<?php if ($orange == 1) echo "<li><a href='/cajas/orange-box' title='ORANGE BOX'><img src='/wp-content/uploads/2014/12/fondo-orange.png'/></a></li>";?>
                                 	<?php if ($pink == 1) echo "<li><a href='/cajas/pink-box' title='PINK BOX'><img src='/wp-content/uploads/2014/12/fondo-pink.png'/><a></li>";?>
                                 	<?php if ($red == 1) echo "<li><a href='/cajas/red-box' title='RED BOX'><img src='/wp-content/uploads/2014/12/fondo-red.png'/></a></li>";?>
                                 	<?php if ($blue == 1) echo "<li><a href='/cajas/blue-box' title='BLUE BOX'><img src='/wp-content/uploads/2014/12/fondo-blue.png'/></a></li>";?>
                                 	<?php if ($green == 1) echo "<li><a href='/cajas/green-box' title='GREEN BOX'><img src='/wp-content/uploads/2014/12/fondo-green.png'/></a></li>";?>
                                 	<?php if ($yellow == 1) echo "<li><a href='/cajas/yellow-box' title='ORANGE BOX'><img src='/wp-content/uploads/2014/12/fondo-yellow.png'/></a></li>";?>
                                 	<?php if ($purple == 1) echo "<li><a href='/cajas/purple-box' title='PURPLE BOX'><img src='/wp-content/uploads/2014/12/fondo-purple.png'/></a></li>";?>
                                 	<?php if ($black == 1) echo "<li><a href='/cajas/black-box' title='BLACK BOX'><img src='/wp-content/uploads/2014/12/fondo-black.png'/></a></li>";?>
                                 </ul>
                                 </p>
                                </div>
-->                                <div class="wp-picture">
                                    <?
php the_post_thumbnail('large'); ?>
                                    <div class="zoom">
                                        <div class="btn_group">
                                            
                                            <a href="<?php echo etheme_get_image(); ?>" class="btn btn-black xmedium-btn" rel="pphoto"><span><?php _e('Expandir', ETHEME_DOMAIN); ?></span></a>
                                        </div>
                                        <i class="bg"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="fes_detalles">
                                <div>
                                    <h3><span>ESTILO</span></h3>
									<?php echo get_post_meta($post->ID, 'f_estilo', true); ?>
                                </div>
                                <div>
                                    <h3><span>CÚANDO</span></h3>
                                    <?php echo get_post_meta($post->ID, 'f_cuándo', true); ?>
                                </div>
                                <div>
                                    <h3><span>DURACIÓN</span></h3>
                                    <?php echo get_post_meta($post->ID, 'f_duración', true); ?>
                                </div>
                                <div>
                                    <h3><span>RECINTO</span></h3>
                                    <?php echo get_post_meta($post->ID, 'f_recinto', true); ?>
                                </div>
                                <div>
                                    <h3><span>ANTECEDENTES</span></h3>
                                    <?php echo get_post_meta($post->ID, 'f_antecedentes', true); ?>
                                </div>
                                <div>
                                    <h3><span>EDAD</span></h3>
                                    <?php echo get_post_meta($post->ID, 'f_edad', true); ?>
                                </div>
                                <div>
                                    <h3><span>LOCALIDAD</span></h3>
                                    <?php echo get_post_meta($post->ID, 'f_localidad', true); ?>
                                </div>
                                <div>
                                    <h3><span>PROVINCIA</span></h3>
                                    <?php echo get_post_meta($post->ID, 'f_provincia', true); ?>
                                </div>
                                <div>
                                    <h3><span>DIRECCIÓN</span></h3>
                                    <?php echo get_post_meta($post->ID, 'f_dirección', true); ?>
                                </div>
                                <div>
                                    <h3><span>HOTELES</span></h3>
                                    <a href="/<?php echo limpiar(str_replace(" ", "-",str_replace(" *", "",str_replace("*", "", get_post_meta($post->ID, 'f_hotel1', true)))));?>" title="<?php echo get_post_meta($post->ID, 'f_hotel1', true); ?>" ><?php echo get_post_meta($post->ID, 'f_hotel1', true); ?></a><br />
                                    <a href="/<?php echo limpiar(str_replace(" ", "-",str_replace(" *", "",str_replace("*", "", get_post_meta($post->ID, 'f_hotel2', true)))));?>" title="<?php echo get_post_meta($post->ID, 'f_hotel2', true); ?>" ><?php echo get_post_meta($post->ID, 'f_hotel2', true); ?></a>
                                </div>
                            </div>
						
						<?php // LUIS
						else: ?>

						<?php 
							$width = etheme_get_option('blog_page_image_width');
							$height = etheme_get_option('blog_page_image_height');
							$crop = etheme_get_option('blog_page_image_cropping');
						?>
						

						<?php if($post_format == 'quote' || $post_format == 'video'): ?>
					    
					            <?php the_content(); ?>
					        
						<?php elseif($post_format == 'gallery'): ?>
					            <?php if(count($attach_ids) > 0): ?>
					                <div class="post-gallery-slider slider_id-<?php echo $slider_id; ?>">
					                    <?php foreach($attach_ids as $attach_id): ?>
					                        <div>
					                            <?php echo wp_get_attachment_image($attach_id, 'large'); ?>
					                        </div>
					                    <?php endforeach; ?>
					                </div>
					    
					                <script type="text/javascript">
					                    jQuery('.slider_id-<?php echo $slider_id; ?>').owlCarousel({
					                        items:1,
					                        navigation: true,
					                        lazyLoad: false,
					                        rewindNav: false,
					                        addClassActive: true,
					                        singleItem : true,
					                        autoHeight : true,
					                        itemsCustom: [1600, 1]
					                    });
					                </script>
					            <?php endif; ?>
					    
						<?php elseif(has_post_thumbnail()): ?>
							<div class="wp-picture">
								<?php the_post_thumbnail('large'); ?>
								<div class="zoom">
									<div class="btn_group">
										<a href="<?php echo etheme_get_image(); ?>" class="btn btn-black xmedium-btn" rel="pphoto"><span><?php _e('Expandir', ETHEME_DOMAIN); ?></span></a>
									</div>
									<i class="bg"></i>
								</div>
							</div>
						<?php endif; ?>

                        <?php if($post_format != 'quote'): ?>
                            <h6 class="active"><?php the_category(',&nbsp;') ?></h6>

                            <h2><?php the_title(); ?></h2>

                        	<?php if(etheme_get_option('blog_byline')): ?>
                                <div class="meta-post">
                                        <?php _e('Posted on', ETHEME_DOMAIN) ?>
                                        <?php the_time(get_option('date_format')); ?> 
                                        <?php _e('at', ETHEME_DOMAIN) ?> 
                                        <?php the_time(get_option('time_format')); ?>
                                        <?php _e('by', ETHEME_DOMAIN);?> <?php the_author_posts_link(); ?>
                                        <?php // Display Comments 

                                                if(comments_open() && !post_password_required()) {
                                                        echo ' / ';
                                                        comments_popup_link('0', '1 Comment', '% Comments', 'post-comments-count');
                                                }

                                         ?>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>

                        <?php if($post_format != 'quote' && $post_format != 'video' && $post_format != 'gallery'): ?>
                            <div class="content-article">
                                    <?php the_content(); ?>
                            </div>
                        <?php elseif($post_format == 'gallery'): ?>
                            <div class="content-article">
                                <?php echo $filtered_content; ?>
                            </div>
                        <?php endif; ?>
					
						<?php if(etheme_get_option('post_share')): ?>
							<div class="share-post">
								<?php echo do_shortcode('[share title="'.__('Share Post', ETHEME_DOMAIN).'"]'); ?>
							</div>
						<?php endif; ?>
						
						<?php if(etheme_get_option('posts_links')): ?>
							<?php etheme_project_links(array()); ?>
						<?php endif; ?>
						
						
						<?php if(etheme_get_option('about_author')): ?>
							<h4 class="title-alt"><span><?php _e('About Author', ETHEME_DOMAIN); ?></span></h4>
							
							<div class="author-info">
								<a class="pull-left" href="#">
									<?php echo get_avatar( get_the_author_meta('email') , 90 ); ?>
								</a>
								<div class="media-body">
									<h4 class="media-heading"><?php the_author_link(); ?></h4>
									<?php echo get_the_author_meta('description'); ?>
								</div>
							</div>
						<?php endif; ?>
						
						<?php if(etheme_get_option('post_related')): ?>
							<div class="related-posts">
								<?php et_get_related_posts(); ?>
							</div>
						<?php endif; ?>
						
						<?php //LUIS
						endif; ?>
					</article>


				<?php endwhile; else: ?>

					<h1><?php _e('No posts were found!', ETHEME_DOMAIN) ?></h1>

				<?php endif; ?>

				<?php comments_template('', true); ?>

			</div>

			<?php if($position == 'right' || ($responsive == 'bottom' && $position == 'left')): ?>
				<div class="<?php echo $sidebar_span; ?> sidebar sidebar-right">
					<?php etheme_get_sidebar($sidebarname); ?>
				</div>
			<?php endif; ?>

		</div>

	</div>
</div>
	
<?php
	get_footer();
?>