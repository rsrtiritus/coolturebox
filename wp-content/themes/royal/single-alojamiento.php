<?php 
	get_header();
?>

<?php 

    extract(etheme_get_page_sidebar());
    $blog_slider = etheme_get_option('blog_slider');
    $postspage_id = get_option('page_for_posts');
    $post_format = get_post_format();
    
    $post_content = $post->post_content;
    preg_match('/\[gallery.*ids=.(.*).\]/', $post_content, $ids);
    if(!empty($ids)) {
	    $attach_ids = explode(",", $ids[1]);
	    $content =  str_replace($ids[0], "", $post_content);
	    $filtered_content = apply_filters( 'the_content', $content);
    }
    
    $slider_id = rand(100,10000);
	if (!empty($codigo)) $url .= '&codigo='.$_GET['codigo'];
	if (isset($_GET['fv']) || $_GET['fv'] <> '') $url .= '&fv='.$_GET['fv'];
	if (isset($_GET['sv']) || $_GET['sv'] <> '') $url .= '&sv='.$_GET['sv'];
	if (isset($_GET['ht']) || $_GET['ht'] <> '') $url .= '&ht='.$_GET['ht'];
?>


<?php if ($page_heading != 'disable' && ($page_slider == 'no_slider' || $page_slider == '')): ?>
	<div class="page-heading bc-type-<?php echo $breadcrumb_type; ?>">
		<div class="container">
			<div class="row">
				<div class="col-md-12 a-center">
				<h1 class="title"><span><?php echo get_the_title($postspage_id); ?></span></h1>
					<?php etheme_breadcrumbs(); ?>
				</div>
			</div>
		</div>
	</div>
<?php endif ?>

<?php if($page_slider != 'no_slider' && $page_slider != ''): ?>
	
	<?php echo do_shortcode('[rev_slider_vc alias="'.$page_slider.'"]'); ?>

<?php endif; ?>

<div class="container">
	<div class="page-content sidebar-position-<?php echo $position; ?> responsive-sidebar-<?php echo $responsive; ?>">
		<div class="row">
			<?php if($position == 'left' || ($responsive == 'top' && $position == 'right')): ?>
				<div class="<?php echo $sidebar_span; ?> sidebar sidebar-left">
					<?php etheme_get_sidebar($sidebarname); ?>
				</div>
			<?php endif; ?>

			<div class="content <?php echo $content_span; ?>">
				<?php if(have_posts()): while(have_posts()) : the_post(); ?>
				
					<article <?php post_class('blog-post post-single'); ?> id="post-<?php the_ID(); ?>" >
						<div class="fest_img">
                            <div>
                                <h2 style="font-size:24px"><?php echo get_the_title($postspage_id); ?></h2>
                              <div>
                                  <h3 style="font-size:0px"><?php echo get_post_meta($post->ID, 'f_comunidad', true); ?></h3>
                              </div>
                            </div>
                        </div>
						<div class="fest_img">
                            <div class="content-article">
                                    <?php the_content(); ?>
                            </div>
                            <div class="wp-picture fest_img">
                                <?php the_post_thumbnail('large'); ?>
                                <div class="zoom">
                                    <div class="btn_group">
                                        <a href="<?php echo etheme_get_image(); ?>" class="btn btn-black xmedium-btn" rel="pphoto"><span><?php _e('Expandir', ETHEME_DOMAIN); ?></span></a>
                                    </div>
                                    <i class="bg"></i>
                                </div>
                            </div>
                        </div>
                        <div class="fes_detalles">
							<?php if (isset($_GET['codigo'])){
							?>
                                <div style="font-size:200%;line-height: 20px;">
                                    <h3><span>SELECCIONAR</span></h3>
                                    <?php 
									echo '<form method="GET" action="'.get_site_url().'/reservas/" style="text-align: center;  padding-bottom: 6px;">';	
									echo '<input type="hidden" name="ht" value="'.$post->ID.'"/>';
									echo '<input type="hidden" name="fv" value="'.$_GET['fv'].'"/>';
									echo '<input type="hidden" name="codigo" value="'.$_GET['codigo'].'"/>';
									echo '<input type="hidden" name="sv" value="'.$_GET['sv'].'"/>';
									echo '<input type="hidden" name="ff" value="'.$_GET['ff'].'"/>';
									$fechas = obtener_fechas_festival($_GET['fv']);
									$noches = get_post_meta($_GET['sv'], 'f_noches', true);
									if ($noches == sizeof($fechas) || $fechas[0] == 'AÚN POR CONFIRMAR' ){
										echo '<input type="hidden" name="hf" value="'.$_GET['ff'].'"/>';
									}
									echo '<input type="submit" value="seleccionar">';
									echo '<input type="submit" value="volver" onclick="history.back()" style="margin-left:6px">';
									echo '</form>';
									?>
                                </div>
                            <?php }?>
                            <div>
                                <h3><span>DIRECCIÓN</span></h3>
                                <?php echo get_post_meta($post->ID, 'f_direccion', true); ?>
                            </div>
                             <div>
                                <h3><span>LOCALIDAD</span></h3>
                                <?php echo get_post_meta($post->ID, 'f_localidad', true); ?>
                            </div>
                            <div>
                                <h3><span>PROVINCIA</span></h3>
                                <?php echo get_post_meta($post->ID, 'f_provincia', true); ?>
                            </div>
                            <?php if (get_post_meta($post->ID, 'f_comunidad', true)){ ?>
                            <div>
                                <h3><span>COMUNIDAD AUTÓNOMA</span></h3>
                                <?php echo get_post_meta($post->ID, 'f_comunidad', true); ?>
                            </div>
                            <?php } ?>
                            
                            <div style="font-size:150%;line-height: 30px;">
                                <h3><span>CATEGORÍA</span></h3>
                                <?php echo get_post_meta($post->ID, 'f_categoria', true); ?>
                            </div>
                            
                            <?php if (get_post_meta($post->ID, 'f_festivales', true)){?>
                                <div style="display:none">
                                    <h3><span>FESTIVALES</span></h3>
                                    <?php
        							$festivales = explode(',',get_post_meta($post->ID, 'f_festivales', true));
									foreach($festivales as $festival):
										if ($festival <> ''){
											echo '<a href="'.get_permalink($festival).'" title="'.get_the_title($festival).'" alt="'.get_the_title($festival).'">'.get_the_title($festival).'</a><br />';
										}
									endforeach;
									?>
                                </div>
                            <?php }?>
                        </div>
					</article>


				<?php endwhile; else: ?>

					<h1><?php _e('No posts were found!', ETHEME_DOMAIN) ?></h1>

				<?php endif; ?>

				<?php comments_template('', true); ?>

			</div>

			<?php if($position == 'right' || ($responsive == 'bottom' && $position == 'left')): ?>
				<div class="<?php echo $sidebar_span; ?> sidebar sidebar-right">
					<?php etheme_get_sidebar($sidebarname); ?>
				</div>
			<?php endif; ?>

		</div>

	</div>
</div>
	
<?php
	get_footer();
?>