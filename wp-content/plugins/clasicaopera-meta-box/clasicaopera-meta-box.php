<?php 
/*
Plugin Name: Clásica y Ópera en campos personalizados
Plugin URI:
*/

//hook para crear el meta box principal de los Clásica y Ópera
function custom_clasicaopera_type() {

// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Clásica y Ópera', 'Post Type General Name', 'festibox' ),
		'singular_name'       => _x( 'Clásica y Ópera', 'Post Type Singular Name', 'festibox' ),
		'menu_name'           => __( 'Clásica y Ópera', 'festibox' ),
		'parent_item_colon'   => __( 'Clásica y Ópera Padre', 'festibox' ),
		'all_items'           => __( 'Todos los Clásica y Ópera', 'festibox' ),
		'view_item'           => __( 'Ver Clásica y Ópera', 'festibox' ),
		'add_new_item'        => __( 'Añadir nuevo Clásica y Ópera', 'festibox' ),
		'add_new'             => __( 'Añadir nuevo', 'festibox' ),
		'edit_item'           => __( 'Editar Clásica y Ópera', 'festibox' ),
		'update_item'         => __( 'Actualizar Clásica y Ópera', 'festibox' ),
		'search_items'        => __( 'Buscar Clásica y Ópera', 'festibox' ),
		'not_found'           => __( 'No encontrado', 'festibox' ),
		'not_found_in_trash'  => __( 'No encontrado en la papelera', 'festibox' ),
	);

// Otras opciones para el tipo de entrada personalizada

	$args = array(
		'label'               => $labels,
		'description'         => __( 'Clásica y Ópera', 'festibox' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'thumbnail', 'comments','page_layout','author' ),
		// Puedes asociar este CPT con una taxonomía por defecto o una taxonomía personalizada
		'taxonomies'          => array( 'caja','cuando','edad','provincia'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);

	// Registro del tipo de entrada personalizada (Customs Post Type)
	register_post_type( 'clasicaopera', $args );
}

add_action( 'init', 'custom_clasicaopera_type', 0 );

add_action( 'add_meta_boxes', 'em_mtbx_clasicaopera' );

function em_mtbx_clasicaopera() {

	// Creamos el meta box personalizado
	add_meta_box( 
                'Datos_clasicaopera', // atributo ID
                'Clásica y Ópera', // Título
                'em_mtbx_clasicaopera_function', // Función que muestra el HTML que aparecerá en la pantalla
                'clasicaopera', // Tipo de entrada. Puede ser 'post', 'page', 'link', o 'custom_post_type'
                'normal', // Parte de la pantalla donde aparecerá. Puede ser 'normal', 'advanced', o 'side'
                'high' // Prioridad u orden en el que aparecerá. Puede ser 'high', 'core', 'default' o 'low'
                );
	
	// Creamos el meta box personalizado
	//Se elimina este metabox, el usuario no se mostrará en pantalla
	/*
	add_meta_box( 
                'usuarios_clasicaopera', // atributo ID
                'Usuarios Clásica y Ópera', // Título
                'em_mtbx_clasicaopera_usuario', // Función que muestra el HTML que aparecerá en la pantalla
                'clasicaopera', // Tipo de entrada. Puede ser 'post', 'page', 'link', o 'custom_post_type'
                'normal', // Parte de la pantalla donde aparecerá. Puede ser 'normal', 'advanced', o 'side'
                'high' // Prioridad u orden en el que aparecerá. Puede ser 'high', 'core', 'default' o 'low'
                );
    */				
}

function em_mtbx_clasicaopera_function( $post ) {
    
    $f_hora = get_post_meta( $post->ID, 'f_hora', true );
    $f_recinto = get_post_meta( $post->ID, 'f_recinto', true );
    $f_localidad = get_post_meta( $post->ID, 'f_localidad', true );
    $f_direccion = get_post_meta( $post->ID, 'f_dirección', true );
    $f_nentrada = get_post_meta( $post->ID, 'f_nentrada', true );
     ?>

         <h4><?php _e("Horarios Evento"); ?></h4>
         <input class="em_mtbx_img" name="f_hora" id="f_hora" type="text" value="<?php if($f_hora && $f_hora != '') echo $f_hora; ?>">
         <h4><?php _e("Recinto"); ?></h4>
         <input class="em_mtbx_img" name="f_recinto_festival" id="f_recinto_festival" type="text" value="<?php if($f_recinto && $f_recinto != '') echo $f_recinto; ?>">
         <h4><?php _e("Dirección"); ?></h4>
         <input class="em_mtbx_img" name="f_direccion_festival" id="f_direccion_festival" type="text" value="<?php if($f_direccion && $f_direccion != '') echo $f_direccion; ?>">
         <h4><?php _e("Localidad"); ?></h4>
         <input class="em_mtbx_img" name="f_localidad_festival" id="f_localidad_festival" type="text" value="<?php if($f_localidad && $f_localidad != '') echo $f_localidad; ?>">
          <div class="clear"></div>
          
	<?php       
}

//Para grabar los campos. Añadir uno nuevo por cada campo existente, en este caso tres.
add_action( 'save_post', 'em_mtbx_clasicaopera_save_meta' );

function em_mtbx_clasicaopera_save_meta( $post_id ) {

	if ( isset( $_POST['f_hora'] ) ) {
	
		update_post_meta( $post_id, 'f_hora', $_POST['f_hora']  );

	}
	if ( isset( $_POST['f_recinto_festival'] ) ) {
	
		update_post_meta( $post_id, 'f_recinto', $_POST['f_recinto_festival']  );

	}
	if ( isset( $_POST['f_localidad_festival'] ) ) {
	
		update_post_meta( $post_id, 'f_localidad', $_POST['f_localidad_festival']  );

	}
	if ( isset( $_POST['f_direccion_festival'] ) ) {
	
		update_post_meta( $post_id, 'f_dirección', $_POST['f_direccion_festival']  );

	}
	if ( isset( $_POST['f_nentrada'] ) ) {
	
		update_post_meta( $post_id, 'f_nentrada', $_POST['f_nentrada']  );

	}
	//Se actualiza automáticamente el usuario
	update_post_meta( $post_id, 'f_usuario', 'coolturebox');
}
/*
Se elimina la funcionalidad de mostrar la lista desplegable de usuarios
function em_mtbx_clasicaopera_usuario( $post ) 
{
    
    $f_usuarios = explode(',',get_post_meta( $post->ID, 'f_usuario', true ));
    $blogusers = get_users( 'blog_id=1&orderby=nicename&role=clasicaopera' );
	?>
    <h4><?php _e("Usuario"); ?></h4>
     
    <select multiple name="f_usuario[]">
	<?php       
    
	print_r($blogusers);
	foreach ( $blogusers as $user ) {
        $id = $user -> ID;
        $title = $user -> display_name;
        $selected = "";
        if (in_array($id, $f_usuarios)){$selected = "selected";}
        echo '<option value="'.$id.'" '.$selected.'>'.$title.'</option>';
    }
	?>
    <div class="clear"></div>         
	<?php       
}

//Para grabar los campos. Añadir uno nuevo por cada campo existente, en este caso tres.
add_action( 'save_post', 'em_mtbx_clasicaopera_usu_save_meta' );

function em_mtbx_clasicaopera_usu_save_meta( $post_id ) {

	if ( isset( $_POST['f_usuario'] ) ) {
	
		update_post_meta( $post_id, 'f_usuario', implode(",", $_POST['f_usuario']));

	}
}
*/